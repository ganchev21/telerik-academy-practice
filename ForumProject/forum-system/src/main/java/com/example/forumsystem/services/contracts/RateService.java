package com.example.forumsystem.services.contracts;

import com.example.forumsystem.models.post.models.Post;
import com.example.forumsystem.models.post.models.PostDtoOut;
import com.example.forumsystem.models.post.models.PostRating;
import com.example.forumsystem.models.user.models.User;

import java.util.List;


public interface RateService {

    List<PostDtoOut> getAllPostsRating ();
    StringBuilder getPostRating (Post post);
    PostRating getById (int userId, int postId);
    int getUserRate (User user, int postId);
    void ratePost (User user, int postId, int rate);

}
