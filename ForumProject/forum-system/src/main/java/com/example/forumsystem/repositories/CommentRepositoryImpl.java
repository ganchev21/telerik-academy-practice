package com.example.forumsystem.repositories;

import com.example.forumsystem.exceptions.EntityNotFoundException;
import com.example.forumsystem.models.comment.models.Comment;
import com.example.forumsystem.models.user.models.User;
import com.example.forumsystem.repositories.contracts.CommentRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class CommentRepositoryImpl implements CommentRepository {
    private final SessionFactory sessionFactory;


    @Autowired
    public CommentRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;

    }


    @Override
    public List<Comment> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Comment> query = session.createQuery("from Comment ", Comment.class);
            return query.list();
        }
    }

    //TODO what is this used for
    @Override
    public Comment getCommentById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Comment comment = session.get(Comment.class, id);
            if (comment == null) {
                throw new EntityNotFoundException("Comment", id);
            }
            return comment;
        }
    }

    @Override
    public void createComment(Comment comment) {
        try (Session session = sessionFactory.openSession()) {
            session.save(comment);
        }
    }

    @Override
    public void updateComment(Comment comment) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(comment);
            session.getTransaction().commit();
        }
    }

    @Override
    public void deleteComment(Comment comment) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(comment);
            session.getTransaction().commit();
        }
    }

    //TODO should we use a query for the below 2 methods?
    @Override
    public List<Comment> getUserComments(User user) {
        List<Comment> comments = getAll();
        return comments.stream()
                .filter(c -> c.getUser().getUsername().equals(user.getUsername()))
                .collect(Collectors.toList());
    }

    @Override
    public List<Comment> getPostComments(int id) {
        List<Comment> comments = getAll();
        List<Comment> result = new ArrayList<>();
        for (Comment comment : comments) {
            if (comment.getPost().getId() == id) {
                result.add(comment);
            }
        }
        return result;
    }
}
