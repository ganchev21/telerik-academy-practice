package com.example.forumsystem.models.post.models;

import java.util.List;

public class PostUserDtoOut {
    private String username;

    private List<String> postsWhoCreated;


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<String> getPostsWhoCreated() {
        return postsWhoCreated;
    }

    public void setPostsWhoCreated(List<String> postsWhoCreated) {
        this.postsWhoCreated = postsWhoCreated;
    }
}
