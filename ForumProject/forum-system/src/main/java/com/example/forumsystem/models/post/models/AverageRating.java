package com.example.forumsystem.models.post.models;

import jakarta.persistence.*;

@Entity
@Table(name = "average_rating")
public class AverageRating {

    @Id
    @Column(name = "post_id")
    int id;

    @Column(name = "rating")
    double averageRating;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getAverageRating() {
        return averageRating;
    }

    public void setAverageRating(double averageRating) {
        this.averageRating = averageRating;
    }
}
