package com.example.forumsystem.models.comment.models;

import java.util.ArrayList;
import java.util.List;

public class CommentDtoOut {

    private String postTitle;
    private String postContent;

    private List<String> usersWhoComment = new ArrayList<>();


    public List<String> getUsersWhoComment() {
        return usersWhoComment;
    }

    public void setUsersWhoComment(List<String> usersWhoComment) {
        this.usersWhoComment = usersWhoComment;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public String getPostContent() {
        return postContent;
    }

    public void setPostContent(String postContent) {
        this.postContent = postContent;
    }

}
