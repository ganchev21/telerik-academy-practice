package com.example.forumsystem.repositories;


import com.example.forumsystem.exceptions.EntityNotFoundException;
import com.example.forumsystem.models.FilterOptions;
import com.example.forumsystem.models.FilterUserOptions;
import com.example.forumsystem.models.post.models.Post;
import com.example.forumsystem.models.user.models.User;
import com.example.forumsystem.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class UserRepositoryImpl implements UserRepository {

    public static final String SORT_INCORRECT_MESSAGE = "Sort should have max 2 parameters divided with '-' symbol";
    private final SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    //TODO remove lastName from filter and remove sort maybe? Not in document
        @Override
        public List<User> filter(FilterUserOptions filterUserOptions) {

            try (Session session = sessionFactory.openSession()) {
                StringBuilder result = new StringBuilder(" from User where permissions.isDeleted = false ");
                Map<String, Object> parameters = new HashMap<>();
                ArrayList<String> filter = new ArrayList<>();

                filterUserOptions.getFirstName().ifPresent(value -> {
                    filter.add(" firstName like :firstName");
                    parameters.put("firstName", "%" + value + "%");
                });

//                filterUserOptions.getLastName().ifPresent(value -> {
//                    filter.add(" lastName like :lastName");
//                    parameters.put("lastName", "%" + value + "%");
//                });

                filterUserOptions.getEmail().ifPresent(value -> {
                    filter.add(" email like :email");
                    parameters.put("email", "%" + value + "%");
                });

                filterUserOptions.getUsername().ifPresent(value -> {
                    filter.add(" username like :username");
                    parameters.put("username", "%" + value + "%");
                });

                if (!filter.isEmpty()){
                    result.append(" and ").append(String.join(" and ", filter));
                }

//                filterUserOptions.getSort().ifPresent(value -> result.append(generateSort(value)));

                Query<User> query = session.createQuery(result.toString(), User.class);
                query.setProperties(parameters);
                return query.list();
            }


        }

    private String generateSort(String value) {

        StringBuilder query = new StringBuilder(" order by ");
        String[] params = value.split("-");

        if (value.isEmpty() || params.length > 2) {
                throw new UnsupportedOperationException(
                        SORT_INCORRECT_MESSAGE);
            }

        switch (params[0]) {
            case "firstname":
                query.append(" firstName ");
                break;
            case "lastname":
                query.append(" lastName ");
                break;
            case "email":
                query.append(" email ");
                break;
            case "username":
                query.append(" username ");
                break;

            default:
                throw new UnsupportedOperationException(
                        SORT_INCORRECT_MESSAGE);
        }

        if (params.length == 2 && params[1].equalsIgnoreCase("desc")) {
            query.append(" desc ");
        }


        return query.toString();
    }

    @Override
    public List<User> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where permissions.isDeleted = false", User.class);
            return query.list();
        }
    }

    @Override
    public User getUserById(int id) {
        try(Session session = sessionFactory.openSession()){
            User user = session.get(User.class, id);
            if(user == null || user.isDeleted()){
            throw new EntityNotFoundException("User", id);
        }
        return user;}
    }

    @Override
    public void createUser(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(user);
            session.save(user.getPermissions());
            session.getTransaction().commit();
        }
    }

    @Override
    public void updateUser(User user)  {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void updatePermissions(User user) {
        try (Session session = sessionFactory.openSession()) {
            user.getPermissions().setUserId(user.getId());

            session.beginTransaction();
            session.saveOrUpdate(user.getPermissions());
            session.getTransaction().commit();
        }
    }



    @Override
    public void deleteUser(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(user);
            session.update(user.getPermissions());
            session.getTransaction().commit();
        }
    }



    @Override
    public User getUserByUsername(String username) {
        try (Session session = sessionFactory.openSession()) {

            Query<User> query = session.createQuery("from User where username = :username", User.class);
            query.setParameter("username", username);

            List<User> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("User", "username", username);
            }

            return result.get(0);
        }
    }

    @Override
    public User getUserByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where email = :email", User.class);
            query.setParameter("email", email);

            List<User> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("User", "email", email);
            }

            return result.get(0);
        }
    }






}
