package com.example.forumsystem.services;

import com.example.forumsystem.models.post.models.*;
import com.example.forumsystem.models.user.models.User;
import com.example.forumsystem.repositories.contracts.RateRepository;
import com.example.forumsystem.services.contracts.RateService;
import com.example.forumsystem.services.contracts.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

import static com.example.forumsystem.helpers.ValidationHelper.validateBlockedUser;
import static com.example.forumsystem.helpers.ValidationHelper.validateUserIsAdmin;

@Service
public class RateServiceImpl implements RateService {

    public static final String RATING_EXCEPTION = "This user have already rated this post!";
    private final RateRepository rateRepository;
    private final PostService postService;

    @Autowired
    public RateServiceImpl(RateRepository rateRepository, PostService postService) {
        this.rateRepository = rateRepository;
        this.postService = postService;
    }


    @Override
    public List<PostDtoOut> getAllPostsRating() {
        Set<Integer> postIds = new HashSet<>();
        List<PostRating> allPost = rateRepository.getAllPostsRating();

        for (PostRating reacts : allPost) {
            postIds.add(reacts.getId().getPost().getId());
        }

        List<PostDtoOut> result = new ArrayList<>();
        for (Integer ids : postIds) {

            PostDtoOut postDtoOut = new PostDtoOut();
            Post currentPost = postService.getPostById(ids);
            postDtoOut.setTitle(currentPost.getTitle());
            postDtoOut.setContent(currentPost.getContent());
            postDtoOut.setCreatedBy(currentPost.getCreatedBy().getUsername());
            postDtoOut.setCreatedById(currentPost.getCreatedBy().getId());
            postDtoOut.setTimestamp(currentPost.getTimestamp());
            postDtoOut.setPostId(currentPost.getId());

            double rating = rateRepository.getPostRating(currentPost.getId());
            postDtoOut.setRating(rating);
            result.add(postDtoOut);
//            result.put(Double.valueOf(String.format("%.2f", rating)), postDtoOut);
        }
        result.sort(Comparator.comparing(PostDtoOut::getRating).reversed());
        return result;
    }

    @Override
    public StringBuilder getPostRating(Post post) {
        StringBuilder sb = new StringBuilder();

        Double result = rateRepository.getAveragePostRating(post.getId());
        int peopleRated = rateRepository.getPeopleRated(post.getId());

        sb.append(post.getTitle()).append("@");
        sb.append(String.format("%.2f", result)).append("*");
        sb.append(peopleRated);
        return sb;
    }


    @Override
    public PostRating getById(int userId, int postId) {
        return rateRepository.getById(userId, postId);
    }

    @Override
    public void ratePost(User user, int postId, int rate) {
        validateBlockedUser(user);
        Post post = postService.getPostById(postId);
        PostRating interactions = setUp(user, post, new PostRating(), new PostRatingID());

        boolean alreadyRated = rateRepository.findRecord(user.getId(), postId);

        if (!alreadyRated) {
            throw new IllegalArgumentException(RATING_EXCEPTION);
        }

        interactions.setRate(rate);
        rateRepository.createRate(interactions);
        AverageRating newRating = new AverageRating();
        newRating.setId(postId);
        newRating.setAverageRating(rateRepository.getPostRating(postId));
        rateRepository.createAverageRate(newRating);

    }

    @Override
    public int getUserRate (User user, int postId) {

        Post post = postService.getPostById(postId);

        return rateRepository.getUserRate(user.getId(), postId);

    }



    private PostRating setUp(User user, Post post, PostRating interactions, PostRatingID idKey) {
        idKey.setPost(post);
        idKey.setUser(user);
        interactions.setId(idKey);
        return interactions;
    }
}
