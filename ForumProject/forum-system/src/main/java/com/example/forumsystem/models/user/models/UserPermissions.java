package com.example.forumsystem.models.user.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table (name = "user_permissions")
public class UserPermissions {


    @Id
    @JsonIgnore
    private int user_id;
    @Column (name = "is_admin")
    private boolean isAdmin;

    @Column (name = "is_deleted")
    private boolean isDeleted;
    @Column (name = "is_blocked")
    private boolean isBlocked;


    public int getUserID() {
        return user_id;
    }

    public void setUserId(int user_id) {
        this.user_id = user_id;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public boolean isBlocked() {
        return isBlocked;
    }

    public void setBlocked(boolean blocked) {
        isBlocked = blocked;
    }
}
