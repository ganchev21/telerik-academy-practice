package com.example.forumsystem.controllers.rest;


import com.example.forumsystem.exceptions.AuthenticationFailureException;
import com.example.forumsystem.exceptions.EntityNotFoundException;
import com.example.forumsystem.helpers.AuthenticationHelper;
import com.example.forumsystem.helpers.CommentMapper;
import com.example.forumsystem.models.comment.models.Comment;
import com.example.forumsystem.models.comment.models.CommentDtoIn;
import com.example.forumsystem.models.comment.models.CommentDtoOut;
import com.example.forumsystem.models.user.models.User;
import com.example.forumsystem.services.contracts.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/comments")
public class CommentController {

    private final CommentService commentService;
    private final CommentMapper commentMapper;
    private final AuthenticationHelper authenticationHelper;


    @Autowired
    public CommentController(CommentService commentService, AuthenticationHelper authenticationHelper, CommentMapper commentMapper) {
        this.commentService = commentService;
        this.commentMapper = commentMapper;
        this.authenticationHelper = authenticationHelper;
    }


    @GetMapping("/user/{id}")
    public List<CommentDtoOut> getUserComments(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            List<Comment> userComments = commentService.getUserComments(loggedUser, id);
            return commentMapper.mapObject(userComments);
        } catch (AuthenticationFailureException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/post/{id}")
    public List<CommentDtoOut> getPostComments(@PathVariable int id) {
        try {
            List<Comment> postComments = commentService.getPostComments(id);
            return commentMapper.mapObject(postComments);
        } catch (AuthenticationFailureException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }

    }

    @PostMapping()
    public void create(@RequestHeader HttpHeaders headers, @RequestBody CommentDtoIn commentDtoIn) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            Comment comment = commentMapper.dtoToObject(new Comment(), commentDtoIn, loggedUser);
            commentService.createComment(comment, loggedUser);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthenticationFailureException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public void update(@RequestHeader HttpHeaders headers, @PathVariable int id,
                       @RequestBody CommentDtoIn commentDtoIn) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            Comment comment = commentService.getCommentById(id);
            Comment commentToUpdate = commentMapper.setContent(comment, commentDtoIn);
            commentService.updateComment(comment, loggedUser);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthenticationFailureException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            Comment comment = commentService.getCommentById(id);
            commentService.deleteComment(loggedUser, comment);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthenticationFailureException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
