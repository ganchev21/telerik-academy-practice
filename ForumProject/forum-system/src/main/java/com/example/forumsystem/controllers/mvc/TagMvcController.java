package com.example.forumsystem.controllers.mvc;

import com.example.forumsystem.exceptions.AuthenticationFailureException;
import com.example.forumsystem.exceptions.DuplicateEntityException;
import com.example.forumsystem.exceptions.EntityNotFoundException;
import com.example.forumsystem.helpers.AuthenticationHelper;
import com.example.forumsystem.helpers.UserMapper;
import com.example.forumsystem.models.comment.models.Comment;
import com.example.forumsystem.models.tag.models.Tag;
import com.example.forumsystem.models.tag.models.TagDtoIn;
import com.example.forumsystem.models.user.models.User;
import com.example.forumsystem.services.contracts.TagService;
import com.example.forumsystem.services.contracts.UserService;
import jakarta.servlet.http.HttpSession;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;


@Controller
@RequestMapping("/tags")
public class TagMvcController {
    private final TagService tagService;
    private final AuthenticationHelper authenticationHelper;


    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session){
        return session.getAttribute("currentUser") !=null;
    }

    @Autowired
    public TagMvcController(TagService tagService, AuthenticationHelper authenticationHelper) {
        this.tagService = tagService;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public String showTags(Model model) {
        try {
            List<Tag> tagList = tagService.getAll();
            model.addAttribute("tags", tagList);
            return "tags";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/{tagId}/update")
    public String updateTag(@PathVariable int tagId, Model model) {
        try {
            Tag tag = tagService.getTagById(tagId);
            TagDtoIn tagDtoIn = new TagDtoIn();
            tagDtoIn.setTags(new ArrayList<>());
            tagDtoIn.getTags().add(tag.getTag());
            model.addAttribute("tagId",tagId);
            model.addAttribute("tagDtoIn", tagDtoIn);
            return "edit-tag";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }



    @PostMapping("/{tagId}/delete")
    public String deleteTag(@PathVariable("tagId") int tagId,
                                HttpSession session,
                                Model model) {

        User loggedUser;
        try {
            loggedUser = authenticationHelper.tryGetAdmin(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            Tag tag = tagService.getTagById(tagId);
            tagService.deleteTag(loggedUser, tag);
            return "redirect:/tags";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

//    @PostMapping("/deleteAll")
//    public String deleteAll(@RequestParam("ids") List<Integer> tagIds, HttpSession session, Model model) {
//
//        User loggedUser;
//        try {
//            loggedUser = authenticationHelper.tryGetAdmin(session);
//        } catch (AuthenticationFailureException e) {
//            return "redirect:/auth/login";
//        }
//
//        for (int tagId : tagIds) {
//            try {
//                Tag tag = tagService.getTagById(tagId);
//                tagService.deleteTag(loggedUser, tag);
//            } catch (EntityNotFoundException e) {
//                model.addAttribute("error", e.getMessage());
//                return "not-found";
//            }
//        }
//
//        return "redirect:/tags";
//    }

    @PostMapping( "/{tagId}/update")
    public String update(@PathVariable int tagId,
                         @Valid @ModelAttribute ("tagDtoIn") TagDtoIn tagDtoIn,
                         BindingResult bindingResult,
                         HttpSession session,
                         Model model                         ) {

        User loggedUser;
        try {
            loggedUser = authenticationHelper.tryGetAdmin(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (bindingResult.hasErrors()){
            return "edit-tag";
        }

        try {
            Tag tagFromRepo = tagService.getTagById(tagId);

            String tagToUpdate = tagDtoIn.getTags().get(0);

            tagService.updateTag(loggedUser,tagFromRepo, tagToUpdate);



            return "redirect:/tags";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("tags[0]", "tag_error", e.getMessage());
            return "edit-tag";
        }
    }

}


