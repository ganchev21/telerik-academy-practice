package com.example.forumsystem.controllers.rest;

import com.example.forumsystem.exceptions.AuthenticationFailureException;
import com.example.forumsystem.exceptions.DuplicateEntityException;
import com.example.forumsystem.exceptions.EntityNotFoundException;
import com.example.forumsystem.helpers.AuthenticationHelper;
import com.example.forumsystem.models.tag.models.Tag;
import com.example.forumsystem.models.tag.models.TagDtoIn;
import com.example.forumsystem.services.contracts.TagService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import com.example.forumsystem.models.user.models.User;

import java.util.List;

@RestController
@RequestMapping("/api/tags")
public class TagController {

    private final TagService tagService;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public TagController(TagService tagService, AuthenticationHelper authenticationHelper) {
        this.tagService = tagService;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<Tag> getAll() {
        return tagService.getAll();
    }

    @GetMapping("/{id}")
    public Tag getTag(@PathVariable int id) {
        try {
            return tagService.getTagById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public List<String> create(@RequestHeader HttpHeaders headers, @Valid @RequestBody TagDtoIn tagDtoIn) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            List<String> tagList = tagDtoIn.getTags();
            tagService.createAll(loggedUser, tagList);
            return tagList;
        } catch (AuthenticationFailureException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }


         @PutMapping("/{id}")
         public Tag update(@RequestHeader HttpHeaders headers,
                            @PathVariable int id,
                            @Valid @RequestBody TagDtoIn tagDtoIn) {
             try {
                 User loggedUser = authenticationHelper.tryGetUser(headers);
                 Tag tagFromRepo = tagService.getTagById(id);

                 String tagToUpdate = tagDtoIn.getTags().get(0);

                 tagService.updateTag(loggedUser,tagFromRepo, tagToUpdate);

                 return tagFromRepo;
             } catch (EntityNotFoundException e) {
                 throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
             } catch (DuplicateEntityException e) {
                 throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
             } catch (AuthenticationFailureException e) {
                 throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
             }
         }

            @DeleteMapping("/{id}")
            public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
                try {
                    User loggedUser = authenticationHelper.tryGetUser(headers);
                    Tag tagToDelete = getTag(id);
                    tagService.deleteTag(loggedUser, tagToDelete);
                } catch (EntityNotFoundException e) {
                    throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
                } catch (AuthenticationFailureException e) {
                    throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
                }
            }

}