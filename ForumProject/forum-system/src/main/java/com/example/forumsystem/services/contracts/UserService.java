package com.example.forumsystem.services.contracts;


import com.example.forumsystem.models.FilterUserOptions;
import com.example.forumsystem.models.user.models.User;

import java.util.List;
import java.util.Optional;

public interface UserService {
    List<User> getAll();

    List<User> filter(FilterUserOptions filterUserOptions);

    void createUser(User user);

    void updateUser(User loggedUser, User user);

    void deleteUser(User loggedUser, User userToDelete);

    void updatePermissions(User loggedUser,User userToUpdate);

    User getUserById(int id);

    User getUserByUsername(String username);

    User getUserByEmail(String email);
}
