package com.example.forumsystem.models;

public class FilterDto {

    private String searchQuery;

    private String sort;

    public FilterDto(String searchQuery, String sort) {
        this.searchQuery = searchQuery;
        this.sort = sort;
    }

    public String getSearchQuery() {
        return searchQuery;
    }

    public void setSearchQuery(String searchQuery) {
        this.searchQuery = searchQuery;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

}
