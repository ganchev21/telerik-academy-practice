package com.example.forumsystem.models.post.models;

import org.hibernate.validator.constraints.Range;

public class PostRatingDtoIn {
//    @Positive
//    private int postId;

    @Range(min = 0, max = 10)
    private int rate;

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }


}
