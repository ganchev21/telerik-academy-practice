package com.example.forumsystem.models.comment.models;

import jakarta.validation.constraints.Size;

public class CommentDto {
    private int id;


    private String author;

    @Size(min = 1, message = "You cannot write an empty comment")
    private String content;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

}
