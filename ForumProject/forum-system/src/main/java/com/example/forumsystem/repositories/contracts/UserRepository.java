package com.example.forumsystem.repositories.contracts;


import com.example.forumsystem.models.FilterOptions;
import com.example.forumsystem.models.FilterUserOptions;
import com.example.forumsystem.models.user.models.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository {

    List<User> filter(FilterUserOptions filterOptions);
    List<User> getAll();
    User getUserById(int id);

    User getUserByUsername(String username);

    User getUserByEmail(String email);

    void createUser(User user);

    void updateUser(User user);

    void deleteUser(User user);

    void updatePermissions(User user);
}
