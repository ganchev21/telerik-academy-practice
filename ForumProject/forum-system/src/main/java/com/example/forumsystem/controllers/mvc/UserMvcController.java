package com.example.forumsystem.controllers.mvc;

import com.example.forumsystem.exceptions.AuthenticationFailureException;
import com.example.forumsystem.exceptions.DuplicateEntityException;
import com.example.forumsystem.exceptions.EntityNotFoundException;
import com.example.forumsystem.helpers.AuthenticationHelper;
import com.example.forumsystem.helpers.UserMapper;
import com.example.forumsystem.models.user.models.PasswordResetDto;
import com.example.forumsystem.models.user.models.User;
import com.example.forumsystem.models.user.models.UserDtoIn;
import com.example.forumsystem.models.FilterUserOptions;
import com.example.forumsystem.models.FilterUserOptionsDto;
import com.example.forumsystem.services.contracts.UserService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/users")
public class UserMvcController {

    private final UserService userService;
    private final UserMapper userMapper;
    private final AuthenticationHelper authenticationHelper;


    @Autowired
    public UserMvcController(UserService userService, UserMapper userMapper, AuthenticationHelper authenticationHelper) {
        this.userService = userService;
        this.userMapper = userMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @GetMapping("/{userId}")
    public String showUser(@PathVariable int userId, Model model) {
        try {

            User user = userService.getUserById(userId);
            model.addAttribute("user", user);
            return "user";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }


    @GetMapping
    public String showUsers(Model model) {
        try {
            List<User> userList = userService.getAll();
            model.addAttribute("users", userList);
            return "users";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/{userId}/update")
    public String showEditUserPage(@PathVariable int userId,
                                   Model model,
                                   HttpSession session) {

        User loggedUser;
        try {
            loggedUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        if (loggedUser.getId() != userId && !loggedUser.isAdmin()) {
            return "not-found";
        }
        try {
            User user = userService.getUserById(userId);
            UserDtoIn userToDto = userMapper.toDto(user);
            model.addAttribute("user", user);
            model.addAttribute("update", userToDto);
            return "user-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/{userId}/update")
    public String updateUser(@PathVariable int userId,
                             @Valid @ModelAttribute("update") UserDtoIn updateUserDto,
                             BindingResult bindingResult,
                             Model model,
                             HttpSession session) {

        User loggedUser;
        try {
            loggedUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }


        try {
            User user = userService.getUserById(userId);
            model.addAttribute("user", user);

            //added user to model to display admin options when error occur
            if (bindingResult.hasErrors()) {
                return "user-update";
            }

            User userToUpdate = userMapper.fromDto(user, updateUserDto);
            userService.updateUser(loggedUser, userToUpdate);
            return "redirect:/users/{userId}";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("email", "email_error", e.getMessage());
            return "user-update";
        }
    }

    @GetMapping("/{userId}/reset-password")
    public String showResetPasswordPage(@PathVariable int userId,
                                        Model model,
                                        HttpSession session) {
        User loggedUser;
        try {
            loggedUser = authenticationHelper.tryGetUser(session);

        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (loggedUser.getId() != userId) {
            return "not-found";
        }

        try {
            User user = userService.getUserById(userId);
            model.addAttribute("userId", user.getId());
            model.addAttribute("password", new PasswordResetDto());
            return "reset-password";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/{userId}/reset-password")
    public String updatePassword(@PathVariable int userId,
                                 @Valid @ModelAttribute("password") PasswordResetDto resetDto,
                                 BindingResult bindingResult,
                                 Model model,
                                 HttpSession session) {

        if (bindingResult.hasErrors()) {
            return "reset-password";
        }

        if (!resetDto.getPassword().equals(resetDto.getPasswordConfirm())) {
            bindingResult.rejectValue("passwordConfirm", "password_error", "Passwords do not match");
            return "reset-password";
        }

        User loggedUser;
        try {
            loggedUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        try {
            User user = userService.getUserById(userId);

            if (resetDto.getPassword().equals(user.getPassword())) {
                bindingResult.rejectValue("password",
                        "password_error",
                        "New password must be different from your last password!");
                return "reset-password";
            }

            User userToUpdate = userMapper.mapPassword(user, resetDto);
            userService.updateUser(loggedUser, userToUpdate);
            return "redirect:/users/{userId}";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }


    @PostMapping("/{userId}/update/permissions")
    public String updatePermissions(@PathVariable int userId,
                                    Model model,
                                    HttpSession session,
                                    @RequestParam("action") String action,
                                    @RequestParam("source") String source) {

        User loggedUser;
        try {
            loggedUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        try {

            User userToModify = userService.getUserById(userId);
            if (action.equals("delete")) {
                userService.deleteUser(loggedUser, userToModify);
                return "redirect:/users";
            }

            User userWithNewPermissions = userMapper.updatePermissions(userToModify, action);
            userService.updatePermissions(loggedUser, userWithNewPermissions);

            if (source.equals("user-update")){
                return "redirect:/users/{userId}/update";
            } else {
                return "redirect:/users";
            }
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }
    @GetMapping("/search")
    public String showUserSearch(@ModelAttribute("filterOptions") FilterUserOptionsDto filterDto, Model model) {
        FilterUserOptions filterOptions = new FilterUserOptions(filterDto.getFirstName(), filterDto.getEmail(), filterDto.getUsername());
        List<User> users = userService.filter(filterOptions);
        model.addAttribute("filterOptions", filterDto);
        model.addAttribute("users", users);
        return "users";
    }

    @ModelAttribute
    public void configuration(FilterUserOptionsDto filterDto, Model model) {
        model.addAttribute("filterOptions", filterDto);
    }
    @ModelAttribute
    public void config(HttpServletRequest request, Model model) {
        model.addAttribute("requestURI", request.getRequestURI());
    }
}
