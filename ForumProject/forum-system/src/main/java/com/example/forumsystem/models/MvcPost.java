package com.example.forumsystem.models;

import com.example.forumsystem.models.comment.models.Comment;
import com.example.forumsystem.models.post.models.PostRatingDtoOut;
import com.example.forumsystem.models.tag.models.Tag;
import com.example.forumsystem.models.user.models.User;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

public class MvcPost {
    int id;
    private String title;
    private String content;
    private LocalDate timestamp;
    private User createdBy;
    private PostRatingDtoOut rating;
    private Set<User> likes;
    private Set<User> dislikes;
    private Set<Tag> tags;
    private List<Comment> commentsList;


    public Set<User> getDislikes() {
        return dislikes;
    }

    public void setDislikes(Set<User> dislikes) {
        this.dislikes = dislikes;
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    public Set<User> getLikes() {
        return likes;
    }

    public void setLikes(Set<User> likes) {
        this.likes = likes;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public LocalDate getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDate timestamp) {
        this.timestamp = timestamp;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public PostRatingDtoOut getRating() {
        return rating;
    }

    public void setRating(PostRatingDtoOut rating) {
        this.rating = rating;
    }

    public List<Comment> getCommentsList() {
        return commentsList;
    }

    public void setCommentsList(List<Comment> commentsList) {
        this.commentsList = commentsList;
    }
}
