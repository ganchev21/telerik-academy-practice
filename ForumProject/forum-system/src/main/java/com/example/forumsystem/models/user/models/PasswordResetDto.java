package com.example.forumsystem.models.user.models;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

public class PasswordResetDto {

    @NotNull
    @Size(min = 8, message = "Password should be at least 8 characters long")
    private String password;
    @NotNull(message = "Password cannot be empty")
    @Size(min = 8, message = "Password should be at least 8 characters long")
    private String passwordConfirm;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }
}
