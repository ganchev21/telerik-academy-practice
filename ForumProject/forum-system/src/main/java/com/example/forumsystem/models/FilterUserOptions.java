package com.example.forumsystem.models;

import org.springframework.stereotype.Component;

import java.util.Optional;
@Component
public class FilterUserOptions {

    private Optional<String> firstName;
    private Optional<String> lastName;
    private Optional<String> email;
    private Optional<String> username;

    private Optional<String> sort;


    public FilterUserOptions(String firstName, String lastName, String email, String username,String sort) {
        this.firstName = Optional.ofNullable(firstName);
        this.lastName = Optional.ofNullable(lastName);
        this.email = Optional.ofNullable(email);
        this.username = Optional.ofNullable(username);
        this.sort = Optional.ofNullable(sort);
    }

    public FilterUserOptions(String firstName, String email, String username) {
        this.firstName=Optional.ofNullable(firstName);
        this.email=Optional.ofNullable(email);
        this.username=Optional.ofNullable(username);
    }


    public FilterUserOptions() {
    }

    public Optional<String> getFirstName() {
        return firstName;
    }

    public void setFirstName(Optional<String> firstName) {
        this.firstName = firstName;
    }

    public Optional<String> getLastName() {
        return lastName;
    }

    public void setLastName(Optional<String> lastName) {
        this.lastName = lastName;
    }

    public Optional<String> getEmail() {
        return email;
    }

    public void setEmail(Optional<String> email) {
        this.email = email;
    }

    public Optional<String> getUsername() {
        return username;
    }

    public void setUsername(Optional<String> username) {
        this.username = username;
    }

    public Optional<String> getSort() {
        return sort;
    }

    public void setSort(Optional<String> sort) {
        this.sort = sort;
    }
}
