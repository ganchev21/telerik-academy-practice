package com.example.forumsystem.services;

import com.example.forumsystem.models.comment.models.Comment;
import com.example.forumsystem.models.comment.models.CommentDtoIn;
import com.example.forumsystem.models.user.models.User;
import com.example.forumsystem.repositories.contracts.CommentRepository;
import com.example.forumsystem.repositories.contracts.PostRepository;
import com.example.forumsystem.repositories.contracts.UserRepository;
import com.example.forumsystem.services.contracts.CommentService;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.example.forumsystem.helpers.ValidationHelper.checkUserPermissions;
import static com.example.forumsystem.helpers.ValidationHelper.validateBlockedUser;

@Service
public class CommentServiceImpl implements CommentService {

    private final CommentRepository commentRepository;
    private final UserRepository userRepository;
    private final PostRepository postRepository;

    public CommentServiceImpl(CommentRepository commentRepository, UserRepository userRepository, PostRepository postRepository) {
        this.commentRepository = commentRepository;

        this.userRepository = userRepository;
        this.postRepository = postRepository;
    }


//    @Override
//    public List<Comment> getAll() {
//        return commentRepository.getAll();
//    }
//
    @Override
    public Comment getCommentById(int id) {
        return commentRepository.getCommentById(id);
    }

    @Override
    public void createComment(Comment comment, User loggeduser) {
        validateBlockedUser(loggeduser);
        postRepository.getPostById(comment.getPost().getId());
        commentRepository.createComment(comment);
    }

    @Override
    public void updateComment(Comment comment, User loggedUser) {
        validateBlockedUser(loggedUser);
        checkUserPermissions(loggedUser, comment.getUser());
        commentRepository.updateComment(comment);
    }

    @Override
    public void deleteComment(User loggedUser, Comment comment) {
        validateBlockedUser(loggedUser);
        checkUserPermissions(loggedUser, comment.getUser());

        commentRepository.deleteComment(comment);
    }

    @Override
    public List<Comment> getUserComments(User loggedUser, int id) {
        User userToCheck = userRepository.getUserById(id);
        return commentRepository.getUserComments(userToCheck);
    }

    @Override
    public List<Comment> getPostComments(int id) {
        postRepository.getPostById(id);
        return commentRepository.getPostComments(id);
    }

}
