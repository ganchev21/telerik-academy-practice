package com.example.forumsystem.models.post.models;

import com.example.forumsystem.models.tag.models.Tag;
import com.example.forumsystem.models.user.models.User;
import jakarta.persistence.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "posts")
public class Post {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "post_id")
    int id;
    @Column(name = "title")
    private String title;
    @Column(name = "content")
    private String content;
    @Column(name = "timestamp")
    private LocalDate timestamp;
    @OneToOne
    @JoinColumn(name = "user_id")
    private User createdBy;


    @ManyToMany (fetch = FetchType.EAGER)
    @JoinTable(name = "tag_posts",
    joinColumns = @JoinColumn (name = "post_id"),
    inverseJoinColumns = @JoinColumn(name = "tag_id"))
    private Set<Tag> tags;

    @ManyToMany (fetch = FetchType.EAGER)
    @JoinTable(name = "likes",
            joinColumns = @JoinColumn (name = "post_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id"))
    private Set<User> likes;

    @ManyToMany (fetch = FetchType.EAGER)
    @JoinTable(name = "dislike",
            joinColumns = @JoinColumn (name = "post_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id"))
    private Set<User> dislikes;


    public Post() {
        this.timestamp = LocalDate.now();
        this.tags = new HashSet<>();
        this.likes = new HashSet<>();
        this.dislikes = new HashSet<>();
    }

    public LocalDate getTimestamp() {
        String time = this.timestamp.toString();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return LocalDate.parse(time, formatter);
    }

    public void setTimestamp(LocalDate timestamp) {
        this.timestamp = timestamp;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User user) {
        this.createdBy = user;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }


    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    public Set<Tag> getTagSet() {
        return tags;
    }

    public void setTagSet(Set<Tag> tagSet) {
        this.tags = tagSet;
    }

    public Set<User> getLikes() {
        return likes;
    }

    public void setLikes(Set<User> likes) {
        this.likes = likes;
    }

    public Set<User> getDislikes() {
        return dislikes;
    }

    public void setDislikes(Set<User> dislikes) {
        this.dislikes = dislikes;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Post post = (Post) o;
        return this.id == post.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}


