package com.example.forumsystem.helpers;

import com.example.forumsystem.models.user.models.*;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {




    public User updatePermissions(User user, String action) {

        boolean isAdmin = user.isAdmin();
        boolean isBlocked = user.isBlocked();

        if (action.equals("admin")) {
            user.setAdmin(!isAdmin);
        } else if (action.equals("block")) {
            user.setBlocked(!isBlocked);
        }

        return user;
    }



    public User fromDto(UserDtoIn dto) {
        User user = new User();
        user.setFirstName(dto.getFirstName());
        user.setLastName(dto.getLastName());
        user.setUsername(dto.getUsername());
        user.setPassword(dto.getPassword());
        user.setEmail(dto.getEmail());

        if (dto.getPhone() != null && !dto.getPhone().isEmpty()){
            user.setPhone(dto.getPhone());
        }
        return user;
    }

    public User fromDto(User user, UserDtoIn dto) {
        User updatedUser = fromDto(dto);
        updatedUser.setId(user.getId());
        updatedUser.setPermissions(user.getPermissions());
        return updatedUser;
    }

    public UserDtoIn toDto(User user) {
        UserDtoIn registerDto = new UserDtoIn();
        registerDto.setFirstName(user.getFirstName());
        registerDto.setLastName(user.getLastName());
        registerDto.setUsername(user.getUsername());
        registerDto.setEmail(user.getEmail());
        registerDto.setPassword(user.getPassword());
        registerDto.setPasswordConfirm(user.getPassword());
        if (user.getPhone().isPresent()) {
            registerDto.setPhone(user.getPhone().orElse(null));
        }
        return registerDto;
    }

        public User mapPassword(User user, PasswordResetDto resetDto) {
            UserDtoIn updatedUser = toDto(user);
            updatedUser.setPassword(resetDto.getPassword());
            updatedUser.setPasswordConfirm(resetDto.getPasswordConfirm());

            return fromDto(user,updatedUser);
        }


}
