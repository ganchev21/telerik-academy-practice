package com.example.forumsystem.services;


import com.example.forumsystem.exceptions.DuplicateEntityException;
import com.example.forumsystem.exceptions.EntityNotFoundException;
import com.example.forumsystem.models.FilterOptions;
import com.example.forumsystem.models.post.models.Post;
import com.example.forumsystem.models.tag.models.Tag;
import com.example.forumsystem.models.user.models.User;
import com.example.forumsystem.repositories.contracts.PostRepository;
import com.example.forumsystem.services.contracts.PostService;
import com.example.forumsystem.services.contracts.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.example.forumsystem.helpers.ValidationHelper.checkUserPermissions;
import static com.example.forumsystem.helpers.ValidationHelper.validateBlockedUser;

@Service
public class PostServiceImpls implements PostService {
    private final PostRepository repository;
    private final TagService tagService;

    @Autowired
    public PostServiceImpls(PostRepository repository, TagService tagService) {
        this.repository = repository;
        this.tagService = tagService;
    }


    @Override
    public List<Post> getMostCommented() {
        return repository.getMostCommentedPosts();
    }

    @Override
    public List<Post> getMostRecentPosts() {
        return repository.getMostRecentPosts();
    }

    @Override
    public List<Post> filter(FilterOptions filterOptions) {
        return repository.filter(filterOptions);
    }

    //TODO implement search
    @Override
    public List<Post> getAll() {
        return repository.getAll();
    }

    @Override
    public Post getPostById(int id) {
        return repository.getPostById(id);
    }

    @Override
    public List<Post> getPostsByTag(String tag) {
        return repository.getPostsByTag(tag);
    }


    @Override
    public String getPostTitle(int id) {
        Post postToCheck = repository.getPostById(id);
        return postToCheck.getTitle();
    }

    @Override
    public String getPostContent(int id) {
        Post postToCheck = repository.getPostById(id);
        return postToCheck.getContent();
    }

    public List<Post> getUserPosts(List<Post> posts, User user) {
        return repository.getUserPosts(posts, user);

    }

    @Override
    public void createPost(Post post) {
        boolean postExists = true;
        try {
            repository.getPostByTitle(post.getTitle());
        } catch (EntityNotFoundException e) {
            postExists = false;
        }
        if (postExists) {
            throw new DuplicateEntityException("Post", "title", post.getTitle());
        }

        repository.createPost(post);

    }

    @Override
    public void updatePost(Post postFromDto, Post existingPost) {
        checkUserPermissions(postFromDto.getCreatedBy(), existingPost.getCreatedBy());
        validateBlockedUser(postFromDto.getCreatedBy());

        boolean postExists = true;
        try {
            Post checkDuplicate = repository.getPostByTitle(postFromDto.getTitle());

            if (checkDuplicate.getId() == existingPost.getId()) {
                postExists = false;
            }
        } catch (EntityNotFoundException e) {
            postExists = false;
        }
        if (postExists) {
            throw new DuplicateEntityException("Post", "title", postFromDto.getTitle());
        }

        //gets existing tags and adds them to post
        Post receiveTags = repository.getPostById(postFromDto.getId());
        postFromDto.setTagSet(receiveTags.getTagSet());
        repository.updatePost(postFromDto);
    }

    @Override
    public void deletePost(User loggedUser, Post postToDelete) {
        checkUserPermissions(loggedUser, postToDelete.getCreatedBy());
        repository.deletePost(postToDelete);
    }

    @Override
    public void likePost(User loggedUser, Post post) {
        validateBlockedUser(loggedUser);

        if (!post.getLikes().contains(loggedUser)) {
            post.getLikes().add(loggedUser);
        } else {
            post.getLikes().remove(loggedUser);
        }
        post.getDislikes().remove(loggedUser);
        repository.updatePost(post);

    }

    @Override
    public void dislikePost(User loggedUser, Post post) {
        validateBlockedUser(loggedUser);
        if (!post.getDislikes().contains(loggedUser)) {
            post.getDislikes().add(loggedUser);
        } else {
            post.getDislikes().remove(loggedUser);
        }
        post.getLikes().remove(loggedUser);
        repository.updatePost(post);
    }

//    @Override
//    public void ratePost (User loggedUser, Post post, int rate) {
//        validateBlockedUser(loggedUser);
//
//        post.getRates().add(loggedUser);
////        boolean alreadyRated = interactionRepository.findRecord(user.getId(), postId);
////
////        if (!alreadyRated) {
////            throw new IllegalArgumentException(RATING_EXCEPTION);
////        }
//
//
//        repository.updatePost(post);
//    }


    @Override
    public void updatePostTags(Post post, List<String> tagList) {
        if (tagList != null) {
            for (String tag : tagList) {
                Tag newTag = tagService.createTag(tag);

                post.getTagSet().add(newTag);

            }
            repository.updatePost(post);
        }
    }

    @Override
    public void removePostTags(Post post, List<String> tagList) {
        if (tagList != null) {
            for (String tag : tagList) {
                Tag newTag = tagService.createTag(tag);
                post.getTagSet().remove(newTag);
            }
            repository.updatePost(post);
        }
    }
}
