package com.example.forumsystem.helpers;

import com.example.forumsystem.models.comment.models.Comment;
import com.example.forumsystem.models.comment.models.CommentDto;
import com.example.forumsystem.models.comment.models.CommentDtoIn;
import com.example.forumsystem.models.comment.models.CommentDtoOut;
import com.example.forumsystem.models.user.models.User;
import com.example.forumsystem.services.contracts.CommentService;
import com.example.forumsystem.services.contracts.PostService;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class CommentMapper {
    private final PostService postService;
    private final CommentService commentService;

    public CommentMapper(PostService postService, CommentService commentService) {
        this.postService = postService;
        this.commentService = commentService;
    }


    public Comment setContent (Comment comment, CommentDtoIn commentDtoIn) {
        comment.setContent(comment.getContent());
        return comment;
    }

    public Comment dtoToObject(Comment comment, CommentDtoIn commentDto, User user) {
        comment.setContent(commentDto.getContent());
        comment.setUser(user);
        comment.setPost(postService.getPostById(commentDto.getPostId()));
        comment.setTimestamp(LocalDate.now());
        return comment;
    }

    public List<CommentDtoOut> ObjectToDto(HashMap<String, List<String>> map) {
        List<CommentDtoOut> commentDtoOut = new ArrayList<>();
        for (Map.Entry<String, List<String>> mapSet : map.entrySet()) {
            CommentDtoOut result = new CommentDtoOut();
            int firstSpace = mapSet.getKey().indexOf("@");
            String title = mapSet.getKey().substring(0, firstSpace);
            String postContent = mapSet.getKey().substring(firstSpace + 1);

            result.setPostTitle(title);
            result.setPostContent(postContent);
            result.setUsersWhoComment(mapSet.getValue());
            commentDtoOut.add(result);
        }
        return commentDtoOut;
    }

    public List<CommentDtoOut>  mapObject(List<Comment> comments) {
        HashMap<String, List<String>> resultMap = new HashMap<>();
        for (Comment comment : comments) {
            StringBuilder sb = new StringBuilder();
            sb.append(comment.getPost().getTitle()).append("@");
            sb.append(comment.getPost().getContent());
            if (!resultMap.containsKey(sb.toString())) {
                resultMap.put(sb.toString(), new ArrayList<>());
            }
            resultMap.get(sb.toString())
                    .add(comment.getUser().getUsername() + ":-> " + comment.getContent());
        }

         return ObjectToDto(resultMap);
    }
    public Comment fromDto(CommentDto commentDTO, int id) {
        Comment comment = commentService.getCommentById(id);
        comment.setContent(commentDTO.getContent());

        return comment;
    }

    public Comment fromDto(CommentDto commentDTO) {
        Comment comment = new Comment();
        comment.setContent(commentDTO.getContent());

        return comment;
    }
    public CommentDto toDto(Comment comment) {
        CommentDto commentDTO = new CommentDto();

        commentDTO.setId(comment.getId());
        commentDTO.setContent(comment.getContent());
        commentDTO.setAuthor(comment.getUser().getUsername());

        return commentDTO;
    }



}
