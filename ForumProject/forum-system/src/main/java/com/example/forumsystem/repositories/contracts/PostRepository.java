package com.example.forumsystem.repositories.contracts;

import com.example.forumsystem.models.FilterOptions;
import com.example.forumsystem.models.post.models.Post;
import com.example.forumsystem.models.user.models.User;

import java.util.List;
import java.util.Optional;

public interface PostRepository {
    List<Post> getAll();

    List<Post> filter(FilterOptions filterOptions);

    Post getPostById(int id);

    void createPost(Post post);

    void updatePost(Post post);

    void deletePost(Post post);

    List<Post> getMostCommentedPosts();

    List<Post> getMostRecentPosts();
    List<Post> getUserPosts(List<Post> post, User user);

    Post getPostByTitle(String title);

    List<Post> getPostsByTag(String tag);


}
