package com.example.forumsystem.services;

import com.example.forumsystem.exceptions.DuplicateEntityException;
import com.example.forumsystem.exceptions.EntityNotFoundException;
import com.example.forumsystem.models.tag.models.Tag;
import com.example.forumsystem.models.user.models.User;
import com.example.forumsystem.repositories.contracts.TagRepository;
import com.example.forumsystem.services.contracts.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.example.forumsystem.helpers.ValidationHelper.validateUserIsAdmin;


@Service
public class TagServiceImpl implements TagService {

    private final TagRepository tagRepository;

    @Autowired
    public TagServiceImpl(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    @Override
    public List<Tag> getAll() {
        return tagRepository.getAll();
    }

    @Override
    public Tag getTagById(int id) {
        return tagRepository.getTagById(id);
    }

    @Override
    public Tag createTag(String tagName) {
        Tag tag = new Tag();
        tag.setTag(tagName.toLowerCase());

        try {
            return tagRepository.getTagByName(tagName);
        } catch (EntityNotFoundException e) {
            tagRepository.createTag(tag);

        }
        return tag;
    }

    @Override
    public void createAll(User loggedUser, List<String> tagList) {
        validateUserIsAdmin(loggedUser);

        for (String tag : tagList) {
            createTag(tag);
        }
    }

    @Override
    public void updateTag(User loggedUser, Tag tagFromRepo, String tagToUpdate) {
        validateUserIsAdmin(loggedUser);


        boolean tagExists = true;
        try {
            Tag newTag = tagRepository.getTagByName(tagToUpdate);
            if (newTag.getId() == tagFromRepo.getId()) {
                tagExists = false;
            }
        } catch (EntityNotFoundException e) {
            tagExists = false;
        }

        if (tagExists) throw new DuplicateEntityException("Tag", "name", tagToUpdate);

        tagFromRepo.setTag(tagToUpdate.toLowerCase());
        tagRepository.updateTag(tagFromRepo);
    }


    @Override
    public void deleteTag(User loggedUser, Tag tag) {
        validateUserIsAdmin(loggedUser);

        tagRepository.deleteTag(tag);
    }
}
