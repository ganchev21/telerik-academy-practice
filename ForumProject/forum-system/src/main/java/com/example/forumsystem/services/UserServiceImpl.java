package com.example.forumsystem.services;


import com.example.forumsystem.exceptions.AuthenticationFailureException;
import com.example.forumsystem.exceptions.DuplicateEntityException;
import com.example.forumsystem.exceptions.EntityNotFoundException;
import com.example.forumsystem.models.FilterUserOptions;
import com.example.forumsystem.models.user.models.User;
import com.example.forumsystem.repositories.contracts.UserRepository;
import com.example.forumsystem.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static com.example.forumsystem.helpers.ValidationHelper.*;


@Service
public class UserServiceImpl implements UserService {

    private static final String USERNAME_UPDATE_ERROR = "Username cannot be changed once created";

    private final UserRepository repository;

    @Autowired
    public UserServiceImpl(UserRepository repository) {
        this.repository = repository;
    }

     @Override
     //all checks for filtering and permissions will be added here ( who can sort who can filter)
     public List<User> filter(FilterUserOptions filterUserOptions) {
         return repository.filter(filterUserOptions);
     }

    @Override
    public List<User> getAll() {
        return repository.getAll();
    }

    @Override
    public User getUserById(int id) {
        return repository.getUserById(id);
    }

    @Override
    public User getUserByUsername(String username) {
        return repository.getUserByUsername(username);
    }

    @Override
    public User getUserByEmail(String email) {
        return repository.getUserByEmail(email);
    }

    @Override
    public void createUser(User user) {
        checkAddedPhonePermissions(user);
        checkForDuplicateValues(user);
        repository.createUser(user);
    }


    @Override
    public void updateUser(User loggedUser, User user) {
        User userFromRepo = repository.getUserById(user.getId());

        if (!userFromRepo.getUsername().equals(user.getUsername())){
            throw new AuthenticationFailureException(USERNAME_UPDATE_ERROR);
        }

        checkUserPermissions(loggedUser, user);
        checkAddedPhonePermissions(user);
        checkForDuplicateValues(user);

        repository.updateUser(user);
    }

    @Override
    public void deleteUser(User loggedUser, User userToDelete) {
        checkUserPermissions(loggedUser, userToDelete);

        //TODO change all fields and only save username (maybe do *DELETED*username for display)

        userToDelete.setDeleted(true);
        userToDelete.setPhone(null);

        repository.deleteUser(userToDelete);
    }

    @Override
    public void updatePermissions(User loggedUser, User userToModify) {

        checkUserPermissions(loggedUser, userToModify);
        validateUserIsAdmin(loggedUser);

        repository.updatePermissions(userToModify);
    }




    public void checkForDuplicateValues(User user) {
        validate("username", user.getUsername(), user.getId());
        validate("email", user.getEmail(), user.getId());
    }

    public void validate(String param, String value, int userId) {
        User checkUser;
        boolean userExists = true;
        try {
            if (param.equals("username")) {
                checkUser = repository.getUserByUsername(value);
            } else {
                checkUser = repository.getUserByEmail(value);
            }
            if (userId == checkUser.getId()) {
                userExists = false;
            }
        } catch (EntityNotFoundException e) {
            userExists = false;
        }

        if (userExists) throw new DuplicateEntityException("User", param, value);
    }
}
