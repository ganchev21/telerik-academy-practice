package com.example.forumsystem.helpers;

import com.example.forumsystem.exceptions.AuthenticationFailureException;
import com.example.forumsystem.exceptions.EntityNotFoundException;
import com.example.forumsystem.models.user.models.User;
import com.example.forumsystem.services.contracts.UserService;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

@Component
public class AuthenticationHelper {

    private static final String AUTHORIZATION_HEADER_NAME = "Authorization";
    private static final String INVALID_AUTHENTICATION_ERROR = "Invalid authentication.";
    public static final String WRONG_USERNAME_OR_PASSWORD = "Wrong username or password";

    private final UserService userService;


    @Autowired
    public AuthenticationHelper(UserService userService) {
        this.userService = userService;
    }


    public User tryGetUser(HttpHeaders headers) {
        if (!headers.containsKey(AUTHORIZATION_HEADER_NAME)) {
            throw new AuthenticationFailureException(INVALID_AUTHENTICATION_ERROR);
        }

        try {
            String userInfo = String.valueOf(headers.getFirst(AUTHORIZATION_HEADER_NAME));
            assert userInfo != null;
            String username = getUsername(userInfo);
            String password = getPassword(userInfo);

            User user = userService.getUserByUsername(username);

            if (!user.getPassword().equals(password) || user.isDeleted()) {
                throw new AuthenticationFailureException(INVALID_AUTHENTICATION_ERROR);
            }
            return user;
        } catch (EntityNotFoundException e) {
            throw new AuthenticationFailureException(INVALID_AUTHENTICATION_ERROR);
        }
    }

    private String getUsername(String userInfo) {
        int firstSpace = userInfo.indexOf(" ");
        if (firstSpace == -1) {
            throw new AuthenticationFailureException(INVALID_AUTHENTICATION_ERROR);
        }

        return userInfo.substring(0, firstSpace);
    }

    private String getPassword(String userInfo) {
        int firstSpace = userInfo.indexOf(" ");
        if (firstSpace == -1) {
            throw new AuthenticationFailureException(INVALID_AUTHENTICATION_ERROR);
        }

        return userInfo.substring(firstSpace + 1);
    }


    public User verifyAuthentication(String username, String password) {
        try {
            User user = userService.getUserByUsername(username);
            if (!user.getPassword().equals(password) || user.isDeleted()) {
                throw new AuthenticationFailureException(WRONG_USERNAME_OR_PASSWORD);
            }
            return user;
        } catch (EntityNotFoundException e) {
            throw new AuthenticationFailureException(WRONG_USERNAME_OR_PASSWORD);
        }

    }


    public User tryGetUser(HttpSession session) {
        String currentUser = (String)session.getAttribute("currentUser");

        if (currentUser == null){
            throw new AuthenticationFailureException("You should be logged in to perform this action!");
        }

        return userService.getUserByUsername(currentUser);
    }


    public User tryGetAdmin(HttpSession session) {
        String currentUser = (String)session.getAttribute("currentUser");

        if (currentUser == null){
            throw new AuthenticationFailureException("You should be logged in to perform this action!");
        }
        User user = userService.getUserByUsername(currentUser);

        if (!user.isAdmin()){
            throw new AuthenticationFailureException(INVALID_AUTHENTICATION_ERROR);
        }
        return user;
    }
}
