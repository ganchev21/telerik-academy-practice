package com.example.forumsystem.models.user.models;


import com.example.forumsystem.models.post.models.Post;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import jakarta.persistence.*;

import java.util.HashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

@Entity
@Table (name = "users")
@SecondaryTable (name =  "phones", pkJoinColumns = @PrimaryKeyJoinColumn (name = "user_id"))
public class User {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private int id;
    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "email")
    private String email;

    @Column(name = "username")
    private String username;
    @Column(name = "password")
    private String password;


    @JsonInclude (JsonInclude.Include.NON_EMPTY)
    @Column(name = "phone", table = "phones")
    private String phone;

    @JsonIgnore
    @OneToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    private UserPermissions permissions;


    public User() {
        this.permissions = new UserPermissions();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isAdmin() {
        return permissions.isAdmin();
    }



    public void setAdmin(boolean isAdmin){
        this.permissions.setAdmin(isAdmin);
    }

    public boolean isBlocked() {
        return permissions.isBlocked();
    }

    public void setBlocked(boolean isBlocked){
        this.permissions.setBlocked(isBlocked);
    }

    public boolean isDeleted() {
        return permissions.isDeleted();
    }

    public void setDeleted(boolean isDeleted){
        this.permissions.setDeleted(isDeleted);
    }

    public void setPermissions(UserPermissions permissions) {
        this.permissions = permissions;
    }
     public UserPermissions getPermissions() {
         return permissions;
     }

    public Optional<String> getPhone() {
        return Optional.ofNullable(phone);
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @PostPersist
    private void setUserId() {
        this.permissions.setUserId(this.id);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return this.id == user.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}

