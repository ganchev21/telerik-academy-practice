package com.example.forumsystem.models.post.models;

public class PostRatingDtoOut {

    private String title;

    private Double rating;

    private int peopleWhoRated;

    public int getPeopleWhoRated() {
        return peopleWhoRated;
    }

    public void setPeopleWhoRated(int peopleWhoRated) {
        this.peopleWhoRated = peopleWhoRated;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


}
