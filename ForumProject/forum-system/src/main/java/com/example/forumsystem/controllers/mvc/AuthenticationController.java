package com.example.forumsystem.controllers.mvc;

import com.example.forumsystem.exceptions.AuthenticationFailureException;
import com.example.forumsystem.exceptions.DuplicateEntityException;
import com.example.forumsystem.helpers.AuthenticationHelper;
import com.example.forumsystem.helpers.UserMapper;
import com.example.forumsystem.models.user.models.LoginDto;
import com.example.forumsystem.models.user.models.UserDtoIn;
import com.example.forumsystem.models.user.models.User;
import com.example.forumsystem.services.contracts.UserService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/auth")
public class AuthenticationController {

    private final UserService userService;
    private final UserMapper userMapper;
    private final AuthenticationHelper authenticationHelper;


    @Autowired
    public AuthenticationController(AuthenticationHelper authenticationHelper, UserMapper userMapper, UserService userService) {
        this.authenticationHelper = authenticationHelper;
        this.userService = userService;
        this.userMapper = userMapper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session){
        return session.getAttribute("currentUser") !=null;
    }

    @ModelAttribute
    public void config(HttpServletRequest request, Model model) {
        model.addAttribute("requestURI", request.getRequestURI());
    }
    @GetMapping("/login")
    public String showLoginPage(Model model){
        model.addAttribute("login",new LoginDto());
        return "user-login";
    }

    @PostMapping("/login")
    public String handleLogin(@Valid @ModelAttribute("login") LoginDto dto,
                              BindingResult bindingResult,
                              HttpSession session){
        if(bindingResult.hasErrors()){
            return "user-login";
        }

        try{
            User user =  authenticationHelper.verifyAuthentication(dto.getUsername(),dto.getPassword());
            session.setAttribute("currentUser", user.getUsername());
            session.setAttribute("userId", user.getId());
            session.setAttribute("isAdmin", user.isAdmin());
            session.setAttribute("isBlocked", user.isBlocked());
            return "redirect:/";
        } catch (AuthenticationFailureException e){
            bindingResult.rejectValue("username","auth_error", e.getMessage());
            return "user-login";
        }
    }

    @GetMapping("/logout")
    public String handleLogOut(HttpSession session){
        session.removeAttribute("currentUser");
        return "redirect:/";
    }


    @GetMapping("/register")
    public String showRegisterPage(Model model){
        model.addAttribute("register", new UserDtoIn());
        return "user-register";
    }

    @PostMapping("/register")
    public String handleRegister(@Valid @ModelAttribute("register") UserDtoIn register,
                                 BindingResult bindingResult){
        if (bindingResult.hasErrors()){
            return "user-register";
        }

        if (!register.getPassword().equals(register.getPasswordConfirm())){
            bindingResult.rejectValue("passwordConfirm", "password_error", "Passwords do not match");
            return "user-register";
        }

        try{
            User user = userMapper.fromDto(register);
            userService.createUser(user);
            return "redirect:/auth/login";
        } catch (DuplicateEntityException e){
            if (e.getErrorType().equals("username")){
                bindingResult.rejectValue("username","username_error", e.getMessage());
            } else if (e.getErrorType().equals("email")){
                bindingResult.rejectValue("email","email_error", e.getMessage());
            }
            return "user-register";
        }
    }
}
