package com.example.forumsystem.repositories;


import com.example.forumsystem.exceptions.EntityNotFoundException;
import com.example.forumsystem.models.tag.models.Tag;
import com.example.forumsystem.models.user.models.User;
import com.example.forumsystem.repositories.contracts.TagRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class TagRepositoryImpl implements TagRepository {

    private final SessionFactory sessionFactory;


    @Autowired
    public TagRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;

    }


    @Override
    public List<Tag> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Tag> query = session.createQuery("from Tag", Tag.class);
            return query.list();
        }
    }


    @Override
    public void createTag(Tag tag) {
        try (Session session = sessionFactory.openSession()) {
            session.save(tag);
        }
    }

    @Override
    public void updateTag(Tag tag) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(tag);
            session.getTransaction().commit();
        }
    }

    @Override
    public void deleteTag(Tag tag) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(tag);
            session.getTransaction().commit();
        }
    }

    @Override
    public Tag getTagByName(String name) {
        try (Session session = sessionFactory.openSession()) {

            Query<Tag> query = session.createQuery("from Tag where tag = :tag", Tag.class);
            query.setParameter("tag", name);

            List<Tag> result = query.list();

            if (result.size() == 0) {
                throw new EntityNotFoundException("Tag", "name", name);
            }

            return result.get(0);
        }
    }

    @Override
    public Tag getTagById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Tag tag = session.get(Tag.class, id);
            if (tag == null) {
                throw new EntityNotFoundException("Tag", id);
            }
            return tag;
        }
    }
}


