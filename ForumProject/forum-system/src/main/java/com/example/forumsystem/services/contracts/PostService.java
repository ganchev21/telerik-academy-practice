package com.example.forumsystem.services.contracts;

import com.example.forumsystem.models.FilterOptions;
import com.example.forumsystem.models.post.models.Post;
import com.example.forumsystem.models.user.models.User;

import java.util.List;
import java.util.Optional;

public interface PostService {
    List<Post> getAll();

    List<Post> filter(FilterOptions filterOptions);

    Post getPostById(int id);
    List <Post> getPostsByTag(String tag);

    void createPost(Post post);

    void updatePost(Post postFromDto, Post existingPost);

    void deletePost(User user, Post post);
    void likePost(User user, Post post);
    void dislikePost(User user, Post post);
//    void ratePost(User user, Post post, int rate);

    List<Post> getUserPosts(List<Post> posts, User user);

    List<Post> getMostCommented();

    List<Post> getMostRecentPosts();


    String getPostTitle(int id);

    String getPostContent(int id);


    void updatePostTags(Post post, List<String> tags);
    void removePostTags(Post post, List<String> tags);
}
