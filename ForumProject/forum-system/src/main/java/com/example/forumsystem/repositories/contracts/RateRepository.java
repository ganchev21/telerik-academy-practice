package com.example.forumsystem.repositories.contracts;

import com.example.forumsystem.models.post.models.AverageRating;
import com.example.forumsystem.models.post.models.PostRating;
import com.example.forumsystem.models.user.models.User;

import java.util.List;

public interface RateRepository {

    List<PostRating> getAllPostsRating ();
    Double getPostRating(int id);

    Double getAveragePostRating(int id);

    int getUserRate (int userId, int postId);

    int getPeopleRated (int id);

    PostRating getById(int userId, int postId);

    void createRate(PostRating postInteractions);

    void createAverageRate(AverageRating rating);

    boolean findRecord (int userId, int postId);

}
