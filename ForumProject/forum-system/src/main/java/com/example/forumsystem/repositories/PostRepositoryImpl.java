package com.example.forumsystem.repositories;


import com.example.forumsystem.exceptions.EntityNotFoundException;
import com.example.forumsystem.models.FilterOptions;
import com.example.forumsystem.models.comment.models.Comment;
import com.example.forumsystem.models.post.models.Post;
import com.example.forumsystem.models.user.models.User;
import com.example.forumsystem.repositories.contracts.PostRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class PostRepositoryImpl implements PostRepository {
    public static final String SORT_INCORRECT_MESSAGE = "Sort should have max 2 parameters divided with '-' symbol";
    public static final int topCommented = 10;
    private final SessionFactory sessionFactory;

    public PostRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Post> getMostCommentedPosts() {
        try (Session session = sessionFactory.openSession()) {
            Query<Comment> query = session.createQuery("from Comment group by post order by count(post) desc", Comment.class);
            query.setMaxResults(topCommented);

            List<Post> results = new ArrayList<>();
            for (Comment c : query.list()) {
                results.add(getPostById(c.getPost().getId()));
            }
            return results;

        }

    }


    @Override
    public List<Post> getMostRecentPosts() {
        try (Session session = sessionFactory.openSession()) {
            Query<Post> query = session.createQuery(
                    "from Post order by timestamp desc", Post.class).setMaxResults(10);
            return query.list();
        }
    }

    @Override
    public List<Post> filter(FilterOptions filterOptions) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder result = new StringBuilder("from Post where 1=1");
            Map<String, Object> parameters = new HashMap<>();
            ArrayList<String> filter = new ArrayList<>();

            filterOptions.getSearchQuery().ifPresent(value -> {
                filter.add(" (title like :query OR content like :query)");
                parameters.put("query", "%" + value + "%");
            });
            if (!filter.isEmpty()) {
                result.append(" and ").append(String.join(" and ", filter));
            }

            filterOptions.getSort().ifPresent(value -> result.append(generateSort("timestamp-" + value)));

            Query<Post> query = session.createQuery(result.toString(), Post.class);
            query.setProperties(parameters);
            return query.list();
        }
    }

    private String generateSort(String value) {

        StringBuilder query = new StringBuilder(" order by ");
        String[] params = value.split("-");

        if (value.isEmpty() || params.length > 2) {
            throw new UnsupportedOperationException(
                    SORT_INCORRECT_MESSAGE);
        }

        switch (params[0]) {
            case "title" -> query.append(" title ");
            case "content" -> query.append(" content ");
            case "timestamp" -> query.append(" timestamp ");
            default -> throw new UnsupportedOperationException(
                    SORT_INCORRECT_MESSAGE);
        }

        if (params.length == 2 && params[1].equalsIgnoreCase("desc")) {
            query.append(" desc ");
        }


        return query.toString();
    }


    @Override
    public List<Post> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Post> query = session.createQuery("from Post", Post.class);
            return query.list();
        }
    }

    @Override
    public Post getPostById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Post post = session.get(Post.class, id);
            if (post == null) {
                throw new EntityNotFoundException("Post", id);
            }
            return post;
        }
    }

    @Override
    public List<Post> getUserPosts(List<Post> posts, User user) {
        try (Session session = sessionFactory.openSession()) {
            Query<Post> query = session.createQuery("FROM Post p " +
                    "WHERE p.createdBy = :user", Post.class);
            query.setParameter("user", user);
            return query.list();
        }
    }


    @Override
    public void createPost(Post post) {
        try (Session session = sessionFactory.openSession()) {
            session.save(post);
        }
    }

    @Override
    public void updatePost(Post post) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(post);
            session.getTransaction().commit();
        }
    }

    @Override
    public void deletePost(Post post) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(post);
            session.getTransaction().commit();
        }
    }


    @Override
    public Post getPostByTitle(String title) {
        try (Session session = sessionFactory.openSession()) {
            Query<Post> query = session.createQuery("from Post where title = :title", Post.class);
            query.setParameter("title", title);

            List<Post> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Title", "title", title);
            }

            return result.get(0);
        }
    }

    @Override
    public List<Post> getPostsByTag(String tag) {
        try (Session session = sessionFactory.openSession()) {
            Query<Post> query = session.createQuery("select p from Post p join p.tags t where t.tag = :tag", Post.class);
            query.setParameter("tag", tag);
            return query.list();
        }
    }


}
