package com.example.forumsystem.models.user.models;

import jakarta.validation.constraints.NotNull;

public class UserPermissionsDto {


    private String isDeleted;

    private String isAdmin;

    private String isBlocked;



    public String getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(String isAdmin) {
        this.isAdmin = isAdmin;
    }

    public String getIsBlocked() {
        return isBlocked;
    }

    public void setIsBlocked(String isBlocked) {
        this.isBlocked = isBlocked;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }


}
