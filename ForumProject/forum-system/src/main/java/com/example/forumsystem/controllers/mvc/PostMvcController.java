package com.example.forumsystem.controllers.mvc;

import com.example.forumsystem.exceptions.AuthenticationFailureException;
import com.example.forumsystem.exceptions.DuplicateEntityException;
import com.example.forumsystem.exceptions.EntityNotFoundException;
import com.example.forumsystem.helpers.AuthenticationHelper;
import com.example.forumsystem.helpers.CommentMapper;
import com.example.forumsystem.helpers.PostMapper;
import com.example.forumsystem.helpers.ValidationHelper;
import com.example.forumsystem.models.FilterDto;
import com.example.forumsystem.models.FilterOptions;
import com.example.forumsystem.models.MvcPost;
import com.example.forumsystem.models.comment.models.Comment;
import com.example.forumsystem.models.comment.models.CommentDto;
import com.example.forumsystem.models.comment.models.CommentDtoIn;
import com.example.forumsystem.models.post.models.Post;
import com.example.forumsystem.models.post.models.PostDtoIn;
import com.example.forumsystem.models.post.models.PostRatingDtoIn;
import com.example.forumsystem.models.user.models.User;
import com.example.forumsystem.services.contracts.CommentService;
import com.example.forumsystem.services.contracts.PostService;
import com.example.forumsystem.services.contracts.RateService;
import com.example.forumsystem.services.contracts.UserService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Controller
@RequestMapping("/posts")
public class PostMvcController {

    private final PostService postService;
    private final RateService rateService;

    private final PostMapper postMapper;

    private final UserService userService;

    private final CommentMapper commentMapper;

    private final CommentService commentService;

    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public PostMvcController(PostService postService,
                             RateService rateService,
                             PostMapper postMapper,
                             UserService userService, CommentMapper commentMapper,
                             CommentService commentService,
                             AuthenticationHelper authenticationHelper) {
        this.postService = postService;
        this.rateService = rateService;
        this.postMapper = postMapper;
        this.userService = userService;
        this.commentMapper = commentMapper;
        this.commentService = commentService;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @GetMapping("/{id}")
    public String showPost(@PathVariable int id, Model model, HttpSession session) {
        boolean userIsLogged = true;
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            userIsLogged = false;
        }

        try {
            Post post = postService.getPostById(id);
            List<Comment> comments = commentService.getPostComments(id);
            StringBuilder rate = rateService.getPostRating(post);
            MvcPost mvcPost = postMapper.mvcPostDtoOut(post, rate, comments);


            model.addAttribute("ratePost", new PostRatingDtoIn());
            model.addAttribute("comment", new CommentDtoIn());
            model.addAttribute("post", mvcPost);
            model.addAttribute("tags", post.getTags());


            if (userIsLogged) {
                User user = authenticationHelper.tryGetUser(session);
                int rating = rateService.getUserRate(user, post.getId());
                if (rating > 0) {
                    model.addAttribute("rating", rating);
                    model.addAttribute("alreadyRated", true);
                } else {
                    model.addAttribute("alreadyRated", false);
                }
            }

            return "post";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }


    @GetMapping()
    public String showPosts(Model model) {
        try {

            List<Post> postList = postService.getAll();
            List<MvcPost> mvcPostList = convertPostToMvc(postList);

            model.addAttribute("posts", mvcPostList);
            return "posts";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/search")
    public String search(@RequestParam(name = "searchInput", required = false) String searchInput,
                         @RequestParam(name = "sortOrder", required = false) String sortOrder,
                         @RequestParam(name = "searchUser", required = false) String username,
                         @RequestParam(name = "searchTags", required = false) String tag,
                         Model model) {
        try {
            User user = null;
            if (username != null && !username.isEmpty()) {
                user = userService.getUserByUsername(username);
            }

            List<Post> postList;

            if (sortOrder != null && sortOrder.equals("mostCommented")) {
                postList = postService.getMostCommented();
            } else if (user != null) {
                postList = postService.getUserPosts(postService.getAll(), user);
                if (sortOrder != null && sortOrder.equals("desc")) {
                    Collections.reverse(postList);
                }
            } else if (tag != null && !tag.isEmpty()) {
                postList = postService.getPostsByTag(tag);
                if (sortOrder != null && sortOrder.equals("desc")) {
                    Collections.reverse(postList);
                }
            } else {
                FilterOptions filterOptions = new FilterOptions(searchInput, sortOrder);
                postList = postService.filter(filterOptions);
            }

            List<MvcPost> mvcPostList = convertPostToMvc(postList);

            model.addAttribute("filterOptions", new FilterDto(searchInput, sortOrder));
            model.addAttribute("posts", mvcPostList);
            return "posts";

        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/new")
    public String showNewPostPage(Model model, HttpSession session) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(session);
            if (loggedUser.isBlocked()) {
                throw new AuthenticationFailureException(ValidationHelper.BLOCKED_USER_CANNOT_PERFORM_THIS_ACTION);
            }
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("post", new PostDtoIn());
        return "create-post";
    }

    @PostMapping("/new")
    public String createPost(@Valid @ModelAttribute("post") PostDtoIn postDto,
                             BindingResult bindingResult, Model model, HttpSession session) {

        if (bindingResult.hasErrors()) {
            return "create-post";
        }

        try {
            User user = authenticationHelper.tryGetUser(session);
            Post post = postMapper.dtoToObject(new Post(), postDto, user);
            postService.createPost(post);

            return "redirect:/posts/" + post.getId();
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("title", "duplicate-post", e.getMessage());
            return "create-post";

        }
    }


    @PostMapping("/{postId}/comments/new")
    public String createComment(@Valid @ModelAttribute("comment")
                                CommentDtoIn commentDtoIn,
                                BindingResult bindingResult,
                                Model model,
                                HttpSession session) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("error", bindingResult.getFieldErrors().get(0).getDefaultMessage());
            return "not-found";
        }
        User loggedUser;
        try {
            loggedUser = authenticationHelper.tryGetUser(session);
            if (loggedUser.isBlocked()) {
                throw new AuthenticationFailureException(ValidationHelper.BLOCKED_USER_CANNOT_PERFORM_THIS_ACTION);
            }
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            Comment comment = commentMapper.dtoToObject(new Comment(), commentDtoIn, loggedUser);
            commentService.createComment(comment, loggedUser);
            return "redirect:/posts/{postId}";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("Title", "duplicate-comment", e.getMessage());
            return "redirect:/posts/{postId}";
        }
    }

    @PostMapping("/{postId}/comments/{commentId}/update")
    public String updateComment(@Valid @ModelAttribute("comment") CommentDto comment,
                                BindingResult bindingResult,
                                Model model,
                                HttpSession session,
                                @PathVariable int postId,
                                @PathVariable int commentId) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (bindingResult.hasErrors()) {
            model.addAttribute("error", bindingResult.getFieldErrors().get(0).getDefaultMessage());
            return "not-found";
        }

        try {
            Comment updatedComment = commentMapper.fromDto(comment, commentId);
            commentService.updateComment(updatedComment, user);
            return "redirect:/posts/{postId}";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/{postId}/update")
    public String showUpdatedPost(@PathVariable int postId,
                                  Model model,
                                  HttpSession session) {

        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            Post post = postService.getPostById(postId);
            PostDtoIn postDtoIn = postMapper.objectToDtoUpdateMethod(post, new PostDtoIn());
            model.addAttribute("post", postDtoIn);
            model.addAttribute("tags1", post.getTagSet());
            return "update-post";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/{postId}/update")
    public String updatePost(@Valid @ModelAttribute("post") PostDtoIn postDtoIn,
                             BindingResult bindingResult,
                             Model model,
                             HttpSession session,
                             @PathVariable int postId) {
        User loggeduser;
        try {
            loggeduser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            Post existingPost = postService.getPostById(postId);
            Post post = postMapper.dtoToObject(existingPost, postDtoIn, loggeduser);


            model.addAttribute("post", postDtoIn);
            model.addAttribute("tags1", post.getTagSet());
            if (bindingResult.hasErrors()) {
                if (bindingResult.hasFieldErrors("tagsToAdd")) {
                    return "update-post";
                } else if (bindingResult.hasFieldErrors("tagsToAdd*")) {
                    FieldError fieldError = bindingResult.getFieldError();
                    if (fieldError != null) {
                        String defaultMessage = fieldError.getDefaultMessage();
                        if (defaultMessage != null) {
                            bindingResult.rejectValue("tagsToAdd", "tags-error", defaultMessage);
                        }
                    }
                    return "update-post";
                }
            }

            postService.updatePost(post, existingPost);
            postService.updatePostTags(post, postDtoIn.getTagsToAdd());
            return "redirect:/posts/" + post.getId();
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("title", "duplicate-post", e.getMessage());


            return "update-post";
        }
    }

    @GetMapping("/{postId}/comments/{commentId}/delete")
    public String getDeleteComment(@PathVariable("postId") int postId,
                                   @PathVariable("commentId") int commentId,
                                   HttpSession session,
                                   Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            Comment comment = commentService.getCommentById(commentId);
            if (!comment.getUser().equals(user) && !user.isAdmin()) {
                return "access-denied";
            }
            model.addAttribute("comment", comment);
            model.addAttribute("postId", postId);
            commentService.deleteComment(user, comment);
            return "redirect:/posts/" + postId;
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/{postId}/delete")
    public String showDeletedPost(Model model,
                                  HttpSession session,
                                  @PathVariable int postId) {

        User loggedUser;
        try {
            loggedUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        try {
            Post post = postService.getPostById(postId);
            postService.deletePost(loggedUser, post);
            return "redirect:/posts";
        } catch (EntityNotFoundException | AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
        }
        return "not-found";
    }


    @GetMapping("/{postId}/like")
    public String likePost(@PathVariable int postId, HttpSession session, Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        try {
            Post post = postService.getPostById(postId);
            postService.likePost(user, post);
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
        return "redirect:/posts/" + postId;
    }


    @GetMapping("/{postId}/dislike")
    public String dislikePost(@PathVariable int postId, HttpSession session, Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        try {
            Post post = postService.getPostById(postId);
            postService.dislikePost(user, post);
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
        return "redirect:/posts/" + postId;
    }


    @PostMapping("/{postId}/tags/delete")
    public String deleteTags(
            PostDtoIn postDtoIn,
            Model model,
            HttpSession session,
            @PathVariable int postId) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            Post post = postService.getPostById(postId);
            postDtoIn = postMapper.objectToDtoDeleteMethod(post, postDtoIn);
            postService.removePostTags(post, postDtoIn.getTagsToRemove());
            return "redirect:/posts/{postId}";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }

    }


    @PostMapping("/{postId}/rate")
    public String ratePost(@Valid PostRatingDtoIn postRatingDtoIn,
                           BindingResult bindingResult,
                           Model model,
                           HttpSession session,
                           @PathVariable int postId) {

        User loggedUser;
        try {
            loggedUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (bindingResult.hasErrors()) {
            return "redirect:/posts/{postId}";
        }
        try {
            rateService.ratePost(loggedUser, postId, postRatingDtoIn.getRate());
            return "redirect:/posts/{postId}";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @ModelAttribute
    public void config(HttpServletRequest request, Model model) {
        model.addAttribute("requestURI", request.getRequestURI());
    }

    private List<MvcPost> convertPostToMvc(List<Post> postList) {
        List<MvcPost> mvcPostList = new ArrayList<>();
        for (Post post : postList) {
            StringBuilder rate = rateService.getPostRating(post);
            List<Comment> comments = commentService.getPostComments(post.getId());
            MvcPost mvcPost = postMapper.mvcPostDtoOut(post, rate, comments);
            mvcPostList.add(mvcPost);
        }
        return mvcPostList;
    }
}



