package com.example.forumsystem.services.contracts;

import com.example.forumsystem.models.tag.models.Tag;
import com.example.forumsystem.models.user.models.User;

import java.util.List;

public interface TagService {

    List<Tag> getAll();

    Tag getTagById(int id);
    Tag createTag(String tag);
    void createAll(User user, List<String> tagList);
    void updateTag(User user, Tag oldTag, String updatedTag);
    void deleteTag(User user, Tag tag);

}
