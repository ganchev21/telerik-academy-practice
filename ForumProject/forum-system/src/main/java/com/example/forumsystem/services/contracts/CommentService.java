package com.example.forumsystem.services.contracts;


import com.example.forumsystem.models.comment.models.Comment;
import com.example.forumsystem.models.comment.models.CommentDtoIn;
import com.example.forumsystem.models.user.models.User;

import java.util.List;

public interface CommentService {

//    List<Comment> getAll();
    Comment getCommentById(int id);

    void createComment(Comment comment, User loggedUser);

    void updateComment(Comment comment, User user);

    void deleteComment(User user, Comment comment);

    List<Comment> getUserComments(User user, int id);

    List<Comment> getPostComments(int id);

}
