package com.example.forumsystem.helpers;

import com.example.forumsystem.exceptions.AuthenticationFailureException;
import com.example.forumsystem.exceptions.EntityNotFoundException;
import com.example.forumsystem.models.user.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ValidationHelper {

    public static final String NON_ADMIN_PHONE_UPDATE = "Only admin accounts can have phone numbers";
    public static final String MODIFY_ERROR_MESSAGE = "Only admin or owner can perform this action";
    public static final String ADMIN_ACCESS_REQUIRED_ERROR = "Only admin accounts can perform this action";
    public static final String BLOCKED_USER_CANNOT_PERFORM_THIS_ACTION = "Blocked user cannot perform this action!";


    @Autowired
    public ValidationHelper() {

    }


    public static void checkUserPermissions(User loggedUser, User userTryingToModify) {
        if (!(loggedUser.isAdmin() || userTryingToModify.getId() == loggedUser.getId())) {
            throw new AuthenticationFailureException(MODIFY_ERROR_MESSAGE);
        }
        if (userTryingToModify.isDeleted()){
            throw new EntityNotFoundException("User", "username" , userTryingToModify.getUsername());
        }
    }


    public static void checkAddedPhonePermissions(User user) {
        if (user.getPhone().isPresent() && !user.isAdmin()){
            throw new AuthenticationFailureException(NON_ADMIN_PHONE_UPDATE);
        }
    }

    public static void validateUserIsAdmin(User user) {
        if (!user.isAdmin()) {
            throw new AuthenticationFailureException(ADMIN_ACCESS_REQUIRED_ERROR);
        }
    }

    public static void validateBlockedUser(User user) {
        if (user.isBlocked()) {
            throw new AuthenticationFailureException(BLOCKED_USER_CANNOT_PERFORM_THIS_ACTION);
        }
    }



}
