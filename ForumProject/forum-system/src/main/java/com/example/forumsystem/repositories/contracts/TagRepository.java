package com.example.forumsystem.repositories.contracts;

import com.example.forumsystem.models.tag.models.Tag;

import java.util.List;

public interface TagRepository {
    List<Tag> getAll();
    void createTag(Tag tag);
    void updateTag(Tag tag);

     void deleteTag(Tag tag);

     Tag getTagByName (String name);
     Tag getTagById(int id);
}
