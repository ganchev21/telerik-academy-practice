package com.example.forumsystem.controllers.mvc;

import com.example.forumsystem.helpers.PostMapper;
import com.example.forumsystem.models.post.models.Post;
import com.example.forumsystem.models.post.models.PostDtoOut;
import com.example.forumsystem.models.user.models.User;
import com.example.forumsystem.services.contracts.PostService;
import com.example.forumsystem.services.contracts.RateService;
import com.example.forumsystem.services.contracts.UserService;
import jakarta.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/")
public class HomeMvcController {

    private final RateService rateService;
    private final UserService userService;
    private final PostService postService;
    private final PostMapper postMapper;

    public HomeMvcController(RateService rateService, UserService userService, PostService postService, PostMapper postMapper) {
        this.rateService = rateService;
        this.userService = userService;
        this.postService = postService;
        this.postMapper = postMapper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session){
        return session.getAttribute("currentUser") !=null;
    }



    @GetMapping()
    public String showHomePage(Model model){
        List<PostDtoOut> topRated = rateService.getAllPostsRating();
        List<PostDtoOut> post = postMapper.listToDtoOut(topRated);
        List<Post> allPosts = postService.getAll();
        List<User> allUsers = userService.getAll();
        model.addAttribute("homePost", post);
        model.addAttribute("allPosts", allPosts);
        model.addAttribute("allUsers", allUsers);
        return "home-page";
    }

    @GetMapping("/admin")
    public String showAdminPage(HttpSession session, Model model){
        return "admin-portal";
    }

}


