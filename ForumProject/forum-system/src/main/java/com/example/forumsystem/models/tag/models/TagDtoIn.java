package com.example.forumsystem.models.tag.models;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;

import java.util.List;

public class TagDtoIn {

    private List<@Pattern(regexp = "^[A-Za-z]{3,20}$", message ="Tags must be between 3 and 20 symbols, no spaces!")String> tags;

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }
}
