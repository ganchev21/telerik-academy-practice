package com.example.forumsystem.models;

import java.util.Optional;

public class FilterOptions {

    private Optional<String> searchQuery;

    private Optional<String> sort;


    public FilterOptions(String searchQuery, String sort) {
        this.searchQuery = Optional.ofNullable(searchQuery);
        this.sort = Optional.ofNullable(sort);
    }

    public Optional<String> getSearchQuery() {
        return searchQuery;
    }

    public void setSearchQuery(Optional<String> searchQuery) {
        this.searchQuery = searchQuery;
    }

    public Optional<String> getSort() {
        return sort;
    }

    public void setSort(Optional<String> sort) {
        this.sort = sort;
    }

}
