package com.example.forumsystem.controllers.rest;


import com.example.forumsystem.exceptions.AuthenticationFailureException;
import com.example.forumsystem.exceptions.DuplicateEntityException;
import com.example.forumsystem.exceptions.EntityNotFoundException;
import com.example.forumsystem.helpers.AuthenticationHelper;
import com.example.forumsystem.helpers.PostMapper;
import com.example.forumsystem.models.FilterOptions;
import com.example.forumsystem.models.post.models.*;
import com.example.forumsystem.models.user.models.User;
import com.example.forumsystem.services.contracts.RateService;
import com.example.forumsystem.services.contracts.PostService;
import com.example.forumsystem.services.contracts.UserService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/api/posts")
public class PostController {
    private final PostService postService;
    private final UserService userService;
    private final PostMapper postMapper;
    private final RateService rateService;
    private final AuthenticationHelper authenticationHelper;

    //TODO create an endpoint to update tags on post only (without actual body of the post) /posts/"{id}"/tags
    @Autowired
    public PostController(PostService postService, UserService userService,
                          AuthenticationHelper authenticationHelper, PostMapper postMapper,
                          RateService rateService) {
        this.postService = postService;
        this.userService = userService;
        this.postMapper = postMapper;
        this.authenticationHelper = authenticationHelper;
        this.rateService = rateService;
    }

//    @GetMapping("/filter")
//    public List<PostDtoOut> filter(String query,
//                                   String sort) {
//        FilterOptions filterOptions= new FilterOptions(query,sort);
//        return postMapper.getPostDtoOutList(postService.filter(filterOptions));
//    }


    @GetMapping("/recent")
    public List<PostDtoOut> getMostRecentPosts() {
        List<Post> posts = postService.getMostRecentPosts();
        return postMapper.getPostDtoOutList(posts);
    }

    @GetMapping("/mostCommented")
    public List<PostDtoOut> getMostCommented() {
        List<Post> posts = postService.getMostCommented();
        return postMapper.getPostDtoOutList(posts);
    }


//    @GetMapping("/rated")
//    public List<PostRatingDtoOut> getTopRatedPost() {
//        Map<Double, String> map = interactionService.getAllPostsRating();
//
//        return postMapper.mapToDto(map);
//
//    }


    @GetMapping
    public List<PostDtoOut> getAll() {
        List<Post> allPosts = postService.getAll();
        return postMapper.getPostDtoOutList(allPosts);
    }

    @GetMapping("/{id}")
    public PostDtoOut getPost(@PathVariable int id) {
        try {
            Post post = postService.getPostById(id);
            return postMapper.postDtoOut(post);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}/title")
    public String getPostTitle(@PathVariable int id) {
        try {
            return postService.getPostTitle(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}/content")
    public String getPostContent(@PathVariable int id) {
        try {
            return postService.getPostContent(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}/rating")
    public PostRatingDtoOut averagePostRating(@PathVariable int id) {
        try {
            Post post = postService.getPostById(id);
            StringBuilder result = rateService.getPostRating(post);

            return postMapper.objectToDtoDeleteMethod(result);

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/user/{id}")
    public PostUserDtoOut getUserPosts(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        User user = userService.getUserById(id);
        User loggedUser = authenticationHelper.tryGetUser(headers);
        return postMapper.postUserDtoOut(postService.getUserPosts(postService.getAll(), user),
                loggedUser, new HashMap<>());
    }

    @PostMapping
    public void create(@RequestHeader HttpHeaders headers, @Valid @RequestBody PostDtoIn postDtoIn) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            Post post = postMapper.dtoToObject(new Post(), postDtoIn, loggedUser);
            postService.createPost(post);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (AuthenticationFailureException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

//    @GetMapping("/{id}/userRating")
//    public int getUserRating (@RequestHeader HttpHeaders headers, @PathVariable int id) {
//            User loggedUser = authenticationHelper.tryGetUser(headers);
//            int rating = rateService.getUserRate(loggedUser, id);
//
//            System.out.println(rating);
//            return rating;
//    }

    @PutMapping("/{id}")
    public Post updatePost(@RequestHeader HttpHeaders headers, @PathVariable int id,
                           @Valid @RequestBody PostDtoIn postDtoIn) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            Post post = postMapper.dtoToObject(id, postDtoIn, loggedUser);
            Post existingPost = postService.getPostById(id);

            postService.updatePost(post, existingPost);
            postService.updatePostTags(post, postDtoIn.getTagsToAdd());
            return post;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (AuthenticationFailureException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    //api/posts/id/like
    //api/posts/id/dislike
    @PutMapping("/{id}/like")
    public void likePost(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            Post post = postService.getPostById(id);
            postService.likePost(loggedUser, post);
            // interactionService.likeDislikePost(loggedUser, postInteractionsDtoIn.getPostId());
        } catch (AuthenticationFailureException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/{id}/dislike")
    public void dislikePost(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            Post post = postService.getPostById(id);
            postService.dislikePost(loggedUser, post);
        } catch (AuthenticationFailureException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/{id}/rate")
    public void ratePost(@RequestHeader HttpHeaders headers, @PathVariable int id,
                         @Valid @RequestBody PostRatingDtoIn postRatingDtoIn) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            rateService.ratePost(loggedUser, id, postRatingDtoIn.getRate());
        } catch (AuthenticationFailureException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            Post post = postService.getPostById(id);
            postService.deletePost(loggedUser, post);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthenticationFailureException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }


}
