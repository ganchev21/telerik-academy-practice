package com.example.forumsystem.helpers;

import com.example.forumsystem.models.MvcPost;
import com.example.forumsystem.models.comment.models.Comment;
import com.example.forumsystem.models.post.models.*;
import com.example.forumsystem.models.user.models.User;
import org.springframework.stereotype.Component;

import java.time.format.DateTimeFormatter;
import java.util.*;

@Component
public class PostMapper {

    public Post dtoToObject(int id, PostDtoIn postDtoIn, User user) {
        Post post = new Post();
        post.setId(id);
        return dtoToObject(post, postDtoIn, user);
    }

    public Post dtoToObject(Post post, PostDtoIn postDtoIn, User user) {
        post.setTitle(postDtoIn.getTitle());
        post.setContent(postDtoIn.getContent());
        post.setCreatedBy(user);
        return post;
    }

    public PostDtoOut postDtoOut(Post post) {
        PostDtoOut postDtoOut = new PostDtoOut();

        postDtoOut.setContent(post.getContent());
        postDtoOut.setTitle(post.getTitle());
        postDtoOut.setCreatedBy(post.getCreatedBy().getUsername());
        postDtoOut.setLikes(post.getLikes().size());
        postDtoOut.setTimestamp(post.getTimestamp());

        return postDtoOut;
    }

    public List<PostDtoOut> getPostDtoOutList(List<Post> postList) {
        List<PostDtoOut> postDtoOutList = new ArrayList<PostDtoOut>();

        for (Post post : postList) {
            PostDtoOut postDtoOut = new PostDtoOut();

            postDtoOut.setTitle(post.getTitle());
            postDtoOut.setContent(post.getContent());
            postDtoOut.setCreatedBy(post.getCreatedBy().getUsername());
            postDtoOut.setLikes(post.getLikes().size());
            postDtoOut.setTimestamp(post.getTimestamp());
            postDtoOutList.add(postDtoOut);
        }
        return postDtoOutList;
    }

    public PostUserDtoOut postUserDtoOut(List<Post> posts, User user, HashMap<String, List<String>> map) {
        PostUserDtoOut postUserDtoOut = new PostUserDtoOut();
        map.put(user.getUsername(), new ArrayList<>());
        for (Post post : posts) {
            if (user.getUsername().equals(post.getCreatedBy().getUsername())) {
                map.get(user.getUsername()).add(post.getTitle() + "   [Created-On: " + post.getTimestamp().
                        format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + "]");
            }
        }

        for (Map.Entry<String, List<String>> mapper : map.entrySet()) {
            postUserDtoOut.setUsername(mapper.getKey());
            postUserDtoOut.setPostsWhoCreated(mapper.getValue());
        }

        return postUserDtoOut;

    }

    public PostRatingDtoOut objectToDtoDeleteMethod(StringBuilder sb) {
        PostRatingDtoOut output = new PostRatingDtoOut();

        int firstSpecialChar = sb.indexOf("@");
        int secondSpecialChar = sb.indexOf("*");
        String title = sb.substring(0, firstSpecialChar);
        String rating = sb.substring(firstSpecialChar + 1, secondSpecialChar);
        String peopleWhoRated = sb.substring(secondSpecialChar + 1);
        output.setTitle(title);
        output.setRating(Double.parseDouble(rating.replace(',','.')));
        output.setPeopleWhoRated(Integer.parseInt(peopleWhoRated));

        return output;

    }

    public List<PostRatingDtoOut> mapToDto(Map<Double, String> map) {
        int counter = 0;
        List<PostRatingDtoOut> resultList = new ArrayList<>();

        for (Map.Entry<Double, String> mapList : map.entrySet()) {
            counter++;
            PostRatingDtoOut toBeAdded = new PostRatingDtoOut();
            toBeAdded.setRating(mapList.getKey());
            toBeAdded.setTitle(mapList.getValue());
            resultList.add(toBeAdded);
            if (counter == 3) {
                break;
            }
        }
        return resultList;
    }

    public MvcPost mvcPostDtoOut (Post post, StringBuilder rate, List<Comment> comments) {
        PostRatingDtoOut rating = objectToDtoDeleteMethod(rate);
        MvcPost mvcPost = new MvcPost();
        mvcPost.setId(post.getId());
        mvcPost.setTitle(post.getTitle());
        mvcPost.setContent(post.getContent());
        mvcPost.setTimestamp(post.getTimestamp());
        mvcPost.setCreatedBy(post.getCreatedBy());
        mvcPost.setCommentsList(comments);
        mvcPost.setRating(rating);
        mvcPost.setLikes(post.getLikes());
        mvcPost.setTags(post.getTagSet());
        mvcPost.setDislikes(post.getDislikes());
        return mvcPost;
    }

    public List<PostDtoOut> listToDtoOut (List<PostDtoOut> list) {
        List<PostDtoOut> resultList = new ArrayList<>();

        int counter = 0;

        for (PostDtoOut element : list) {
            PostDtoOut post = new PostDtoOut();
            counter++;
            post.setRating(element.getRating());
            post.setTitle(element.getTitle());
            post.setContent(element.getContent());
            post.setCreatedBy(element.getCreatedBy());
            post.setCreatedById(element.getCreatedById());
            post.setTimestamp(element.getTimestamp());
            post.setPostId(element.getPostId());
            resultList.add(post);
            if (counter == 5) {
                break;
            }
        }
        return resultList;
    }
    public PostDtoIn objectToDtoDeleteMethod(Post post, PostDtoIn postDtoIn) {
        postDtoIn.setTitle(post.getTitle());
        postDtoIn.setContent(post.getContent());
        List<String> tags = post.getTags().stream().map(tag -> tag.getTag() + "").toList();
        postDtoIn.setTagsToAdd(tags);
        return postDtoIn;
    }

    public PostDtoIn objectToDtoUpdateMethod(Post post, PostDtoIn postDtoIn) {
        postDtoIn.setTitle(post.getTitle());
        postDtoIn.setContent(post.getContent());
//        List<String> tags = post.getTags().stream().map(tag -> tag.getTag() + "").toList();
//        postDtoIn.setTagsToAdd(tags);
        return postDtoIn;
    }

}
