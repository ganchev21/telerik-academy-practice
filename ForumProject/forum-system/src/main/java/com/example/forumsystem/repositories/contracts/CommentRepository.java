package com.example.forumsystem.repositories.contracts;

import com.example.forumsystem.models.comment.models.Comment;
import com.example.forumsystem.models.user.models.User;

import java.util.List;

public interface CommentRepository {
    List<Comment> getAll();
    Comment getCommentById(int id);

    void createComment(Comment comment);

    void updateComment(Comment comment);

    void deleteComment(Comment comment);

    List<Comment> getUserComments(User user);

    List<Comment> getPostComments(int id);

}
