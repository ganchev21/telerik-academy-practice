package com.example.forumsystem.models.user.models;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;

public class UserDtoIn extends LoginDto {



    @NotNull(message = "Firstname can't be empty")
    @Pattern(regexp = "^[A-Za-zÀ-ÿ\\s]{4,32}$", message = "First name should be between 4 and 32 characters long and not contain special characters")
    private String firstName;
    @NotNull(message = "Lastname can't be empty")
    @Pattern(regexp = "^[A-Za-zÀ-ÿ\\s]{4,32}$", message = "Lastname should be between 4 and 32 characters long and not contain special characters")
    private String lastName;
    @NotNull(message = "Email can't be empty")
    @Pattern(regexp = "[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}", message = "Email should be in a valid format")
    private String email;

    @Pattern(regexp = "^$|^[+](\\d{1,3}\\s?){1,5}$", message = "Phone number should be between 8 and 12 digits and start with +")
    private String phone;

    @NotNull(message = "Password cannot be empty")
    @Size(min = 8, message = "Password should be at least 8 characters long")
    private String passwordConfirm;


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }


    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

}
