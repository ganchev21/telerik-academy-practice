package com.example.forumsystem.models;

import org.springframework.stereotype.Component;

@Component
public class FilterUserOptionsDto {

    private String firstName;

    private String lastName;

    private String email;

    private String username;

    private String sort;

    public FilterUserOptionsDto(String firstName, String lastName, String email, String username, String sort) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.username = username;
        this.sort = sort;
    }

    public FilterUserOptionsDto(String firstName, String email, String username) {
        this.firstName=firstName;
        this.email=email;
        this.username=username;
    }

    public FilterUserOptionsDto() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }
}
