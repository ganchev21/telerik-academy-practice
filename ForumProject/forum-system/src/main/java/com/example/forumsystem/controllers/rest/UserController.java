package com.example.forumsystem.controllers.rest;

import com.example.forumsystem.exceptions.AuthenticationFailureException;
import com.example.forumsystem.exceptions.DuplicateEntityException;
import com.example.forumsystem.exceptions.EntityNotFoundException;
import com.example.forumsystem.exceptions.UniqueEmailException;
import com.example.forumsystem.helpers.AuthenticationHelper;
import com.example.forumsystem.helpers.UserMapper;
import com.example.forumsystem.models.user.models.UserDtoIn;
import com.example.forumsystem.models.user.models.User;
import com.example.forumsystem.services.contracts.UserService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private final UserService userService;
    private final UserMapper userMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public UserController(UserService userService, UserMapper userMapper, AuthenticationHelper authenticationHelper) {
        this.userService = userService;
        this.userMapper = userMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<User> getAll() {
        return userService.getAll();
    }

//    @GetMapping("/filter")
//
//    public List<User> filter(
//            @RequestParam(required = false) Optional<String> firstname,
//            @RequestParam(required = false) Optional<String> lastname,
//            @RequestParam(required = false) Optional<String> email,
//            @RequestParam(required = false) Optional<String> username,
//            @RequestParam(required = false) Optional<String> sort) {
//
//        return userService.filter(firstname, lastname, email, username, sort);
//    }


    @GetMapping("/{id}")
    public User getUser(@PathVariable int id) {
        try {
            return userService.getUserById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }


    @PostMapping
    public User create(@Valid @RequestBody UserDtoIn userDtoIn) {
        try {
            User user = userMapper.fromDto(userDtoIn);
            userService.createUser(user);
            return user;
        } catch (UniqueEmailException e) {
            throw new ResponseStatusException(HttpStatus.FOUND, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }


    @PutMapping("/{id}")
    public User update(@RequestHeader HttpHeaders headers,
                       @PathVariable int id,
                       @Valid @RequestBody UserDtoIn userDtoIn) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            User existingUser = getUser(id);

            User userToUpdate = userMapper.fromDto(existingUser, userDtoIn);

            userToUpdate.setPermissions(existingUser.getPermissions());

            userService.updateUser(loggedUser, userToUpdate);

            return userToUpdate;
        } catch (UniqueEmailException e) {
            throw new ResponseStatusException(HttpStatus.FOUND, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (AuthenticationFailureException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            User userToDelete = getUser(id);
            userService.deleteUser(loggedUser, userToDelete);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthenticationFailureException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }


    @PutMapping("/{id}/permissions")
    public void updatePermissions(@RequestHeader HttpHeaders headers, @PathVariable int id,
                                  @RequestBody String action) {
        try {

            User loggedUser = authenticationHelper.tryGetUser(headers);
            User updatedUser = userMapper.updatePermissions(getUser(id), action);

            userService.updatePermissions(loggedUser, updatedUser);
        } catch (UniqueEmailException e) {
            throw new ResponseStatusException(HttpStatus.FOUND, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

}
