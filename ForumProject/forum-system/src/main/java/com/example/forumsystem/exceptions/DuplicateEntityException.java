package com.example.forumsystem.exceptions;

public class DuplicateEntityException extends RuntimeException {

    private final String errorType;
    public DuplicateEntityException(String type, String attribute, String value) {
        super(String.format("%s with %s %s already exist!", type, attribute, value));
        this.errorType = attribute;
    }

    public String getErrorType(){
        return errorType;
    }
}
