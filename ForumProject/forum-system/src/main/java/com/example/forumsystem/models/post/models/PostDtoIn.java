package com.example.forumsystem.models.post.models;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;

import java.util.List;


public class PostDtoIn {

    //TODO FIX CONSTRAINTS TO PROJECT SPECIFICS
    @NotNull(message = "Firstname can't be empty")
    @Size(min = 16, max = 64, message = "Post title should be between 16 and 64 symbols")
    private String title;
    @NotNull
    @Size(min = 32, max = 8192, message = "Content should be between 32 and 8192 symbols")
    private String content;

    @Size (min=0,max=50, message ="Max 50 tags can be added at once")
    private List<@Pattern(regexp = "^[A-Za-z]{3,20}$", message ="Tags must be between 3 and 20 symbols, no spaces!")String> tagsToAdd;

    private List<String> tagsToRemove;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<String> getTagsToAdd() {
        return tagsToAdd;
    }

    public void setTagsToAdd(List<String> tagsToAdd) {
        this.tagsToAdd = tagsToAdd;
    }

    public List<String> getTagsToRemove() {
        return tagsToRemove;
    }

    public void setTagsToRemove(List<String> tagsToRemove) {
        this.tagsToRemove = tagsToRemove;
    }
}
