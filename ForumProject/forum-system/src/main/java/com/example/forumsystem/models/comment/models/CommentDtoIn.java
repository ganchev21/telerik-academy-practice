package com.example.forumsystem.models.comment.models;


import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Positive;
import jakarta.validation.constraints.Size;

public class CommentDtoIn {

    @Size(min = 1, message = "You cannot write an empty comment")
    private String content = "";
    @Positive
    private int postId;


    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }


    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

}
