package com.example.forumsystem.models.post.models;

import com.example.forumsystem.models.user.models.User;
import jakarta.persistence.Embeddable;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;

import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class PostRatingID implements Serializable {

    @OneToOne
    @JoinColumn(name = "post_id")
    private Post post;
    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PostRatingID interactionID = (PostRatingID) o;
        return post.id == interactionID.post.getId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(post.id);
    }
}
