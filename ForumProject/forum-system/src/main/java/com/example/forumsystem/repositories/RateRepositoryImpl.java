package com.example.forumsystem.repositories;

import com.example.forumsystem.exceptions.EntityNotFoundException;
import com.example.forumsystem.models.post.models.AverageRating;
import com.example.forumsystem.models.post.models.PostRating;
import com.example.forumsystem.models.user.models.User;
import com.example.forumsystem.repositories.contracts.RateRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RateRepositoryImpl implements RateRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public RateRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public List<PostRating> getAllPostsRating() {
        try (Session session = sessionFactory.openSession()) {
            Query<PostRating> query = session.createQuery("from PostRating p " +
                    "where p.rate != 0 ", PostRating.class);

            return query.list();
        }
    }

    @Override
    public Double getPostRating(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<Object> query = session.createQuery("SELECT AVG(p.rate) FROM PostRating p " +
                    "WHERE p.id.post.id = :postId ", Object.class);
            query.setParameter("postId", id);

//            Double result = Double.parseDouble(query.getSingleResult().toString());
//            return result;

            return query.list().get(0) == null ? 0.00 : query.list().stream()
                    .mapToDouble(o -> Double.parseDouble("" + o))
                    .boxed().toList().get(0);



        }
    }

    public int getPeopleRated (int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<PostRating> query = session.createQuery("from PostRating p " +
                    "where p.id.post.id = :postId", PostRating.class);
            query.setParameter("postId", id);

            return query.list().size();
        }
    }

    @Override
    public Double getAveragePostRating(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<Object> query = session.createQuery("select a.averageRating from AverageRating a " +
                    "where a.id = :postId ", Object.class);
            query.setParameter("postId", id);

            return query.list().isEmpty() ? 0.00 : query.list().stream()
                    .mapToDouble(o -> Double.parseDouble("" + o))
                    .boxed().toList().get(0);

        }
    }

    @Override
    public int getUserRate (int userId, int postId) {
        try (Session session = sessionFactory.openSession()) {

            Query<Object> query = session.createQuery("select p.rate from PostRating p " +
                    "where p.id.post.id = :postId and p.id.user.id = :userId", Object.class);
            query.setParameter("postId", postId);
            query.setParameter("userId", userId);

            return query.list().isEmpty() ? 0 : query.list().stream()
                    .mapToInt(o -> Integer.parseInt("" + o))
                    .boxed().toList().get(0);
        }
    }

    @Override
    public PostRating getById(int userId, int postId) {
        try (Session session = sessionFactory.openSession()) {

            Query<PostRating> query = session.createQuery("from PostRating p " +
                    " where p.id.post.id = :postId and p.id.user.id = :userId", PostRating.class);
            query.setParameter("postId", postId);
            query.setParameter("userId", userId);
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException("Interaction", "with keys", String.format("%d and %d", userId, postId));
            }
            return query.list().get(0);
        }
    }

    @Override
    public void createRate(PostRating postInteractions) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(postInteractions);
            session.getTransaction().commit();
        }
    }

    @Override
    public void createAverageRate(AverageRating rating) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.saveOrUpdate(rating);
            session.getTransaction().commit();
        }
    }

    @Override
    public boolean findRecord (int userId, int postId) {
        try (Session session = sessionFactory.openSession()) {
            Query<PostRating> query = session.createQuery("from PostRating p " +
                    "where p.id.user.id = :userId and p.id.post.id = :postId", PostRating.class);
            query.setParameter("userId", userId);
            query.setParameter("postId", postId);

            return query.list().isEmpty();
        }
    }
}
