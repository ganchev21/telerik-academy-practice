package com.example.forumsystem.models.post.models;

import jakarta.persistence.Column;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;

import java.util.Objects;

@Entity
@Table(name = "all_rates")
public class PostRating {

    @EmbeddedId
    private PostRatingID id = new PostRatingID();
    @Column(name = "post_rated")
    private int rate;


    public PostRating() {
    }

    public PostRatingID getId() {
        return id;
    }

    public void setId(PostRatingID id) {
        this.id = id;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PostRating interactions = (PostRating) o;
        return id == interactions.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }


}
