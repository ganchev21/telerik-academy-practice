package com.example.forumsystem.exceptions;

public class UniqueEmailException extends RuntimeException {
    public UniqueEmailException() {
        super("Email must be unique!");
    }
}
