package com.example.forumsystem.services;

import com.example.forumsystem.exceptions.AuthenticationFailureException;
import com.example.forumsystem.exceptions.DuplicateEntityException;
import com.example.forumsystem.exceptions.EntityNotFoundException;
import com.example.forumsystem.models.user.models.User;
import com.example.forumsystem.repositories.contracts.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.example.forumsystem.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTests {
    @Mock
    UserRepository mockRepository;

    @InjectMocks
    UserServiceImpl service;
    @Test
    public void getById_Should_ReturnUser_When_MatchExists() {
        //Arrange
        User mockUser = createMockUser();
        Mockito.when(mockRepository.getUserById(1))
                .thenReturn(mockUser);

        //Act
        User user = service.getUserById(1);

        //Assert
        Assertions.assertEquals(1, user.getId());
        Assertions.assertEquals(mockUser.getFirstName(), user.getFirstName());
        Assertions.assertEquals(mockUser.getLastName(), user.getLastName());
        Assertions.assertEquals(mockUser.getUsername(), user.getUsername());
        Assertions.assertEquals(mockUser.getEmail(), user.getEmail());
    }


    @Test
    public void getByUsername_Should_ReturnUser_When_MatchExists() {
        //Arrange
        User mockUser = createMockUser();
        Mockito.when(mockRepository.getUserByUsername(Mockito.anyString()))
                .thenReturn(mockUser);

        //Act
        User user = service.getUserByUsername(mockUser.getUsername());

        //Assert
        Assertions.assertEquals(mockUser.getUsername(), user.getUsername());
    }

    @Test
    public void create_Should_ThrowException_When_UserWithUsernameExists() {
        //Arrange
        User firstUser = createMockUser();
        User secondUser = createMockUser();
        secondUser.setId(2);

        Mockito.when(mockRepository.getUserByUsername(Mockito.anyString()))
                .thenReturn(firstUser);

        //Act, assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.createUser(secondUser));
    }

    @Test
    public void create_Should_ThrowException_When_UserWithEmailExists() {
        //Arrange
        User firstUser = createMockUser();
        User secondUser = createMockUser();
        secondUser.setId(2);

        Mockito.when(mockRepository.getUserByUsername(Mockito.anyString()))
                .thenReturn(secondUser);
        Mockito.when(mockRepository.getUserByEmail(Mockito.anyString()))
                .thenReturn(firstUser);

        //Act, assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.createUser(secondUser));
    }


    @Test
    public void create_Should_CallRepository_When_NewUserIsCreated() {
        //Arrange
        User user = createMockUser();

        Mockito.when(mockRepository.getUserByUsername(Mockito.anyString()))
                .thenThrow(new EntityNotFoundException());
        Mockito.when(mockRepository.getUserByEmail(Mockito.anyString()))
                .thenThrow(new EntityNotFoundException());
        //Act
        service.createUser(user);

        //Аssert
        Mockito.verify(mockRepository,Mockito.times(1))
                .createUser(Mockito.any(User.class));
    }

    @Test
    public void getAll_Should_CallRepository_When_AllUsersAreRequested() {
        service.getAll();

        Mockito.verify(mockRepository,Mockito.times(1))
                .getAll();
    }

    @Test
    public void update_Should_ThrowException_When_UsernameIsUpdated() {
        //Arrange
        User user = createMockUser();
        user.setUsername("mockUser2");

        Mockito.when(mockRepository.getUserById(Mockito.anyInt()))
                .thenReturn(user);

        //Act, Assert
        Assertions.assertThrows(AuthenticationFailureException.class, () ->
                service.updateUser(createMockUser(),createMockUser()));
    }


    @Test
    public void update_Should_ThrowException_When_PhoneAddedToNonAdminUser() {
        //Arrange
        User user = createMockUser();
        user.setPhone("+12345678");

        Mockito.when(mockRepository.getUserById(Mockito.anyInt()))
                .thenReturn(user);

        //Act, Аssert
        Assertions.assertThrows(AuthenticationFailureException.class, () ->
                service.updateUser(user,user));
    }

    @Test
    public void update_Should_CallRepository_When_ExistingUserIsUpdated() {
        //Arrange
        User user = createMockUser();


        Mockito.when(mockRepository.getUserByUsername(Mockito.anyString()))
                .thenReturn(user);
        Mockito.when(mockRepository.getUserByEmail(Mockito.anyString()))
                .thenReturn(user);
        Mockito.when(mockRepository.getUserById(Mockito.anyInt()))
                .thenReturn(user);

        //Act
        service.updateUser(user,user);

        //Аssert
        Mockito.verify(mockRepository,Mockito.times(1))
                .updateUser(Mockito.any(User.class));
    }


    @Test
    public void delete_Should_CallRepository_When_ExistingUserIsDeleted() {
        //Arrange
        User user = createMockUser();

        //Act
        service.deleteUser(user,user);

        //Аssert
        Mockito.verify(mockRepository,Mockito.times(1))
                .deleteUser(Mockito.any(User.class));
    }


    @Test
    public void delete_Should_ThrowException_When_UserIsNotAccountOwner() {
        //Arrange
        User firstUser = createMockUser();
        User secondUser = createMockAdmin();

        //Act, Аssert
        Assertions.assertThrows(AuthenticationFailureException.class, () ->
                service.deleteUser(firstUser,secondUser));
    }


    @Test
    public void delete_Should_SetUserAsDeleted_When_deleteUserMethodIsCalled() {
        //Arrange
        User user = createMockUser();

        service.deleteUser(user,user);

        //Act, Аssert
        Assertions.assertTrue(user.isDeleted());
    }


    @Test
    public void updatePermissions_Should_ThrowException_When_NonAdminUserAssignsAdminAccess() {
        //Arrange
        User user = createMockUser();

        //Act, Аssert
        Assertions.assertThrows(AuthenticationFailureException.class, () ->
                service.updatePermissions(user,user));
    }


    @Test
    public void updatePermissions_Should_CallRepository_When_UserPermissionsAreUpdated() {
        //Arrange
        User adminUser = createMockAdmin();
        User user = createMockUser();

        //Act
        service.updatePermissions(adminUser, user);
        //Аssert
        Mockito.verify(mockRepository,Mockito.times(1))
                .updatePermissions(Mockito.any(User.class));
    }



}
