package com.example.forumsystem.services;

import com.example.forumsystem.models.comment.models.Comment;
import com.example.forumsystem.models.post.models.Post;
import com.example.forumsystem.models.user.models.User;
import com.example.forumsystem.repositories.contracts.CommentRepository;
import com.example.forumsystem.repositories.contracts.PostRepository;
import com.example.forumsystem.repositories.contracts.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;


import static com.example.forumsystem.Helpers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CommentServiceImplTests {

    @Mock
    CommentRepository commentRepository;
    @Mock
    PostRepository postRepository;
    @Mock
    UserRepository userRepository;
    @InjectMocks
    CommentServiceImpl service;


    @Test
    public void getById_Should_ReturnComment_When_MatchExists() {
        //Arrange
        Comment mockComment = createMockComment();
        when(commentRepository.getCommentById(1))
                .thenReturn(mockComment);

        //Act
        Comment comment = service.getCommentById(1);

        //Assert
        assertEquals(1, comment.getId());
        assertEquals(mockComment.getContent(), comment.getContent());
        assertEquals(mockComment.getPost(), comment.getPost());
        assertEquals(mockComment.getUser(), comment.getUser());

    }

//    @Test
//    public void create_Should_ThrowException_When_PostDoesNotExist() {
//
//    }

    @Test
    public void create_Should_CallRepository_When_NewCommentIsCreated() {
        //Arrange
        Comment mockComment = createMockComment();

        when(postRepository.getPostById(Mockito.anyInt()))
                .thenReturn(mockComment.getPost());

        //Act
        service.createComment(mockComment, mockComment.getUser());

        //Assert
        Mockito.verify(commentRepository, times(1))
                .createComment(Mockito.any(Comment.class));
    }

    @Test
    public void update_Should_CallRepository_When_ExistingCommentIsUpdated() {
        //Arrange
        Comment mockComment = createMockComment();

        //Act
        service.updateComment(mockComment, mockComment.getUser());

        //Assert
        Mockito.verify(commentRepository, times(1))
                .updateComment(mockComment);
    }

    @Test
    public void delete_Should_CallRepository_When_ExistingCommentIsDeleted() {
        //Arrange
        Comment mockComment = createMockComment();

        //Act
        service.deleteComment(mockComment.getUser(), mockComment);

        //Assert
        Mockito.verify(commentRepository, times(1))
                .deleteComment(mockComment);
    }

    @Test
    public void getUserComments_Should_CallRepository_When_MatchExist() {
        //Arrange
        User mockUser = createMockUser();
        when(userRepository.getUserById(1))
                .thenReturn(mockUser);

        //Act
        service.getUserComments(mockUser, mockUser.getId());

        //Arrange
        Mockito.verify(commentRepository, times(1))
                .getUserComments(mockUser);
    }

    @Test
    public void getPostComments_Should_CallRepository_When_MatchExist() {
        //Arrange
        Post mockPost = createMockPost();
        when(postRepository.getPostById(1))
                .thenReturn(mockPost);

        //Act
        service.getPostComments(mockPost.getId());

        //Arrange
        Mockito.verify(commentRepository, times(1))
                .getPostComments(mockPost.getId());
    }

}
