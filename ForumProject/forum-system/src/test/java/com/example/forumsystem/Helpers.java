package com.example.forumsystem;

import com.example.forumsystem.models.comment.models.Comment;
import com.example.forumsystem.models.post.models.Post;
import com.example.forumsystem.models.post.models.PostRating;
import com.example.forumsystem.models.post.models.PostRatingID;
import com.example.forumsystem.models.tag.models.Tag;
import com.example.forumsystem.models.user.models.User;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Helpers {

    public static User createMockUser() {
        var mockUser = new User();
        mockUser.setId(1);
        mockUser.setFirstName("MockFirstName");
        mockUser.setLastName("MockLastName");
        mockUser.setUsername("MockUsername");
        mockUser.setEmail("MockUser@mockuser.mock");
        mockUser.setPassword("mockpassword");
        return mockUser;
    }

    public static User createMockAdmin() {
        User mockAdmin = createMockUser();
        mockAdmin.setId(2);
        mockAdmin.setUsername("MockAdmin");
        mockAdmin.setEmail("admin@mock.com");
        mockAdmin.setAdmin(true);
        mockAdmin.setPhone("+00000000");
        return mockAdmin;
    }

    public static Post createMockPost(){
        var mockPost = new Post();
        mockPost.setId(1);
        mockPost.setTitle("MockTitle");
        mockPost.setContent("MockContent");
        mockPost.setTimestamp(LocalDate.now());
        mockPost.setCreatedBy(createMockUser());
        return mockPost;
    }

    public static Comment createMockComment(){
        var mockComment = new Comment();
        mockComment.setId(1);
        mockComment.setContent("This is a test content");
        mockComment.setTimestamp(LocalDate.now());
        mockComment.setPost(createMockPost());
        mockComment.setUser(createMockUser());
        return mockComment;
    }

    public static Tag createMockTag(){
        var mockTag = new Tag();
        mockTag.setId(1);
        mockTag.setTag("mock");
        return mockTag;
    }

    public static PostRating createMockPostRating() {
        var mockRate = new PostRating();
        var mockRateId = new PostRatingID();
        mockRateId.setPost(createMockPost());
        mockRateId.setUser(createMockUser());

        mockRate.setId(mockRateId);
        mockRate.setRate(5);
        return mockRate;
    }
}
