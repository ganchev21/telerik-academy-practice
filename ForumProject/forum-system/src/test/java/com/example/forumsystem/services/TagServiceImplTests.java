package com.example.forumsystem.services;

import com.example.forumsystem.exceptions.DuplicateEntityException;
import com.example.forumsystem.exceptions.EntityNotFoundException;
import com.example.forumsystem.models.tag.models.Tag;
import com.example.forumsystem.repositories.contracts.TagRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static com.example.forumsystem.Helpers.createMockAdmin;
import static com.example.forumsystem.Helpers.createMockTag;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class TagServiceImplTests {

    @Mock
    private TagRepository tagRepository;

    @InjectMocks
    private TagServiceImpl tagService;

    @Test
    public void getAll_Should_ReturnTags_When_methodIsCalled() {
        //Arrange
        List<Tag> tags = new ArrayList<>();
        tags.add(createMockTag());
        tags.add(createMockTag());
        Mockito.when(tagRepository.getAll()).thenReturn(tags);

        //Act
        List<Tag> result = tagService.getAll();

        //Assert
        Assertions.assertEquals(tags, result);
    }

    @Test
    public void getById_Should_ReturnTag_When_IdIsProvided() {
        //Arrange
        Tag tag = createMockTag();
        Mockito.when(tagRepository.getTagById(1)).thenReturn(tag);

        //Act
        Tag result = tagService.getTagById(1);

        //Assert
        Assertions.assertEquals(tag, result);
    }

    @Test
    public void createTag_Should_CallRepository_When_TagDoesNotExist() {
        //Arrange
        Tag tag = createMockTag();
        Mockito.when(tagRepository.getTagByName(Mockito.anyString()))
                .thenThrow(new EntityNotFoundException());
        //Act
        tagService.createTag(tag.getTag());

        //Assert
        Mockito.verify(tagRepository, times(1))
                .createTag(any(Tag.class));
    }


    @Test
    public void createTag_Should_ReturnTag_When_TagExists() {
        //Arrange
        Tag mockTag = createMockTag();
        Mockito.when(tagRepository.getTagByName(Mockito.anyString())).thenReturn(mockTag);

        //Act
        Tag resultTag = tagService.createTag(mockTag.getTag());

        //Assert
        Mockito.verify(tagRepository, times(0))
                .createTag(Mockito.any(Tag.class));
        Assertions.assertEquals(mockTag, resultTag);
    }


    // @Test
    // public void createAll_Should_CreateAllTags_When_UserIsAdmin() {
    //     //Arrange
    //     User mockAdmin = createMockAdmin();
    //     List<String> mockTagList = Arrays.asList("mock1", "mock2");

    //     //Mock the createTag method
    //     Mockito.when(tagService.createTag(Mockito.anyString()))
    //             .thenReturn(Mockito.any(Tag.class));

    //     //Act
    //     tagService.createAll(mockAdmin, mockTagList);

    //     //Assert
    //     Mockito.verify(tagService, times(mockTagList.size()))
    //             .createTag(Mockito.anyString());
    // }


    @Test
    public void updateTag_Should_ThrowDuplicateException_When_TagNameExists() {
        //Arrange
        Tag mockTag = createMockTag();
        mockTag.setId(2);

        Mockito.when(tagRepository.getTagByName(Mockito.anyString()))
                .thenReturn(mockTag);

        //Act and Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> {
            tagService.updateTag(createMockAdmin(),
                    createMockTag(),
                    Mockito.anyString());
        });
    }

    @Test
    public void updateTag_Should_CallRepo_When_NoDuplicateTagIsFound() {
        //Arrange

        Mockito.when(tagRepository.getTagByName(Mockito.anyString()))
                .thenThrow(new EntityNotFoundException());

        //Act
        tagService.updateTag(createMockAdmin(),
                createMockTag(),
                Mockito.anyString());
        //Assert
        Mockito.verify(tagRepository, times(1))
                .updateTag(Mockito.any(Tag.class));

    }

    @Test
    public void updateTag_Should_CallRepo_WhenSameTagIsUpdated() {
        //Arrange
        Tag mockTag = createMockTag();

        Mockito.when(tagRepository.getTagByName(Mockito.anyString()))
                .thenReturn(mockTag);
        //Act
        tagService.updateTag(createMockAdmin(),
                createMockTag(),
                Mockito.anyString());
        //Assert
        Mockito.verify(tagRepository, times(1))
                .updateTag(Mockito.any(Tag.class));

    }


    @Test
    public void deleteTag_Should_CallRepo_WhenTagIsDeleted() {
        //Arrange Act
        tagService.deleteTag(createMockAdmin(), createMockTag());

        //Assert
        Mockito.verify(tagRepository, times(1))
                .deleteTag(Mockito.any(Tag.class));
    }


}
