package com.example.forumsystem.services;

import com.example.forumsystem.exceptions.DuplicateEntityException;
import com.example.forumsystem.exceptions.EntityNotFoundException;
import com.example.forumsystem.helpers.ValidationHelper;
import com.example.forumsystem.models.post.models.Post;
import com.example.forumsystem.models.tag.models.Tag;
import com.example.forumsystem.models.user.models.User;
import com.example.forumsystem.repositories.TagRepositoryImpl;
import com.example.forumsystem.repositories.contracts.PostRepository;
import com.example.forumsystem.repositories.contracts.TagRepository;
import org.hibernate.SessionFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.*;

import static com.example.forumsystem.Helpers.*;
import static org.junit.jupiter.api.Assertions.fail;

@ExtendWith(MockitoExtension.class)
public class PostServiceImplTests {
    @Mock
    PostRepository mockRepository;

    @InjectMocks
    PostServiceImpls service;

    @Mock
    TagServiceImpl tagService;


    @Test
    public void getMostCommented_Should_CallRepository() {
        //Arrange
        Post post1 = createMockPost();
        Post post2 = createMockPost();
        Post post3 = createMockPost();

        List<Post> mostCommentedPosts = Arrays.asList(post1, post2, post3);
        Mockito.when(mockRepository.getMostCommentedPosts()).thenReturn(mostCommentedPosts);

        //Act
        List<Post> actual = service.getMostCommented();
        //Assert

        Assertions.assertEquals(actual, mockRepository.getMostCommentedPosts());
    }

    @Test
    public void getMostRecent_Should_CallRepository() {
        List<Post> expectedPosts = new ArrayList<>();
        Post post1 = createMockPost();
        Post post2 = createMockPost();
        expectedPosts.add(post1);
        expectedPosts.add(post2);

        Mockito.when(mockRepository.getMostRecentPosts()).thenReturn(expectedPosts);
        List<Post> actualPosts = service.getMostRecentPosts();

        Assertions.assertEquals(expectedPosts, actualPosts);
    }

    @Test
    public void getAll_Should_CallRepository() {
        Mockito.when(mockRepository.getAll()).thenReturn(null);

        service.getAll();

        Mockito.verify(mockRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void getPostById_Should_ReturnPost_When_IdMatches() {
        Post post = createMockPost();

        Mockito.when(mockRepository.getPostById(Mockito.anyInt())).thenReturn(post);

        Post result = service.getPostById(post.getId());

        Assertions.assertEquals(post, result);
    }

    @Test
    public void getPostTitle_Should_ReturnPost_When_TitleMatches() {
        Post mockPost = createMockPost();
        Mockito.when(mockRepository.getPostById(Mockito.anyInt())).thenReturn(mockPost);

        String postTitle = service.getPostTitle(mockPost.getId());

        Assertions.assertEquals(mockPost.getTitle(), postTitle);
    }

    @Test
    public void getPostContent_Should_ReturnPost_When_ContentMatches() {
        Post post = createMockPost();
        Mockito.when(mockRepository.getPostById(Mockito.anyInt())).thenReturn(post);

        String postContent = service.getPostContent(post.getId());

        Assertions.assertEquals(post.getContent(), postContent);
    }

    @Test
    public void getUserPost_Should_ReturnUserPost_When_BothExist() {
        Post post = createMockPost();
        User user = createMockUser();
        List<Post> posts = List.of(post);

        Mockito.when(mockRepository.getUserPosts(posts, user)).thenReturn(posts);

        List<Post> userPosts = service.getUserPosts(posts, user);

        Assertions.assertEquals(1, userPosts.size());
        Assertions.assertEquals(post, userPosts.get(0));
    }

    @Test
    public void create_Should_CallRepository_When_PostWithSameNameDoesNotExist() {
        Post post = createMockPost();
        Mockito.when(mockRepository.getPostByTitle(post.getTitle())).thenThrow(EntityNotFoundException.class);

        service.createPost(post);

        Mockito.verify(mockRepository).createPost(post);
    }

    @Test
    public void create_Should_ThrowException_When_PostWithSameNameExists() {
        Post post = createMockPost();
        Mockito.when(mockRepository.getPostByTitle(post.getTitle())).thenThrow(DuplicateEntityException.class);

        Assertions.assertThrows(DuplicateEntityException.class, () -> service.createPost(post));
    }

    //TODO is not working
    @Test
    public void update_Should_UpdatePost_WhenUserIsCreator() {
        Post postFromDto = createMockPost();
        Post existingPost = createMockPost();
        User creator = createMockUser();
        postFromDto.setCreatedBy(creator);
        existingPost.setCreatedBy(creator);

        Mockito.when(mockRepository.getPostByTitle(postFromDto.getTitle())).thenReturn(existingPost);
        Mockito.when(mockRepository.getPostById(postFromDto.getId())).thenReturn(existingPost);


        service.updatePost(postFromDto, existingPost);

        Mockito.verify(mockRepository, Mockito.times(1))
                .updatePost(existingPost);
    }

    @Test
    public void update_Should_ThrowException_When_PostWithSameNameExists() {
        Post post = createMockPost();
        Mockito.when(mockRepository.getPostByTitle(post.getTitle())).thenThrow(DuplicateEntityException.class);

        Assertions.assertThrows(DuplicateEntityException.class, () -> service.createPost(post));
    }
    @Test
    public void delete_shouldDeletePost_whenPassedValidUserAndPost() {
        // given
        User mockedLoggedUser = createMockUser();
        Post mockedPostToDelete = createMockPost();

        // when
        service.deletePost(mockedLoggedUser, mockedPostToDelete);

        // then
        Mockito.verify(mockRepository, Mockito.times(1)).deletePost(mockedPostToDelete);
    }


    //TODO rename last 3 test MatchExist is not the right naming
    @Test
    public void likePost_Should_CallRepository_When_MatchExist () {
        //Arrange
        User mockUser = createMockUser();
        Post mockPost = createMockPost();

        //Act
        service.likePost(mockUser, mockPost);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .updatePost(mockPost);

    }

    @Test
    public void dislike_Should_CallRepository_When_MatchExist () {
        //Arrange
        User mockUser = createMockUser();
        Post mockPost = createMockPost();

        //Act
        service.dislikePost(mockUser, mockPost);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .updatePost(mockPost);

    }


    @Test
    public void updatePostTags_Should_CallRepository_When_MatchExist () {
        //Arrange
        Post mockPost = createMockPost();

        Mockito.when(tagService.createTag(Mockito.anyString()))
                .thenReturn(createMockTag());

        //Act
        service.updatePostTags(mockPost, List.of(Mockito.anyString()));

        //Arrange
        Mockito.verify(mockRepository, Mockito.times(1))
                .updatePost(mockPost);


    }

    @Test
    public void removePostTags_Should_CallRepository_When_MatchExist () {
        //Arrange
        Post mockPost = createMockPost();

        Mockito.when(tagService.createTag(Mockito.anyString()))
                .thenReturn(createMockTag());

        //Act
        service.removePostTags(mockPost, List.of(Mockito.anyString()));

        //Arrange
        Mockito.verify(mockRepository, Mockito.times(1))
                .updatePost(mockPost);


    }

}
