package com.example.forumsystem.services;

import com.example.forumsystem.exceptions.DuplicateEntityException;
import com.example.forumsystem.exceptions.EntityNotFoundException;
import com.example.forumsystem.models.post.models.Post;
import com.example.forumsystem.models.post.models.PostRating;
import com.example.forumsystem.models.user.models.User;
import com.example.forumsystem.repositories.contracts.PostRepository;
import com.example.forumsystem.repositories.contracts.RateRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static com.example.forumsystem.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class RateServiceImplTests {

    @Mock
    private RateRepository rateRepository;

    @Mock
    private PostServiceImpls postService;

    @InjectMocks
    private RateServiceImpl rateService;


    //TODO throw exception on line 42

    @Test
    public void getAllRating_Should_Call_Repository_When_ValidData_Provided() {
        //Arrange
        PostRating mockRate = createMockPostRating();
        Post mockPost = createMockPost();
        Mockito.when(rateRepository.getAllPostsRating())
                .thenReturn(List.of(mockRate));
        Mockito.when(postService.getPostById(mockPost.getId()))
                .thenReturn(mockPost);

        //Act
        rateService.getAllPostsRating();


        //Assert
        Mockito.verify(rateRepository, Mockito.times(1))
                .getAllPostsRating();

    }

    @Test
    public void getPostRating_Should_Return_StringBuilder_When_ValidData_Provided() {

        Post mockPost = createMockPost();
        Mockito.when(rateRepository.getAveragePostRating(mockPost.getId()))
                .thenReturn(4.50);
        Mockito.when(rateRepository.getPeopleRated(mockPost.getId()))
                .thenReturn(5);

        StringBuilder result = rateService.getPostRating(mockPost);

        Assertions.assertEquals(mockPost.getTitle() + "@4.50*5", result.toString());

    }

    @Test
    public void getById_Should_Call_Repository_When_ValidData_Provided() {
        //Arrange
        Post mockPost = createMockPost();
        User mockUser = createMockUser();

        //Act
        rateService.getById(mockUser.getId(), mockPost.getId());

        //Assert
        Mockito.verify(rateRepository, Mockito.times(1))
                .getById(mockUser.getId(), mockPost.getId());

    }

    @Test
    public void ratePost_Should_ThrowException_When_SomeoneWants_To_RateAgain() {
        Post mockPost = createMockPost();

        Mockito.when(postService.getPostById(Mockito.anyInt()))
                .thenReturn(mockPost);
        Mockito.when(rateRepository.findRecord(Mockito.anyInt(), Mockito.anyInt()))
                .thenReturn(false);


        Assertions.assertThrows(IllegalArgumentException.class, () -> rateService.ratePost(createMockUser(), mockPost.getId(), Mockito.anyInt()));

    }

}
