create table tags
(
    tag_id int auto_increment
        primary key,
    tag    varchar(20) not null
        unique
);

create table users
(
    user_id    int auto_increment
        primary key,
    first_name varchar(32)  not null,
    last_name  varchar(32)  not null,
    username   varchar(32)  not null,
    email      varchar(100) not null,
    password   varchar(40)  not null
);

create table phones
(
    user_id int         not null,
    phone   varchar(30) not null,
    constraint phones_users_user_id_fk
        foreign key (user_id) references users (user_id)
);

create table posts
(
    post_id   int auto_increment
        primary key,
    user_id   int           not null,
    title     varchar(64)   not null,
    content   varchar(8192) not null,
    timestamp datetime      not null,
    constraint posts_users_user_id_fk
        foreign key (user_id) references users (user_id)
);

create table all_rates
(
    post_rated int default 0 not null,
    post_id    int           not null,
    user_id    int           not null,
    primary key (post_id, user_id),
    constraint rates_posts_post_id_fk
        foreign key (post_id) references posts (post_id)
            on delete cascade,
    constraint rates_users_user_id_fk
        foreign key (user_id) references users (user_id)
);

create table average_rating
(
    post_id int    not null,
    rating  double not null,
    constraint average_rating_posts_post_id_fk
        foreign key (post_id) references posts (post_id)
            on delete cascade
);

create table comments
(
    comments_id int auto_increment
        primary key,
    post_id     int           not null,
    user_id     int           not null,
    content     varchar(8192) null,
    timestamp   datetime      not null,
    constraint comments_posts_post_id_fk
        foreign key (post_id) references posts (post_id)
            on delete cascade,
    constraint comments_users_user_id_fk
        foreign key (user_id) references users (user_id)
);

create table dislike
(
    user_id int not null,
    post_id int not null,
    primary key (post_id, user_id),
    constraint dislike_posts_post_id_fk
        foreign key (post_id) references posts (post_id),
    constraint dislike_users_user_id_fk
        foreign key (user_id) references users (user_id)
);

create table likes
(
    user_id int not null,
    post_id int not null,
    primary key (post_id, user_id),
    constraint likes_posts_post_id_fk
        foreign key (post_id) references posts (post_id),
    constraint likes_users_user_id_fk
        foreign key (user_id) references users (user_id)
);

create table tag_posts
(
    post_id int not null,
    tag_id  int not null,
    primary key (post_id, tag_id),
    constraint tag_posts_posts_post_id_fk
        foreign key (post_id) references posts (post_id),
    constraint tag_posts_tags_tag_id_fk
        foreign key (tag_id) references tags (tag_id)
            on delete cascade
);

create definer = forum_system_user@localhost trigger delete_unused_tags
    after delete
on tag_posts
    for each row
BEGIN
    IF NOT EXISTS (SELECT * FROM tag_posts WHERE tag_id = OLD.tag_id) THEN
DELETE FROM tags WHERE tag_id = OLD.tag_id;
END IF;
END;

create table user_permissions
(
    is_deleted tinyint(1) default 0 not null,
    is_blocked tinyint(1) default 0 not null,
    is_admin   tinyint(1) default 0 not null,
    user_id    int auto_increment
        primary key,
    constraint user_permissions_users_user_id_fk
        foreign key (user_id) references users (user_id)
);

