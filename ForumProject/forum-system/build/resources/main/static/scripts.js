/*!
* Start Bootstrap - Clean Blog v6.0.9 (https://startbootstrap.com/theme/clean-blog)
* Copyright 2013-2023 Start Bootstrap
* Licensed under MIT (https://github.com/StartBootstrap/startbootstrap-clean-blog/blob/master/LICENSE)
*/
window.addEventListener('DOMContentLoaded', () => {
    let scrollPos = 0;
    const mainNav = document.getElementById('mainNav');
    const headerHeight = mainNav.clientHeight;
    window.addEventListener('scroll', function() {
        const currentTop = document.body.getBoundingClientRect().top * -1;
        if ( currentTop < scrollPos) {
            // Scrolling Up
            if (currentTop > 0 && mainNav.classList.contains('is-fixed')) {
                mainNav.classList.add('is-visible');
            } else {
                console.log(123);
                mainNav.classList.remove('is-visible', 'is-fixed');
            }
        } else {
            // Scrolling Down
            mainNav.classList.remove(['is-visible']);
            if (currentTop > headerHeight && !mainNav.classList.contains('is-fixed')) {
                mainNav.classList.add('is-fixed');
            }
        }
        scrollPos = currentTop;
    });
})

function updateTagField(checkbox) {
    // Get the tag value from the checkbox label
    var tagValue = checkbox.parentNode.textContent.trim();

    // Get the current value of the tag field
    var tagField = document.getElementById("tagField");
    var currentTags = tagField.value.split(", ");

    // Update the tag field based on the checkbox status
    if (checkbox.checked) {
        // Add the tag value to the tag field if it's not already there
        if (currentTags.indexOf(tagValue) === -1) {
            // Add a comma before the tag value if there are already other tags
            tagField.value += (tagField.value ? ", " : "") + tagValue;
        }
    } else {
        // Remove the tag value from the tag field if it's already there
        var index = currentTags.indexOf(tagValue);
        if (index !== -1) {
            currentTags.splice(index, 1);
        }

        // Update the tag field value
        tagField.value = currentTags.join(", ");
    }
}

function searchTags(searchInput) {
    const searchTerm = searchInput.value.toLowerCase();
    const tagItems = document.querySelectorAll('.tag-item');

    tagItems.forEach(tagItem => {
        const tagText = tagItem.textContent.toLowerCase();
        if (tagText.includes(searchTerm)) {
            tagItem.style.display = 'table-row';
        } else {
            tagItem.style.display = 'none';
        }
    });
}

function showEditCommentPopup(button) {
    const commentId = button.id.split('-')[2];
    document.getElementById("editCommentPopup-" + commentId).style.display = "block";
    document.getElementById("button-edit-" + commentId).style.display = "none";
    document.getElementById("deleteCommentForm-" + commentId).style.display = "none";
}
