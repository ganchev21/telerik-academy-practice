package RandomPackage;

import java.util.Random;

public class Main {
    public static void main(String[] args) {

        System.out.println("Hello Buddy!");

        Random random = new Random();

        String[] text = new String[] {"Ani", "Biser", "Andreev", "Iliev", "Anton", "Vesko", "Mihailov"};
        String[] age = new String[] {"6","12","15","18","22","35","40","50","64","76","88"};
        String[] text1 = new String[] {"love", "like", "hate", "can't stand", "is disgusted of"};
        String[] text2 = new String[] {"Ice cream", "Hot-Dog", "Water", "Game of Thrones", "Movies", "Pizza"};

        System.out.println(random(text,random) + " is " + random(age, random) +" years old and " + random(text1, random) + " " + random(text2, random));


    }

    public static String random (String[] text, Random random) {
        int randomize = random.nextInt(text.length);

        return text[randomize];
    }

}
