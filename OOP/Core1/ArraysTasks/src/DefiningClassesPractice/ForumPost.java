package DefiningClassesPractice;

import org.w3c.dom.ls.LSOutput;

import java.util.ArrayList;
import java.util.List;

public class ForumPost {
    String author;
    String text;
    int upVotes;
    List<String> replies;

    public ForumPost(String name, String text, int upVotes) {
        this.author = name;
        this.text = text;
        this.upVotes = upVotes;
        this.replies = new ArrayList<>();
    }
    public ForumPost(String name, String text) {
        this.author = name;
        this.text = text;
        this.replies = new ArrayList<>();
    }

    public String format () {
        String result = String.format("%s / by %s, %d votes. %n",text, author, upVotes);


        return result;
    }


}
