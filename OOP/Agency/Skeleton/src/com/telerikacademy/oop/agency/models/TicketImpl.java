package com.telerikacademy.oop.agency.models;

import com.telerikacademy.oop.agency.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.agency.models.contracts.Identifiable;
import com.telerikacademy.oop.agency.models.contracts.Journey;
import com.telerikacademy.oop.agency.models.contracts.Ticket;

public class TicketImpl implements Ticket, Identifiable {

    private int id;
    private Journey journey;
    private double cost;

    public TicketImpl(int id, Journey journey, double costs) {
        this.id = id;
        this.journey = journey;
        setCost(costs);
    }

    private void setCost(double cost) {
        if (cost < 0) {
            throw new InvalidUserInputException(
                    String.format("Value of 'costs' must be a positive number. Actual value: %.2f.", cost));
        }
        this.cost = cost;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public Journey getJourney() {
        return journey;
    }

    @Override
    public double calculatePrice() {
        return cost * journey.calculateTravelCosts();
    }

    @Override
    public double getAdministrativeCosts() {
        return cost;
    }

    @Override
    public String getAsString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Ticket ----").append(System.lineSeparator());
        sb.append("Destination: ").append(getJourney().getDestination()).append(System.lineSeparator());
        sb.append(String.format("Price: %.2f", calculatePrice())).append(System.lineSeparator());
        return sb.toString();
    }
    //Ticket ----
    //Destination: Turnovo
    //Price: 6342.00
}
