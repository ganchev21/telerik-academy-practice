package com.telerikacademy.oop.agency.models.vehicles;

import com.telerikacademy.oop.agency.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.agency.models.vehicles.contracts.Train;
import com.telerikacademy.oop.agency.utils.ValidationHelper;

public class TrainImpl extends VehicleBase implements Train {

   private static final int PASSENGER_MIN_VALUE = 30;
   private static final int PASSENGER_MAX_VALUE = 150;
   private static final int CARTS_MIN_VALUE = 1;
   private static final int CARTS_MAX_VALUE = 15;
   private static final double PRICE_MIN_VALUE = 0.1;
   private static final double PRICE_MAX_VALUE = 2.5;
    private int carts;


    public TrainImpl(int id, int passengerCapacity, double pricePerKilometer, int carts) {
        super(id,passengerCapacity,pricePerKilometer, VehicleType.LAND);
        setCarts(carts);
    }

    @Override
    protected void validateCapacity(int capacity) {
        ValidationHelper.validateValueInRange(capacity, PASSENGER_MIN_VALUE, PASSENGER_MAX_VALUE,
                String.format("A train cannot have less than %d passengers or more than %d passengers.",
                        PASSENGER_MIN_VALUE, PASSENGER_MAX_VALUE));
    }

    @Override
    protected void validatePrice(double price) {
        ValidationHelper.validateValueInRange(price, PRICE_MIN_VALUE, PRICE_MAX_VALUE,
                String.format("A vehicle with a price per kilometer lower than $%.2f or higher than $%.2f cannot exist!",
                        PRICE_MIN_VALUE, PRICE_MAX_VALUE));
    }

    private void setCarts(int carts) {
        ValidationHelper.validateValueInRange(carts, CARTS_MIN_VALUE, CARTS_MAX_VALUE,
                String.format("A train cannot have less than %d cart or more than %d carts.",
                        CARTS_MIN_VALUE, CARTS_MAX_VALUE));
        this.carts = carts;
    }

    @Override
    public int getCarts() {
        return carts;
    }

    @Override
    public String getAsString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Train ----").append(System.lineSeparator());
        sb.append(super.getAsString());
        sb.append("Carts amount: ").append(getCarts()).append(System.lineSeparator());
        return sb.toString();
    }
    //Train ----
    //Passenger capacity: 80
    //Price per kilometer: 0.44
    //Vehicle type: LAND
    //Carts amount: 3
}