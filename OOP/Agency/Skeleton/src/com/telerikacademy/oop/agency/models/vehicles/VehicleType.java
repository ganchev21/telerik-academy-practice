package com.telerikacademy.oop.agency.models.vehicles;

public enum VehicleType {
    LAND,
    AIR,
    SEA;
}
