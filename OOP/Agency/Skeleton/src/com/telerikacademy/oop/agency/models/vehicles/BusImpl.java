package com.telerikacademy.oop.agency.models.vehicles;

import com.telerikacademy.oop.agency.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.agency.models.contracts.Identifiable;
import com.telerikacademy.oop.agency.models.vehicles.contracts.Bus;
import com.telerikacademy.oop.agency.models.vehicles.contracts.Vehicle;
import com.telerikacademy.oop.agency.utils.ValidationHelper;

public class BusImpl extends VehicleBase implements Bus {

   private static final int PASSENGER_MIN_VALUE = 10;
   private static final int PASSENGER_MAX_VALUE = 50;
   private static final double PRICE_MIN_VALUE = 0.1;
   private static final double PRICE_MAX_VALUE = 2.5;




    public BusImpl(int id, int passengerCapacity, double pricePerKilometer) {
        super(id, passengerCapacity, pricePerKilometer,VehicleType.LAND);
       // throw new UnsupportedOperationException("Not implemented yet.");
    }

    @Override
    protected void validateCapacity(int capacity) {
        ValidationHelper.validateValueInRange(capacity, PASSENGER_MIN_VALUE, PASSENGER_MAX_VALUE,
                String.format("A bus cannot have less than %d passengers or more than %d passengers.",
                        PASSENGER_MIN_VALUE, PASSENGER_MAX_VALUE));
    }

    @Override
    protected void validatePrice(double price) {
        ValidationHelper.validateValueInRange(price, PRICE_MIN_VALUE, PRICE_MAX_VALUE,
                String.format("A vehicle with a price per kilometer lower than $%.2f or higher than $%.2f cannot exist!",
                        PRICE_MIN_VALUE, PRICE_MAX_VALUE));

    }

    @Override
    public String getAsString() {
        return "Bus ----" +
                System.lineSeparator() +
                super.getAsString();
    }
    //Bus ----
    //Passenger capacity: 10
    //Price per kilometer: 0.70
    //Vehicle type: LAND
    //####################
}