package com.telerikacademy.oop.agency.models.vehicles;

import com.telerikacademy.oop.agency.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.agency.models.vehicles.contracts.Airplane;
import com.telerikacademy.oop.agency.utils.ValidationHelper;

public class AirplaneImpl extends VehicleBase implements Airplane {

    private static final int PASSENGER_MIN_CAPACITY = 1;
    private static final int PASSENGER_MAX_CAPACITY = 800;
    private static final double PRICE_MIN_VALUE = 0.1;
    private static final double PRICE_MAX_VALUE = 2.5;

    private boolean hasFood;


    public AirplaneImpl(int id, int passengerCapacity, double pricePerKilometer, boolean hasFreeFood) {
        super(id, passengerCapacity, pricePerKilometer, VehicleType.AIR);
        this.hasFood = hasFreeFood;

    }

    @Override
    protected void validateCapacity(int capacity) {
        ValidationHelper.validateValueInRange(capacity, PASSENGER_MIN_CAPACITY, PASSENGER_MAX_CAPACITY,
                String.format("A vehicle with less than %d passengers or more than %d passengers cannot exist!",
                        PASSENGER_MIN_CAPACITY, PASSENGER_MAX_CAPACITY));
    }

    @Override
    protected void validatePrice(double price) {
        ValidationHelper.validateValueInRange(price, PRICE_MIN_VALUE, PRICE_MAX_VALUE,
                String.format("A vehicle with a price per kilometer lower than $%.2f or higher than $%.2f cannot exist!",
                        PRICE_MIN_VALUE, PRICE_MAX_VALUE));
    }

    @Override
    public boolean hasFreeFood() {
        return hasFood;
    }

    @Override
    public String getAsString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Airplane ----").append(System.lineSeparator());
        sb.append(super.getAsString());
        sb.append("Has free food: ").append(hasFreeFood()).append(System.lineSeparator());
        return sb.toString();
    }
    //Airplane ----
    //Passenger capacity: 230
    //Price per kilometer: 1.00
    //Vehicle type: AIR
    //Has free food: true
    //####################
}