package com.telerikacademy.oop.agency;

import com.telerikacademy.oop.agency.core.AgencyEngineImpl;
import com.telerikacademy.oop.agency.models.vehicles.BusImpl;
import com.telerikacademy.oop.agency.models.vehicles.contracts.Bus;

public class Startup {

    public static void main(String[] args) {
        AgencyEngineImpl engine = new AgencyEngineImpl();
        engine.start();

    }

}
