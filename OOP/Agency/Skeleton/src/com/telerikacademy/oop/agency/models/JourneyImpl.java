package com.telerikacademy.oop.agency.models;

import com.telerikacademy.oop.agency.models.contracts.Identifiable;
import com.telerikacademy.oop.agency.models.contracts.Journey;
import com.telerikacademy.oop.agency.models.vehicles.VehicleBase;
import com.telerikacademy.oop.agency.models.vehicles.contracts.Vehicle;
import com.telerikacademy.oop.agency.utils.ValidationHelper;

public class JourneyImpl implements Journey, Identifiable {

    private static final int START_LOCATION_MIN_LENGTH = 5;
    private static final int START_LOCATION_MAX_LENGTH = 25;
    private static final int DESTINATION_MIN_LENGTH = 5;
    private static final int DESTINATION_MAX_LENGTH = 25;
    private static final int DISTANCE_MIN_VALUE = 5;
    private static final int DISTANCE_MAX_VALUE = 5000;

    private int id;
    private String startLocation;
    private String destination;
    private int distance;
    private Vehicle vehicle;

    public JourneyImpl(int id, String startLocation, String destination, int distance, Vehicle vehicle) {
        this.id = id;
        setStartLocation(startLocation);
        setDestination(destination);
        setDistance(distance);
        this.vehicle = vehicle;
    }

    private void setStartLocation(String startLocation) {
        ValidationHelper.validateStringLength(startLocation, START_LOCATION_MIN_LENGTH, START_LOCATION_MAX_LENGTH,
                String.format("The StartingLocation's length cannot be less than %d or more than %d symbols long.",
                        START_LOCATION_MIN_LENGTH, START_LOCATION_MAX_LENGTH));
        this.startLocation = startLocation;
    }

    private void setDestination(String destination) {
        ValidationHelper.validateStringLength(destination, DESTINATION_MIN_LENGTH, DESTINATION_MAX_LENGTH,
                String.format("The Destination's length cannot be less than %d or more than %d symbols long.",
                        DESTINATION_MIN_LENGTH, DESTINATION_MAX_LENGTH));
        this.destination = destination;
    }

    private void setDistance(int distance) {
        ValidationHelper.validateValueInRange(distance, DISTANCE_MIN_VALUE,DISTANCE_MAX_VALUE,
                String.format("The Distance cannot be less than %d or more than %d kilometers.",
                        DESTINATION_MIN_LENGTH, DISTANCE_MAX_VALUE));
        this.distance = distance;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public int getDistance() {
        return distance;
    }

    @Override
    public Vehicle getVehicle() {
        return vehicle;
    }

    @Override
    public String getStartLocation() {
        return startLocation;
    }

    @Override
    public String getDestination() {
        return destination;
    }

    @Override
    public double calculateTravelCosts() {
        return distance * vehicle.getPricePerKilometer();
    }

    @Override
    public String getAsString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Journey ----").append(System.lineSeparator());
        sb.append("Start location: ").append(getStartLocation()).append(System.lineSeparator());
        sb.append("Destination: ").append(getDestination()).append(System.lineSeparator());
        sb.append("Distance: ").append(getDistance()).append(System.lineSeparator());
        sb.append("Vehicle type: ").append(getVehicle().getType()).append(System.lineSeparator());
        sb.append(String.format("Travel costs: %.2f", calculateTravelCosts())).append(System.lineSeparator());
        return sb.toString();
    }
    //Journey ----
    //Start location: Sofia
    //Destination: Turnovo
    //Distance: 300
    //Vehicle type: LAND
    //Travel costs: 210.00
}