package com.telerikacademy.oop.agency.models.vehicles;

import com.telerikacademy.oop.agency.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.agency.models.contracts.Identifiable;
import com.telerikacademy.oop.agency.models.contracts.Printable;
import com.telerikacademy.oop.agency.models.vehicles.contracts.Vehicle;
import com.telerikacademy.oop.agency.utils.ValidationHelper;

public abstract class VehicleBase implements Vehicle, Identifiable{

   private static final int MIN_PASSENGER_CAPACITY = 1;
   private static final int MAX_PASSENGER_CAPACITY = 800;
   private static final double MIN_PRICE_PER_KILOMETER = 0.10;
   private static final double MAX_PRICE_PER_KILOMETER = 2.50;
    private int passengerCapacity;
    private VehicleType type;
    private double pricePerKilometer;
    private int id;

    public VehicleBase(int id, int passengerCapacity, double pricePerKilometer, VehicleType type) {
        this.id = id;
        setPassengerCapacity(passengerCapacity);
        setPricePerKilometer(pricePerKilometer);
        setType(type);
    }

    private void setPassengerCapacity(int passengerCapacity) {
        ValidationHelper.validateValueInRange(passengerCapacity, MIN_PASSENGER_CAPACITY, MAX_PASSENGER_CAPACITY,
                String.format("A vehicle with less than %d passengers or more than %d passengers cannot exist!",
                        MIN_PASSENGER_CAPACITY, MAX_PASSENGER_CAPACITY));
        validateCapacity(passengerCapacity);
        this.passengerCapacity = passengerCapacity;
    }

    private void setType(VehicleType type) {
        if (!type.equals(VehicleType.AIR) && !type.equals(VehicleType.LAND) && !type.equals(VehicleType.SEA)) {
            throw new InvalidUserInputException("Invalid vehicle type!");
        }
        this.type = type;
    }

    private void setPricePerKilometer(double pricePerKilometer) {
        ValidationHelper.validateValueInRange(pricePerKilometer, MIN_PRICE_PER_KILOMETER, MAX_PRICE_PER_KILOMETER,
                String.format("A vehicle with a price per kilometer lower than $%.2f or higher than $%.2f cannot exist!"
                        , MIN_PRICE_PER_KILOMETER, MAX_PRICE_PER_KILOMETER));

        validatePrice(pricePerKilometer);
        this.pricePerKilometer = pricePerKilometer;

    }

    protected abstract void validateCapacity(int capacity);

    protected abstract void validatePrice(double price);


    @Override
    public VehicleType getType() {
        return type;
    }

    @Override
    public int getPassengerCapacity() {
        return passengerCapacity;
    }

    @Override
    public double getPricePerKilometer() {
        return pricePerKilometer;
    }

    @Override
    public int getId() {
        return id;
    }
    @Override
    public String getAsString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Passenger capacity: ").append(getPassengerCapacity()).append(System.lineSeparator());
        sb.append(String.format("Price per kilometer: %.2f", getPricePerKilometer())).append(System.lineSeparator());
        sb.append("Vehicle type: ").append(getType()).append(System.lineSeparator());
        return sb.toString();

    }
    //Bus ----
    //Passenger capacity: 10
    //Price per kilometer: 0.70
    //Vehicle type: LAND
    //####################
}
