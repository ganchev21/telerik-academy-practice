package com.telerikacademy.oop.cosmetics.models;

import com.telerikacademy.oop.cosmetics.models.contracts.Product;
import com.telerikacademy.oop.cosmetics.models.enums.GenderType;

public abstract class CommonClass implements Product {
    protected String name;
    protected String brand;
    protected double price;
    protected GenderType gender;

    public CommonClass(String name, String brand, double price, GenderType gender) {
        setName(name);
        setBrand(brand);
        setPrice(price);
        setGender(gender);
    }



    public void setPrice(double price) {
        if (price < 0) {
            throw new IllegalArgumentException("Price cannot be negative");
        } else {
            this.price = price;
        }
    }

    public void setGender(GenderType gender) {
        if (gender.equals(GenderType.WOMEN) || gender.equals(GenderType.MEN)
        || gender.equals(GenderType.UNISEX)) {
            this.gender = gender;
        } else {
            throw new IllegalArgumentException("Unknown gender!");
        }
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        validateName(name);
        this.name = name;
    }

    public void setBrand(String brand) {
        validateBrand(brand);
        this.brand = brand;
    }

    @Override
    public String getBrandName() {
        return brand;
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public GenderType getGenderType() {
        return gender;
    }

    public abstract void validateName (String name);

    public abstract void validateBrand (String brand);
    @Override
    public String print() {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("#%s %s",name, brand)).append(System.lineSeparator());
        sb.append(String.format(" #Price: %.2f", price)).append(System.lineSeparator());
        sb.append(String.format(" #Gender: %s", gender)).append(System.lineSeparator());
        return sb.toString();

    }
}
//#MyMan Nivea
// #Price: $10.99
// #Gender: Men
// #Milliliters: 1000
// #Usage: EveryDay
