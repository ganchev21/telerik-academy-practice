package com.telerikacademy.oop.cosmetics.commands;

import com.telerikacademy.oop.cosmetics.core.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.contracts.CosmeticsRepository;
import com.telerikacademy.oop.cosmetics.models.enums.GenderType;
import com.telerikacademy.oop.cosmetics.utils.ParsingHelpers;
import com.telerikacademy.oop.cosmetics.utils.ValidationHelpers;

import java.util.Collections;
import java.util.List;

public class CreateToothpasteCommand implements Command {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 5;

    private final CosmeticsRepository cosmeticsRepository;

    public CreateToothpasteCommand(CosmeticsRepository cosmeticsRepository) {
        this.cosmeticsRepository = cosmeticsRepository;
    }

    private String toothpasteExist(String name, String brandName, double price,
                                   GenderType genderType, List<String> ingredients) {
        if (cosmeticsRepository.productExist(name)) {
            throw new IllegalArgumentException(String.format(ParsingHelpers.PRODUCT_NAME_ALREADY_EXISTS, "Toothpaste", name));
        }
        cosmeticsRepository.createToothpaste(name, brandName, price, genderType, ingredients);

        return String.format(ParsingHelpers.PRODUCT_CREATED,"Toothpaste", name);

    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        String name = parameters.get(0);
        String brand = parameters.get(1);
        double price = ParsingHelpers.tryParseDouble(parameters.get(2), ParsingHelpers.INVALID_PRICE);
        GenderType gender = ParsingHelpers.tryParseGender(parameters.get(3));
        List<String> ingredients = Collections.singletonList(parameters.get(4));

        return toothpasteExist(name, brand, price, gender, ingredients);
        // throw new UnsupportedOperationException("Not implemented yet.");
    }

}