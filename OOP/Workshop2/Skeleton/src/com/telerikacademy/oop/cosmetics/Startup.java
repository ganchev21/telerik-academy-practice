package com.telerikacademy.oop.cosmetics;

import com.telerikacademy.oop.cosmetics.core.CosmeticsEngineImpl;

public class Startup {

    public static void main(String[] args) {
        CosmeticsEngineImpl engine = new CosmeticsEngineImpl();
        engine.start();
    }

}
//test Cream
//CreateShampoo MyMan Nivea 10.99 Men 1000 Every_Day
//CreateToothpaste White Colgate 10.99 Men calcium,fluorid
//CreateCream MyWoman Nivea 29.99 Men Vanilla
//CreateCategory Shampoos
//CreateCategory Toothpastes
//CreateCategory Creams
//AddToCategory Shampoos MyMan
//AddToCategory Toothpastes White
//AddToCategory Creams MyWoman
//AddToShoppingCart MyMan
//AddToShoppingCart White
//AddToShoppingCart MyWoman
//ShowCategory Shampoos
//ShowCategory Toothpastes
//ShowCategory Creams
//TotalPrice
//RemoveFromCategory Shampoos MyMan
//ShowCategory Shampoos
//RemoveFromShoppingCart MyMan
//TotalPrice
//Exit

//Wrong message when a product is created

//unnecessary implements Product