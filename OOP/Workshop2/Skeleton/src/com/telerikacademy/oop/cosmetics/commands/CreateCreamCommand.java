package com.telerikacademy.oop.cosmetics.commands;

import com.telerikacademy.oop.cosmetics.core.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.contracts.CosmeticsRepository;
import com.telerikacademy.oop.cosmetics.models.enums.GenderType;
import com.telerikacademy.oop.cosmetics.models.enums.ScentType;
import com.telerikacademy.oop.cosmetics.utils.ParsingHelpers;
import com.telerikacademy.oop.cosmetics.utils.ValidationHelpers;

import java.util.List;

public class CreateCreamCommand implements Command {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 5;

    private final CosmeticsRepository cosmeticsRepository;

    public CreateCreamCommand(CosmeticsRepository cosmeticsRepository) {
        this.cosmeticsRepository = cosmeticsRepository;
    }

    public String createCream (String name, String brand, double price, GenderType gender, ScentType scent) {
        if (cosmeticsRepository.productExist(name)) {
            throw new IllegalArgumentException(String.format("Cream with name %s already exists!", name));
        }
        cosmeticsRepository.createCream(name, brand, price, gender, scent);
        return String.format("Cream with name %s was created!", name);
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        String name = parameters.get(0);
        String brand = parameters.get(1);
        double price = ParsingHelpers.tryParseDouble(parameters.get(2), ParsingHelpers.INVALID_PRICE);
        GenderType gender = ParsingHelpers.tryParseGender(parameters.get(3));
        ScentType scent = ParsingHelpers.tryParseScent(parameters.get(4));

        return createCream(name, brand, price, gender, scent);
    }

}
