package com.telerikacademy.oop.cosmetics.models.contracts;

import java.util.List;

public interface Category {

    String getName();

    void addProduct(Product product);

    void removeProduct(Product product);

    String print();

    List<Product> getProducts ();

}
