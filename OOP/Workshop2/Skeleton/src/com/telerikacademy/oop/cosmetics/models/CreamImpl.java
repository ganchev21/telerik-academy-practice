package com.telerikacademy.oop.cosmetics.models;

import com.telerikacademy.oop.cosmetics.models.contracts.Cream;
import com.telerikacademy.oop.cosmetics.models.enums.GenderType;
import com.telerikacademy.oop.cosmetics.models.enums.ScentType;
import com.telerikacademy.oop.cosmetics.utils.ValidationHelpers;

public class CreamImpl extends CommonClass implements Cream {
    //name minimum 3 symbols and maximum 15
    //brand minimum 3 symbols and maximum 15
    private static final int CREAM_NAME_MIN_LENGTH = 3;
    private static final int CREAM_NAME_MAX_LENGTH = 15;
    private static final int CREAM_BRAND_MIN_LENGTH = 3;
    private static final int CREAM_BRAND_MAX_LENGTH = 15;

    private ScentType scent;
    public CreamImpl(String name, String brand, double price, GenderType gender, ScentType scent) {
        super(name, brand, price, gender);
        setScent(scent);
    }

    public void setScent(ScentType scent) {
        if (scent.equals(ScentType.LAVENDER) || scent.equals(ScentType.ROSE)
                || scent.equals(ScentType.VANILLA)) {
            this.scent = scent;
        } else {
            throw new IllegalArgumentException("Unknown scent!");
        }
    }


    @Override
    public void validateName(String name) {
        ValidationHelpers.validateStringLength(name, CREAM_NAME_MIN_LENGTH, CREAM_NAME_MAX_LENGTH, "Name");
    }

    @Override
    public void validateBrand(String brand) {
        ValidationHelpers.validateStringLength(brand, CREAM_BRAND_MIN_LENGTH, CREAM_BRAND_MAX_LENGTH, "Brand");

    }

    @Override
    public ScentType getScent() {
        return scent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreamImpl cream = (CreamImpl) o;
        return getName().equals(cream.getName()) &&
                getBrandName().equals(cream.getBrandName()) &&
                getPrice() == cream.getPrice() &&
                getGenderType().equals(cream.getGenderType()) &&
                getScent().equals(cream.getScent());
    }
    @Override
    public String print () {
        StringBuilder sb = new StringBuilder();
        sb.append(super.print());
        sb.append(String.format(" #Scent: %s", getScent())).append(System.lineSeparator());
        return sb.toString();
    }
}
