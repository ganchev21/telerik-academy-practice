package com.telerikacademy.oop.cosmetics.models.contracts;

import com.telerikacademy.oop.cosmetics.models.enums.GenderType;

import java.util.List;

public interface Toothpaste extends Product{

    List<String> getIngredients();

}
