package com.telerikacademy.oop.cosmetics.tests.commands;

import com.telerikacademy.oop.cosmetics.commands.AddToCategoryCommand;
import com.telerikacademy.oop.cosmetics.core.CosmeticsRepositoryImpl;
import com.telerikacademy.oop.cosmetics.core.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.contracts.CosmeticsRepository;
import com.telerikacademy.oop.cosmetics.models.contracts.Category;
import com.telerikacademy.oop.cosmetics.models.contracts.Product;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.telerikacademy.oop.cosmetics.tests.models.CategoryTests.VALID_CATEGORY_NAME;
import static com.telerikacademy.oop.cosmetics.tests.models.CategoryTests.addInitializedCategoryToRepository;
import static com.telerikacademy.oop.cosmetics.tests.models.ShampooTests.VALID_SHAMPOO_NAME;
import static com.telerikacademy.oop.cosmetics.tests.models.ShampooTests.addInitializedShampooToRepository;
import static com.telerikacademy.oop.cosmetics.tests.utils.TestUtilities.getList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class AddToCategoryCommandTests {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;

    private Command addToCategoryCommand;
    private CosmeticsRepository cosmeticsRepository;

    @BeforeEach
    public void before() {
        cosmeticsRepository = new CosmeticsRepositoryImpl();
        addToCategoryCommand = new AddToCategoryCommand(cosmeticsRepository);
    }

    @Test
    public void should_ThrowException_When_ArgumentCountDifferentThanExpected() {
        // Arrange
        List<String> params = getList(EXPECTED_NUMBER_OF_ARGUMENTS - 1);

        // Act, Assert
        assertThrows(IllegalArgumentException.class, () -> addToCategoryCommand.execute(params));
    }

    @Test
    public void should_ThrowException_When_CategoryDoesNotExist() {
        // Arrange
        List<String> params = List.of(VALID_CATEGORY_NAME, VALID_SHAMPOO_NAME);

        // Act, Assert
        assertThrows(IllegalArgumentException.class, () -> addToCategoryCommand.execute(params));
    }


    @Test
    public void should_ThrowException_When_ProductDoesNotExist() {
        // Arrange
        Category category = addInitializedCategoryToRepository(cosmeticsRepository);
        List<String> params = List.of(category.getName(), VALID_SHAMPOO_NAME);

        // Act, Assert
        assertThrows(IllegalArgumentException.class, () -> addToCategoryCommand.execute(params));
    }

    @Test
    public void should_AddProductToCategory_When_ArgumentsAreValid() {
        // Arrange
        Category category = addInitializedCategoryToRepository(cosmeticsRepository);
        Product product = addInitializedShampooToRepository(cosmeticsRepository);
        List<String> params = List.of(category.getName(), product.getName());
        addToCategoryCommand.execute(params);

        // Act, Assert
        assertEquals(1, cosmeticsRepository.getCategories().size());
    }
}
