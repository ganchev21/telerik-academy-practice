package com.telerikacademy.oop.cosmetics.tests.commands;

import com.telerikacademy.oop.cosmetics.commands.CreateCategoryCommand;
import com.telerikacademy.oop.cosmetics.core.CosmeticsRepositoryImpl;
import com.telerikacademy.oop.cosmetics.core.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.contracts.CosmeticsRepository;
import com.telerikacademy.oop.cosmetics.models.contracts.Category;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.telerikacademy.oop.cosmetics.tests.models.CategoryTests.VALID_CATEGORY_NAME;
import static com.telerikacademy.oop.cosmetics.tests.models.CategoryTests.addInitializedCategoryToRepository;
import static com.telerikacademy.oop.cosmetics.tests.utils.TestUtilities.getList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CreateCategoryCommandTests {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;

    Command createCategoryCommand;
    CosmeticsRepository cosmeticsRepository;

    @BeforeEach
    public void beforeEach() {
        cosmeticsRepository = new CosmeticsRepositoryImpl();
        createCategoryCommand = new CreateCategoryCommand(cosmeticsRepository);
    }

    @Test
    public void should_ThrowException_When_ArgumentCountDifferentThanExpected() {
        // Arrange
        List<String> params = getList(EXPECTED_NUMBER_OF_ARGUMENTS - 1);

        // Act, Assert
        assertThrows(IllegalArgumentException.class, () -> createCategoryCommand.execute(params));
    }

    @Test
    public void should_ThrowException_When_NameExist() {
        // Arrange, Act
        Category category = addInitializedCategoryToRepository(cosmeticsRepository);
        List<String> parameters = List.of(category.getName());

        // Assert
        assertThrows(IllegalArgumentException.class, () -> createCategoryCommand.execute(parameters));
    }

    @Test
    public void should_AddToList_When_ArgumentsAreValid() {
        // Arrange
        List<String> parameters = List.of(VALID_CATEGORY_NAME);

        // Act
        createCategoryCommand.execute(parameters);

        // Assert
        assertEquals(1, cosmeticsRepository.getCategories().size());
    }
}
