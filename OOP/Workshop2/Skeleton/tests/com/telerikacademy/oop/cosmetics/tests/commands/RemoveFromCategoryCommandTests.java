package com.telerikacademy.oop.cosmetics.tests.commands;

import com.telerikacademy.oop.cosmetics.commands.RemoveFromCategoryCommand;
import com.telerikacademy.oop.cosmetics.core.CosmeticsRepositoryImpl;
import com.telerikacademy.oop.cosmetics.core.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.contracts.CosmeticsRepository;
import com.telerikacademy.oop.cosmetics.models.contracts.Category;
import com.telerikacademy.oop.cosmetics.models.contracts.Product;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.telerikacademy.oop.cosmetics.tests.models.CategoryTests.VALID_CATEGORY_NAME;
import static com.telerikacademy.oop.cosmetics.tests.models.CategoryTests.addInitializedCategoryToRepository;
import static com.telerikacademy.oop.cosmetics.tests.models.ShampooTests.VALID_SHAMPOO_NAME;
import static com.telerikacademy.oop.cosmetics.tests.models.ShampooTests.addInitializedShampooToRepository;
import static com.telerikacademy.oop.cosmetics.tests.utils.TestUtilities.getList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class RemoveFromCategoryCommandTests {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;

    private Command removeFromCategoryCommand;
    private CosmeticsRepository cosmeticsRepository;

    @BeforeEach
    public void before() {
        cosmeticsRepository = new CosmeticsRepositoryImpl();
        removeFromCategoryCommand = new RemoveFromCategoryCommand(cosmeticsRepository);
    }

    @Test
    public void should_ThrowException_When_ArgumentCountDifferentThanExpected() {
        // Arrange
        List<String> params = getList(EXPECTED_NUMBER_OF_ARGUMENTS - 1);

        // Act, Assert
        assertThrows(IllegalArgumentException.class, () -> removeFromCategoryCommand.execute(params));
    }

    @Test
    public void should_ThrowException_When_CategoryDoesNotExist() {
        // Arrange
        Product product = addInitializedShampooToRepository(cosmeticsRepository);
        List<String> params = List.of(VALID_CATEGORY_NAME, product.getName());

        // Act, Assert
        assertThrows(IllegalArgumentException.class, () -> removeFromCategoryCommand.execute(params));
    }

    @Test
    public void should_ThrowException_When_ProductDoesNotExist() {
        // Arrange
        Category category = addInitializedCategoryToRepository(cosmeticsRepository);
        List<String> params = List.of(category.getName(), VALID_SHAMPOO_NAME);

        // Act, Assert
        assertThrows(IllegalArgumentException.class, () -> removeFromCategoryCommand.execute(params));
    }

    @Test
    public void should_RemoveFromCategory_When_ArgumentsAreValid() {
        // Arrange
        Product testProduct = addInitializedShampooToRepository(cosmeticsRepository);
        Category testCategory = addInitializedCategoryToRepository(cosmeticsRepository);
        testCategory.addProduct(testProduct);
        List<String> params = List.of(testCategory.getName(), testProduct.getName());
        removeFromCategoryCommand.execute(params);

        // Act, Assert
        assertEquals(0, testCategory.getProducts().size());
    }
}
