package com.telerikacademy.oop.cosmetics.tests.commands;

import com.telerikacademy.oop.cosmetics.commands.CreateToothpasteCommand;
import com.telerikacademy.oop.cosmetics.core.CosmeticsRepositoryImpl;
import com.telerikacademy.oop.cosmetics.core.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.contracts.CosmeticsRepository;
import com.telerikacademy.oop.cosmetics.models.contracts.Product;
import com.telerikacademy.oop.cosmetics.models.enums.GenderType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.telerikacademy.oop.cosmetics.tests.models.ShampooTests.addInitializedShampooToRepository;
import static com.telerikacademy.oop.cosmetics.tests.models.ToothpasteTests.VALID_TOOTHPASTE_BRAND_NAME;
import static com.telerikacademy.oop.cosmetics.tests.models.ToothpasteTests.VALID_TOOTHPASTE_NAME;
import static com.telerikacademy.oop.cosmetics.tests.utils.TestUtilities.getList;
import static org.junit.jupiter.api.Assertions.*;

public class CreateToothpasteCommandTests {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 5;

    Command createToothpasteCommand;
    CosmeticsRepository cosmeticsRepository;

    @BeforeEach
    public void beforeEach() {
        cosmeticsRepository = new CosmeticsRepositoryImpl();
        createToothpasteCommand = new CreateToothpasteCommand(cosmeticsRepository);
    }

    @Test
    public void should_ThrowException_When_ArgumentCountDifferentThanExpected() {
        // Arrange
        List<String> params = getList(EXPECTED_NUMBER_OF_ARGUMENTS - 1);

        // Act, Assert
        assertThrows(IllegalArgumentException.class, () -> createToothpasteCommand.execute(params));
    }

    @Test
    public void should_ThrowException_When_PriceInvalid() {
        //Arrange
        List<String> parameters = List.of(
                VALID_TOOTHPASTE_NAME,
                VALID_TOOTHPASTE_BRAND_NAME,
                "Invalid Price",
                GenderType.MEN.toString(),
                "test1,test2,test3");
        //Act, Assert
        assertThrows(IllegalArgumentException.class, () -> createToothpasteCommand.execute(parameters));
    }

    @Test
    public void should_ThrowException_When_GenderInvalid() {
        //Arrange
        List<String> parameters = List.of(
                VALID_TOOTHPASTE_NAME,
                VALID_TOOTHPASTE_BRAND_NAME,
                "10",
                "Invalid Gender",
                "test1,test2,test3");
        //Act, Assert
        assertThrows(IllegalArgumentException.class, () -> createToothpasteCommand.execute(parameters));
    }

    @Test
    public void should_ThrowException_When_NameExists() {
        //Arrange
        Product testProduct = addInitializedShampooToRepository(cosmeticsRepository);

        List<String> parameters = List.of(
                testProduct.getName(),
                VALID_TOOTHPASTE_BRAND_NAME,
                "10.75",
                GenderType.MEN.toString(),
                "test1,test2,test3");

        //Act, Assert
        assertThrows(IllegalArgumentException.class, () -> createToothpasteCommand.execute(parameters));
    }

    @Test
    public void should_Return_InitializedProduct() {
        // Arrange, Act
        List<String> parameters = List.of(
                VALID_TOOTHPASTE_NAME,
                VALID_TOOTHPASTE_BRAND_NAME,
                "10.75",
                GenderType.MEN.toString(),
                "test1,test2,test3");
        createToothpasteCommand.execute(parameters);

        // Assert
        Product toothpaste = cosmeticsRepository.findProductByName(VALID_TOOTHPASTE_NAME);
        assertAll(
                () -> assertEquals(toothpaste.getBrandName(), VALID_TOOTHPASTE_BRAND_NAME),
                () -> assertEquals(toothpaste.getPrice(), 10.75),
                () -> assertEquals(toothpaste.getGenderType(), GenderType.MEN)
        );
    }

    @Test
    public void should_addToList_when_argumentsAreValid() {
        // Arrange
        List<String> parameters = List.of(
                VALID_TOOTHPASTE_NAME,
                VALID_TOOTHPASTE_BRAND_NAME,
                "10.75",
                GenderType.MEN.toString(),
                "test1,test2,test3");

        //Act, Assert
        assertAll(
                () -> assertDoesNotThrow(() -> createToothpasteCommand.execute(parameters)),
                () -> assertEquals(1, cosmeticsRepository.getProducts().size())
        );
    }

}
