package com.telerikacademy.oop.cosmetics.tests.commands;

import com.telerikacademy.oop.cosmetics.commands.ShowCategoryCommand;
import com.telerikacademy.oop.cosmetics.core.CosmeticsRepositoryImpl;
import com.telerikacademy.oop.cosmetics.core.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.contracts.CosmeticsRepository;
import com.telerikacademy.oop.cosmetics.models.contracts.Category;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.telerikacademy.oop.cosmetics.tests.models.CategoryTests.VALID_CATEGORY_NAME;
import static com.telerikacademy.oop.cosmetics.tests.models.CategoryTests.addInitializedCategoryToRepository;
import static com.telerikacademy.oop.cosmetics.tests.utils.TestUtilities.getList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ShowCategoryCommandTests {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;

    private Command showCategoryCommand;
    private CosmeticsRepository cosmeticsRepository;

    @BeforeEach
    public void before() {
        cosmeticsRepository = new CosmeticsRepositoryImpl();
        showCategoryCommand = new ShowCategoryCommand(cosmeticsRepository);
    }

    @Test
    public void should_ThrowException_When_ArgumentCountDifferentThanExpected() {
        // Arrange
        List<String> params = getList(EXPECTED_NUMBER_OF_ARGUMENTS - 1);

        // Act, Assert
        assertThrows(IllegalArgumentException.class, () -> showCategoryCommand.execute(params));
    }

    @Test
    public void should_ThrowException_When_CategoryDoesNotExist() {
        // Arrange
        List<String> params = List.of(VALID_CATEGORY_NAME);

        // Act, Assert
        assertThrows(IllegalArgumentException.class, () -> showCategoryCommand.execute(params));
    }

    @Test
    public void should_ShowCategory_When_ArgumentsAreValid() {
        // Arrange
        Category category = addInitializedCategoryToRepository(cosmeticsRepository);
        showCategoryCommand.execute(List.of(category.getName()));

        // Act, Assert
        assertEquals(1, cosmeticsRepository.getCategories().size());
    }
}
