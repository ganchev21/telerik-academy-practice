package com.telerikacademy.oop.cosmetics.tests.models;

import com.telerikacademy.oop.cosmetics.core.contracts.CosmeticsRepository;
import com.telerikacademy.oop.cosmetics.models.CategoryImpl;
import com.telerikacademy.oop.cosmetics.models.contracts.Category;
import com.telerikacademy.oop.cosmetics.models.contracts.Product;
import org.junit.jupiter.api.Test;

import static com.telerikacademy.oop.cosmetics.tests.models.ShampooTests.initializeTestShampoo;
import static com.telerikacademy.oop.cosmetics.tests.utils.TestUtilities.getString;
import static org.junit.jupiter.api.Assertions.*;

public class CategoryTests {

    public static final int NAME_MIN_LENGTH = 2;
    public static final int NAME_MAX_LENGTH = 15;
    public static final String INVALID_CATEGORY_NAME = getString(NAME_MAX_LENGTH + 1);
    public static final String VALID_CATEGORY_NAME = getString(NAME_MIN_LENGTH + 1);

    @Test
    public void add_Should_AddProduct_When_ProductIsValid() {
        // Arrange
        Category category = initializeTestCategory();
        Product productToAdd = initializeTestShampoo();

        // Act, Assert
        assertAll(
                () -> assertDoesNotThrow(() -> category.addProduct(productToAdd)),
                () -> assertEquals(1, category.getProducts().size())
        );
    }

    @Test
    public void construct_Should_ThrowException_When_CategoryNameIsInvalid() {
        // Arrange, Act, Assert
        assertThrows(IllegalArgumentException.class, () -> new CategoryImpl(INVALID_CATEGORY_NAME));
    }

    @Test
    public void construct_Should_CreateCategory_When_NameIsValid() {
        // Arrange, Act, Assert
        assertDoesNotThrow(() -> new CategoryImpl(VALID_CATEGORY_NAME));
    }

    @Test
    public void construct_Should_InitializeNewListOfProducts_When_CategoryIsCreated() {
        // Arrange, Act
        Category category = initializeTestCategory();

        // Assert
        assertNotNull(category.getProducts());
    }

    @Test
    public void remove_Should_RemoveProduct_When_ProductIsValid() {
        // Arrange, Act
        Category category = initializeTestCategory();
        Product product = initializeTestShampoo();
        category.addProduct(product);

        // Act, Assert
        assertAll(
                () -> assertDoesNotThrow(() -> category.removeProduct(product)),
                () -> assertEquals(0, category.getProducts().size())
        );
    }

    @Test
    public void remove_Should_ThrowException_When_ProductNotFound() {
        // Arrange, Act
        Category category = initializeTestCategory();
        Product productToRemove = initializeTestShampoo();

        // Act, Assert
        assertThrows(IllegalArgumentException.class, () -> category.removeProduct(productToRemove));
    }

    public static Category addInitializedCategoryToRepository(CosmeticsRepository repository) {
        return repository.createCategory(VALID_CATEGORY_NAME);
    }

    public static CategoryImpl initializeTestCategory() {
        return new CategoryImpl(VALID_CATEGORY_NAME);
    }

}
