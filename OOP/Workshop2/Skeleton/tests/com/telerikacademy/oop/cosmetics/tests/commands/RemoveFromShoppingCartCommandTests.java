package com.telerikacademy.oop.cosmetics.tests.commands;

import com.telerikacademy.oop.cosmetics.commands.RemoveFromShoppingCartCommand;
import com.telerikacademy.oop.cosmetics.core.CosmeticsRepositoryImpl;
import com.telerikacademy.oop.cosmetics.core.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.contracts.CosmeticsRepository;
import com.telerikacademy.oop.cosmetics.models.contracts.Product;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.telerikacademy.oop.cosmetics.tests.models.ShampooTests.VALID_SHAMPOO_NAME;
import static com.telerikacademy.oop.cosmetics.tests.models.ShampooTests.addInitializedShampooToRepository;
import static com.telerikacademy.oop.cosmetics.tests.utils.TestUtilities.getList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class RemoveFromShoppingCartCommandTests {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;

    private Command removeFromShoppingCartCommand;
    private CosmeticsRepository cosmeticsRepository;

    @BeforeEach
    public void before() {
        cosmeticsRepository = new CosmeticsRepositoryImpl();
        removeFromShoppingCartCommand = new RemoveFromShoppingCartCommand(cosmeticsRepository);
    }

    @Test
    public void should_ThrowException_When_ArgumentCountDifferentThanExpected() {
        // Arrange
        List<String> params = getList(EXPECTED_NUMBER_OF_ARGUMENTS - 1);

        // Act, Assert
        assertThrows(IllegalArgumentException.class, () -> removeFromShoppingCartCommand.execute(params));
    }

    @Test
    public void should_ThrowException_When_ProductDoesNotExist() {
        // Arrange
        List<String> params = List.of(VALID_SHAMPOO_NAME);

        // Act, Assert
        assertThrows(IllegalArgumentException.class, () -> removeFromShoppingCartCommand.execute(params));
    }

    @Test
    public void should_RemoveFromShoppingCart_When_ArgumentsAreValid() {
        // Arrange
        Product product = addInitializedShampooToRepository(cosmeticsRepository);
        cosmeticsRepository.getShoppingCart().addProduct(product);
        List<String> params = List.of(product.getName());
        removeFromShoppingCartCommand.execute(params);

        // Act, Assert
        assertEquals(0, cosmeticsRepository.getShoppingCart().getProducts().size());
    }
}
