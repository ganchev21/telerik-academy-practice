package com.telerikacademy.oop.furniture.exceptions;

public class ElementNotFoundException extends RuntimeException {

    public ElementNotFoundException(String message) {
        super(message);
    }

}
