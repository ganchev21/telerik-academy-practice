package com.telerikacademy.oop.furniture;

import com.telerikacademy.oop.furniture.core.FurnitureEngineImpl;
import com.telerikacademy.oop.furniture.models.AdjustableChairImpl;
import com.telerikacademy.oop.furniture.models.ChairImpl;
import com.telerikacademy.oop.furniture.models.ConvertibleChairImpl;
import com.telerikacademy.oop.furniture.models.contracts.AdjustableChair;
import com.telerikacademy.oop.furniture.models.contracts.ConvertibleChair;
import com.telerikacademy.oop.furniture.models.contracts.Furniture;
import com.telerikacademy.oop.furniture.models.enums.ChairType;
import com.telerikacademy.oop.furniture.models.enums.MaterialType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Startup {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);


//        ConvertibleChair adjChair = new ConvertibleChairImpl(
//                "AASSSDD", MaterialType.WOODEN, 3.42, 3.2, 4, ChairType.CONVERTIBLE);
//        System.out.println(adjChair.getHeight());
//        adjChair.convert();
//        System.out.println(adjChair.getHeight());
//        System.out.println(adjChair.toString());
//        adjChair.setHeight(7);
//        adjChair.convert();
//        System.out.println(adjChair.toString());
//        FurnitureEngineImpl engine = new FurnitureEngineImpl();
//        engine.start();
        List<String> input = Arrays.stream(scanner.nextLine().split("#")).toList();
        System.out.println(input);

    }

}
