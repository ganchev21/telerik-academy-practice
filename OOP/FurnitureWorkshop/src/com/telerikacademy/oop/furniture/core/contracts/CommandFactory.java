package com.telerikacademy.oop.furniture.core.contracts;

import com.telerikacademy.oop.furniture.commands.contracts.Command;

public interface CommandFactory {

    Command createCommandFromCommandName(String commandTypeAsString, FurnitureRepository furnitureRepository);

}
