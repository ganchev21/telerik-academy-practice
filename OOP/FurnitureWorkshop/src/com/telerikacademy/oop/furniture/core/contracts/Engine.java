package com.telerikacademy.oop.furniture.core.contracts;

public interface Engine {

    void start();

}
