package com.telerikacademy.oop.furniture.models;

import com.telerikacademy.oop.furniture.models.contracts.Chair;
import com.telerikacademy.oop.furniture.models.contracts.Furniture;
import com.telerikacademy.oop.furniture.models.enums.ChairType;
import com.telerikacademy.oop.furniture.models.enums.MaterialType;
import com.telerikacademy.oop.furniture.utils.ValidationHelper;

public class ChairBase extends FurnitureBase implements Furniture, Chair {

    private int legs;
    private ChairType type;

    public ChairBase(String modelCode, MaterialType material, double price, double height,int legs, ChairType type) {
        super(modelCode, material, price, height);
        setLegs(legs);
        this.type = type;

    }


    private void setLegs(int legs) {
        ValidationHelper.validateGreaterThanZero(legs);
        this.legs = legs;
    }

    @Override
    public int getNumberOfLegs() {
        return legs;
    }

    public ChairType getType() {
        return type;
    }

}
