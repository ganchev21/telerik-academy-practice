package com.telerikacademy.oop.furniture.models;

import com.telerikacademy.oop.furniture.models.contracts.Chair;
import com.telerikacademy.oop.furniture.models.enums.ChairType;
import com.telerikacademy.oop.furniture.models.enums.MaterialType;
import com.telerikacademy.oop.furniture.models.enums.ChairType;
import com.telerikacademy.oop.furniture.utils.ValidationHelper;

public class ChairImpl extends ChairBase {


    public ChairImpl(String modelCode, MaterialType material, double price, double height, int legs) {
        super(modelCode, material, price, height, legs, ChairType.REGULAR);
    }

    public ChairImpl(String modelCode, MaterialType material, double price, double height, int legs, ChairType type) {
        this(modelCode, material, price, height, legs);

    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Type: Chair").append(super.toString()).append(String.format("Legs: %d", getNumberOfLegs()));
        return sb.toString();
    }
}
