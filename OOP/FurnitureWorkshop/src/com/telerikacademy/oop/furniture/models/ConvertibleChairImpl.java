package com.telerikacademy.oop.furniture.models;

import com.telerikacademy.oop.furniture.models.contracts.ConvertibleChair;
import com.telerikacademy.oop.furniture.models.contracts.Furniture;
import com.telerikacademy.oop.furniture.models.enums.ChairType;
import com.telerikacademy.oop.furniture.models.enums.MaterialType;
import com.telerikacademy.oop.furniture.utils.ValidationHelper;

public class ConvertibleChairImpl extends ChairBase implements ConvertibleChair {

    public static final double CONVERTED_HEIGHT = 0.1;
    private String state = "Normal";
    private double height;


    public ConvertibleChairImpl(String modelCode, MaterialType material, double price, double height, int legs) {
        super(modelCode, material, price, height, legs, ChairType.CONVERTIBLE);
        this.height = height;
    }
    public ConvertibleChairImpl(String modelCode, MaterialType material, double price, double height, int legs, ChairType type) {
        this(modelCode, material, price, height, legs);
    }

    @Override
    public boolean getIsConverted() {
        if (state.equalsIgnoreCase("normal")) {
            return false;
        }
        return true;
    }

    @Override
    public void convert() {
        if (state.equalsIgnoreCase("normal")) {
            state = "Converted";
            super.setHeight(CONVERTED_HEIGHT);
        } else {
            state = "Normal";
            super.setHeight(height);
        }
    }
    @Override
    public double getHeight() {
        return super.getHeight();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Type: Convertible Chair").append(super.toString()).append(
                String.format("Legs: %d, State: %s", getNumberOfLegs(), state));
        return sb.toString();
    }
}
