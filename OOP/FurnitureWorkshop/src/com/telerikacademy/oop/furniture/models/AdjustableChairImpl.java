package com.telerikacademy.oop.furniture.models;

import com.telerikacademy.oop.furniture.models.contracts.AdjustableChair;
import com.telerikacademy.oop.furniture.models.contracts.Chair;
import com.telerikacademy.oop.furniture.models.enums.ChairType;
import com.telerikacademy.oop.furniture.models.enums.MaterialType;
import com.telerikacademy.oop.furniture.utils.ValidationHelper;

public class AdjustableChairImpl extends ChairBase implements AdjustableChair{


    public AdjustableChairImpl(String modelCode, MaterialType material, double price, double height, int legs) {
        super(modelCode, material, price, height, legs, ChairType.ADJUSTABLE);
        //this.height = height;
    }
    public AdjustableChairImpl(String modelCode, MaterialType material, double price, double height, int legs, ChairType type) {
        this(modelCode, material, price, height, legs);
    }

    @Override
    public void setHeight(double newHeight) {
        ValidationHelper.validateGreaterThanZero(newHeight);
        super.setHeight(newHeight);
    }
    @Override
    public double getHeight() {
        return super.getHeight();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Type: Adjustable Chair").append(super.toString()).append(
                String.format("Legs: %d", getNumberOfLegs()));
        return sb.toString();
    }

}
