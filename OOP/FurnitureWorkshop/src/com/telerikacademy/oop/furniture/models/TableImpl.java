package com.telerikacademy.oop.furniture.models;

import com.telerikacademy.oop.furniture.models.contracts.Table;
import com.telerikacademy.oop.furniture.models.enums.MaterialType;
import com.telerikacademy.oop.furniture.utils.ValidationHelper;

public class TableImpl extends FurnitureBase implements Table {
    //Area: calculated by the following formula: length * width.
    //Length: Should be greater than zero.
    //Width: Should be greater than zero.
    private double length;
    private double width;

    public TableImpl(String modelCode, MaterialType material, double price, double height, double length, double width) {
        super(modelCode, material, price, height);
        setLength(length);
        setWidth(width);
    }


    private void setLength(double length) {
        ValidationHelper.validateGreaterThanZero(length);
        this.length = length;
    }

    private void setWidth(double width) {
        ValidationHelper.validateGreaterThanZero(width);
        this.width = width;
    }

    @Override
    public double getLength() {
        return length;
    }

    @Override
    public double getWidth() {
        return width;
    }

    @Override
    public double getArea() {
        return width * length;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Type: Table").append(super.toString()).append(String.format("Length: %.2f, Width: %.2f, Area: %.2f",
                        length, width, getArea()));
        return sb.toString();
    }
}
