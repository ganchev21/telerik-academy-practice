package com.telerikacademy.oop.furniture.models.contracts;

import com.telerikacademy.oop.furniture.models.enums.ChairType;

public interface Chair extends Furniture {

    int getNumberOfLegs();
    ChairType getType();
    String getModelCode();

}
