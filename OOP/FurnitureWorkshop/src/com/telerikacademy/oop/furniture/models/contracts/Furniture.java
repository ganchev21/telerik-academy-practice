package com.telerikacademy.oop.furniture.models.contracts;

import com.telerikacademy.oop.furniture.models.enums.MaterialType;

public interface Furniture {

    String getModelCode();

    MaterialType getMaterialType();

    double getPrice();

    double getHeight();

}
