package com.telerikacademy.oop.furniture.models;

import com.telerikacademy.oop.furniture.models.contracts.Furniture;
import com.telerikacademy.oop.furniture.models.enums.MaterialType;
import com.telerikacademy.oop.furniture.utils.ValidationHelper;

public class FurnitureBase implements Furniture {
    public static final int MODEL_CODE_MIN_LENGTH = 3;
    //Each furniture piece has model code, material type, price, and height.

    private String modelCode;
    private MaterialType material;
    private double price;
    private double height;

    public FurnitureBase(String modelCode, MaterialType material, double price, double height) {
        setModelCode(modelCode);
        this.material = material;
        setPrice(price);
        setHeight(height);
    }

    private void setModelCode(String modelCode) {
        ValidationHelper.validateStringMinLength(modelCode, MODEL_CODE_MIN_LENGTH);
        this.modelCode = modelCode;
    }


    private void setPrice(double price) {
        ValidationHelper.validateGreaterThanZero(price);
        this.price = price;
    }

    protected void setHeight(double height) {
        ValidationHelper.validateGreaterThanZero(height);
        this.height = height;
    }

    @Override
    public String getModelCode() {
        return modelCode;
    }

    @Override
    public MaterialType getMaterialType() {
        return material;
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public double getHeight() {
        return height;
    }
//- Type: Convertible Chair, Model Code: GG4201, Material: Plastic, Price: 80.00, Height: 1.00, Legs: 3, State: Normal
    @Override
    public String toString() {
        return String.format(", Model Code: %s, Material: %s, Price: %.2f, Height: %.2f, ",
                modelCode, material, price, height);
    }
}
