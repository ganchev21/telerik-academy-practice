package com.telerikacademy.oop.furniture.models.contracts;

import com.telerikacademy.oop.furniture.models.enums.ChairType;

public interface ConvertibleChair extends Furniture{

    boolean getIsConverted();

    void convert();
    ChairType getType();
}
