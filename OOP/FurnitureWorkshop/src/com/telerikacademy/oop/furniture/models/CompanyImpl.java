package com.telerikacademy.oop.furniture.models;

import com.telerikacademy.oop.furniture.exceptions.ElementNotFoundException;
import com.telerikacademy.oop.furniture.models.contracts.Company;
import com.telerikacademy.oop.furniture.models.contracts.Furniture;
import com.telerikacademy.oop.furniture.utils.ValidationHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class CompanyImpl implements Company {
    public static final int NAME_MIN_LENGTH = 5;
    public static final int REGISTRATION_LENGTH = 10;
    public static final String MESSAGE_FOR_LENGTH = "Registration Number must be exactly 10 symbols";
    public static final String ONLY_DIGITS = "Registration Number must contain only digits.";


    private String name;
    private String registrationNumber;
    private List<Furniture> furnitures;

    public CompanyImpl(String name, String registrationNumber) {
        setName(name);
        setRegistrationNumber(registrationNumber);
        furnitures = new ArrayList<>();
    }

    private void setName(String name) {
        ValidationHelper.validateStringMinLength(name, NAME_MIN_LENGTH);
        this.name = name;

    }

    private void setRegistrationNumber(String registrationNumber) {
        ValidationHelper.validateStringExactLength(registrationNumber, REGISTRATION_LENGTH, MESSAGE_FOR_LENGTH);
        ValidationHelper.validateOnlyDigits(registrationNumber, ONLY_DIGITS);
        this.registrationNumber = registrationNumber;
    }

    public String getName() {
        return name;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    @Override
    public List<Furniture> getFurniture() {
        return new ArrayList<>(furnitures);
    }

    @Override
    public void addFurniture(Furniture furniture) {
        furnitures.add(furniture);
    }

    @Override
    public void removeFurniture(Furniture furniture) {
        if (furniture != null) {
            furnitures.remove(furniture);
        }
    }

    @Override
    public Furniture findFurnitureByModelCode(String modelCode) {
        if (furnitures != null) {
            for (Furniture items : furnitures) {
                if (items.getModelCode().equalsIgnoreCase(modelCode)) {
                    return items;
                }
            }
        }
        throw new ElementNotFoundException(
                String.format("Furniture with model code %s does not exist.", modelCode));
    }
    @Override
    public String getCatalog() {
        if (furnitures.isEmpty()) {
            return String.format("Catalog of %s (%s): No furniture.",
                    name, registrationNumber);
        }
        StringBuilder strBuilder = new StringBuilder();

        Collections.sort(furnitures, Comparator.comparing(Furniture::getPrice).
                thenComparing(Furniture::getModelCode));

        strBuilder.append(String.format("Catalog of %s (%s):", name,registrationNumber)).append(System.lineSeparator());
        for (Furniture items : furnitures) {
            strBuilder.append("  - ");
            strBuilder.append(items).append(System.lineSeparator());
        }

        return strBuilder.toString().trim();
    }
}
