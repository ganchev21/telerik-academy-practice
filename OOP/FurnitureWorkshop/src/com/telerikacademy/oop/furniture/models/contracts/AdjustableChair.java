package com.telerikacademy.oop.furniture.models.contracts;

import com.telerikacademy.oop.furniture.models.enums.ChairType;

public interface AdjustableChair extends Furniture{

    void setHeight(double height);

    double getHeight();
    ChairType getType();

}
