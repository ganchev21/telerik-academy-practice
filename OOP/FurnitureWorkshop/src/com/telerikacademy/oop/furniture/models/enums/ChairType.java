package com.telerikacademy.oop.furniture.models.enums;

public enum ChairType {
    REGULAR,
    ADJUSTABLE,
    CONVERTIBLE
}
