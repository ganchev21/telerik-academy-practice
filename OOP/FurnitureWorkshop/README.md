<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg" alt="logo" width="300px" style="margin-top: 20px;"/>

## OOP Workshop - Furniture

A furniture manufacturer keeps track of the companies they work with and the furniture they are manufacturing.

- Each furniture piece has **model code, material type, price, and height**.
- Each table has **length and width in meters**.
- Chairs can be one of three types: **regular, adjustable and convertible**.
  - Each chair has **number of legs**.
  - Each adjustable chair **can adjust its height**.
  - Each convertible chair can switch its state (either "Converted" or "Normal").
- Each company has **name, registration number and catalog of the furniture they offer**. Companies can add or remove furniture from catalogs. Companies can find furniture from their list of furniture by model code. Companies can show a catalog of all furniture they offer.

### Design the Class Hierarchy

Your task is to design an object-oriented class hierarchy to model the Furniture Factory, using the best practices for object-oriented design (OOD) and object-oriented programming (OOP). Avoid duplicated code though abstraction, inheritance, and polymorphism and encapsulate correctly all fields.
You are given few Java interfaces that you must use to build a adequate interface hierarchy and use as a basis of your code.

### Validation

- Furniture:
  - Model: Cannot be empty or with less than 3 symbols.
  - Price: Cannot be less or equal to 0.00.
  - Height: Cannot be less or equal to 0.00.
- Table:
  - Area: calculated by the following formula: `length * width`.
  - Length: Should be greater than zero.
  - Width: Should be greater than zero.
- Adjustable Chair:
  - Can change height.
- Convertible Chair:
  - Has two states – `Converted` or `Normal`.
  - If the state is `Converted`, the height of the chair is `0.10`.
  - If the state is `Normal`, the height is the initial one.
  - When initialized the state is `Normal`.
- Company:
  - Name: Cannot be empty or with less than 5 symbols.
  - Registration Number: Must be exactly 10 symbols and must contain only digits.
  - Cannot have two pieces of furniture with the same model code.
- The company `getCatalog()` method returns the information about the available furniture.

### Printing

#### Printing Rules

- All double type fields should be printed rounded to two decimal places.
- The `getCatalog()` method should return the furniture **ordered by price then by model** (if there are any added).
  - If the company has no furniture added, print "No furniture".
  - If the company has furniture, print the information about each furniture on a separate line (see example below).

```
Catalog of {Company name} {(Company number)}: {No furniture (if empty)}
  - {Information about the piece of furniture (if not empty)}
  - {Information about the piece of furniture (if not empty)}
  - {Information about the piece of furniture (if not empty)}
  ...
```

#### Tables

```none
Type: Table, Model Code: {{modelCode}}, Material: {{material}}, Price: {{price}}, Height: {{height}}, Length: {{length}}, Width: {{width}}, Area: {{area}}
```

#### Regular and Adjustable Chairs

```none
Type: Chair, Model Code: {{modelCode}}, Material: {{material}}, Price: {{price}}, Height: {{height}}, Legs: {{legsCount}}
```

#### Convertible Chair

```none
Type: Convertible Chair, Model Code: {{modelCode}}, Material: {{material}}, Price: {{price}}, Height: {{height}}, Legs: {{legsCount}}, State: {{isConverted ? Converted : Normal}}
```

> Hint: There are a lot of repetitions. Can you avoid code duplication? Think about `getAdditionalInfo()` method - how and where should you implement it?

### Commands

#### CreateTableCommand

You have to implement the `CreateTableCommand` class in the `commands` package. Study the already implemented commands to understand how to do it.

The command is used to create a table with a given model code, material, price, height, length and width. Duplicate models codes are not allowed. As a result the command returns a message in the form of "Table {{model code}} created".

The command's signature is the following:

**CreateTable {{model code}} {{material}} {{price}} {{height}} {{length}} {{width}}**.

#### CreateChairCommand

The command is used to create a chair with a given model code, material, price, height, length and width. Duplicate models codes are not allowed. Use the `chairType` argument to decide what type of table needs to be created. Most of this command is already implemented. You have to implement the logic of deciding what type of chair to create.

> *Note:* All commands should return appropriate success messages. In case of invalid operation or error, the appropriate error message is printed.

## Step by step guide

> *Note:* The code will not compile successfully at first. Do not panic and try to figure out what information can the errors give you.

> *Important:* You ***must not*** change any of the test. Just use them to guide you and check on your progress.

1. Build the interface hierarchy

   - Look at the `contracts` package and decide how the interfaces in there are connected.

1. Build the class hierarchy

   - Are there any common fields/behavior that can be combined in a base class?
   - Do you have repeating code in more than one class?

1. Implement `FurnitureRepositoryImpl`

   - Look at the `getFurniture()` method. It must return a `List` of `Product`s. How can we use all the lists we already have to create a list of `Furniture` (*hint: Polymorphism!*)?
   - Look at the `findElementByModelCode()` method. How can we use it to implement `findChairByModelCode()`, `findAdjustableChairByModelCode()`, `findConvertibleChairByModelCode()`, and `findFurnitureByModelCode()`?

1. Add printing

   - You need to override the `toString()` method in each class. Follow the format specified above.

1. Validate all properties according to the guidelines specified above

   - Take a look at what's in the `ValidationHelpers` class. Is there something that can be useful?
   - If the need arises, you can add more validation methods in `ValidationHelpers`

1. Implement the methods inside `CompanyImpl`

   - Remember what had to be done in order for a class that we created to be sortable?
   - Consider using the `Comparable` interface?

1. Implement the `CreateTableCommand`

1. Finish the `CreateChairCommand`

### Sample Input

```none
CreateCompany TelerikAcademyFurniture 1337855391
ShowCompanyCatalog TelerikAcademyFurniture
CreateTable EQ1923 wooden 123.4 0.50 0.45 0.65
CreateChair HG1124 leather 99.99 1.20 5 Regular
CreateChair LY8212 leather 111.56 0.80 4 Adjustable
CreateChair GG4201 plastic 80.00 1.00 3 Convertible
ShowCompanyCatalog TelerikAcademyFurniture
AddFurnitureToCompany TelerikAcademyFurniture EQ1923
AddFurnitureToCompany TelerikAcademyFurniture HG1124
AddFurnitureToCompany TelerikAcademyFurniture LY8212
AddFurnitureToCompany TelerikAcademyFurniture GG4201
ShowCompanyCatalog TelerikAcademyFurniture
RemoveFurnitureFromCompany TelerikAcademyFurniture HT9172
ShowCompanyCatalog TelerikAcademyFurniture
FindFurnitureFromCompany TelerikAcademyFurniture GG4201
ConvertChair GG4201
FindFurnitureFromCompany TelerikAcademyFurniture LY8212
RemoveFurnitureFromCompany TelerikAcademyFurniture HG1124
RemoveFurnitureFromCompany TelerikAcademyFurniture EQ1923
ShowCompanyCatalog TelerikAcademyFurniture
Exit
```

> *Hint:* Don't use the whole input as a test. Break it as simple as possible. Maybe one line at a time is a good starting point.

### Sample Output

```none
Company TelerikAcademyFurniture created.
Catalog of TelerikAcademyFurniture (1337855391): No furniture.
Table with model code EQ1923 created.
Chair with model code HG1124 created.
Chair with model code LY8212 created.
Chair with model code GG4201 created.
Catalog of TelerikAcademyFurniture (1337855391): No furniture.
EQ1923 added to TelerikAcademyFurniture.
HG1124 added to TelerikAcademyFurniture.
LY8212 added to TelerikAcademyFurniture.
GG4201 added to TelerikAcademyFurniture.
Catalog of TelerikAcademyFurniture (1337855391):
  - Type: Convertible Chair, Model Code: GG4201, Material: Plastic, Price: 80.00, Height: 1.00, Legs: 3, State: Normal
  - Type: Chair, Model Code: HG1124, Material: Leather, Price: 99.99, Height: 1.20, Legs: 5
  - Type: Adjustable Chair, Model Code: LY8212, Material: Leather, Price: 111.56, Height: 0.80, Legs: 4
  - Type: Table, Model Code: EQ1923, Material: Wooden, Price: 123.40, Height: 0.50, Length: 0.45, Width: 0.65, Area: 0.29
Furniture with model code HT9172 does not exist.
Catalog of TelerikAcademyFurniture (1337855391):
  - Type: Convertible Chair, Model Code: GG4201, Material: Plastic, Price: 80.00, Height: 1.00, Legs: 3, State: Normal
  - Type: Chair, Model Code: HG1124, Material: Leather, Price: 99.99, Height: 1.20, Legs: 5
  - Type: Adjustable Chair, Model Code: LY8212, Material: Leather, Price: 111.56, Height: 0.80, Legs: 4
  - Type: Table, Model Code: EQ1923, Material: Wooden, Price: 123.40, Height: 0.50, Length: 0.45, Width: 0.65, Area: 0.29
Type: Convertible Chair, Model Code: GG4201, Material: Plastic, Price: 80.00, Height: 1.00, Legs: 3, State: Normal
Chair GG4201's state switched from normal to converted.
Type: Adjustable Chair, Model Code: LY8212, Material: Leather, Price: 111.56, Height: 0.80, Legs: 4
HG1124 removed from TelerikAcademyFurniture.
EQ1923 removed from TelerikAcademyFurniture.
Catalog of TelerikAcademyFurniture (1337855391):
  - Type: Convertible Chair, Model Code: GG4201, Material: Plastic, Price: 80.00, Height: 0.10, Legs: 3, State: Converted
  - Type: Adjustable Chair, Model Code: LY8212, Material: Leather, Price: 111.56, Height: 0.80, Legs: 4
```

Current implemented commands the engine supports are:

- **CreateCompany (name) (registration number)** – adds a company with given name and registration number. Duplicate names are not allowed. As a result the command returns “Company (name) created”.
- **AddFurnitureToCompany (company name) (furniture model)** – searches for furniture and adds it to an existing company’s catalog. As a result the command returns “Furniture (furniture model) added to company (company name)”.
- **RemoveFurnitureFromCompany (company name) (furniture model)** – searches for furniture and removes it from an existing company’s catalog. As a result the command returns “Furniture (furniture model) removed from company (company name)”.
- **FindFurnitureFromCompany (company name) (furniture model)** – searches for furniture in an existing company’s catalog. If found the engine prints the furniture’s toString() method.
- **ShowCompanyCatalog (company name)** – searches for a company and invokes it’s catalog() method.

- **CreateChair (model) (material) (price) (height) (legs) (type)** – creates a chair by given model, material, price, height, legs and type. Type can be “Normal”, “Adjustable” and “Convertible”. Duplicate models are not allowed. As a result the command returns “Chair (model) created”.
- **SetChairHeight (model) (height)** – searches for a chair by model and sets its height, if the chair is adjustable. As a result the command returns “Chair (model) adjusted to height (height)”.
- **ConvertChair (model)** – searches for a chair by model and converts its state, if the chair is convertible. As a result the command returns “Chair (model) converted”.
In case of invalid operation or error, the engine returns appropriate text messages.
