package com.telerikacademy.oop.furniture.models;

import com.telerikacademy.oop.furniture.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.furniture.models.enums.MaterialType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static com.telerikacademy.oop.furniture.TestData.Chair.*;
import static com.telerikacademy.oop.furniture.TestUtilities.initializeStringWithSize;
import static com.telerikacademy.oop.furniture.models.FurnitureBase.MODEL_CODE_MIN_LENGTH;

public class FurnitureBase_Tests {
    @Test
    public void constructor_should_throwException_when_modelCodeLengthLessThanMinimum() {
        //Arrange, Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class,
                () -> new ChairImpl(initializeStringWithSize(MODEL_CODE_MIN_LENGTH - 1),
                        MaterialType.LEATHER,
                        Double.parseDouble(VALID_PRICE),
                        Double.parseDouble(VALID_HEIGHT),
                        Integer.parseInt(VALID_LEGS_COUNT)));
    }

    @Test
    public void constructor_should_throwException_when_heightIsLessThanZero() {
        //Arrange, Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class,
                () -> new ChairImpl(VALID_MODEL_CODE,
                        MaterialType.LEATHER,
                        Double.parseDouble(VALID_PRICE),
                        -1,
                        Integer.parseInt(VALID_LEGS_COUNT)));
    }

    @Test
    public void constructor_should_throwException_when_priceIsLessThanZero() {
        //Arrange, Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class,
                () -> new ChairImpl(initializeStringWithSize(MODEL_CODE_MIN_LENGTH - 1),
                        MaterialType.LEATHER,
                        -1,
                        Double.parseDouble(VALID_HEIGHT),
                        Integer.parseInt(VALID_LEGS_COUNT)));
    }

}
