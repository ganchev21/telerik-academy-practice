package com.telerikacademy.oop.furniture.models;

import com.telerikacademy.oop.furniture.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.furniture.models.enums.MaterialType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static com.telerikacademy.oop.furniture.TestData.Table.*;

public class TableImpl_Tests {

    @ParameterizedTest(name = "with length: {0}")
    @ValueSource(ints = {0, -1})
    public void constructor_should_throwException_when_lengthInvalid(int length) {
        // Arrange, Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class,
                () -> new TableImpl(VALID_MODEL_CODE,
                        MaterialType.LEATHER,
                        Double.parseDouble(VALID_PRICE),
                        Double.parseDouble(VALID_HEIGHT),
                        length,
                        Double.parseDouble(VALID_WIDTH)));
    }

    @ParameterizedTest(name = "with width: {0}")
    @ValueSource(ints = {0, -1})
    public void constructor_should_throwException_when_widthInvalid(int width) {
        // Arrange, Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class,
                () -> new TableImpl(VALID_MODEL_CODE,
                        MaterialType.LEATHER,
                        Double.parseDouble(VALID_PRICE),
                        Double.parseDouble(VALID_HEIGHT),
                        Double.parseDouble(VALID_LENGTH),
                        width));
    }

}
