package com.telerikacademy.oop.furniture.models;

import com.telerikacademy.oop.furniture.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.furniture.models.contracts.AdjustableChair;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static com.telerikacademy.oop.furniture.TestUtilities.createAdjustableChair;

public class AdjustableChair_Tests {

    @Test
    public void setHeight_should_adjustChairHeight() {
        // Arrange
        AdjustableChair adjChair = createAdjustableChair();

        // Act
        adjChair.setHeight(7);

        // Assert
        Assertions.assertEquals(7, adjChair.getHeight());
    }

    @Test
    public void setHeight_should_throwException_when_heightLessThanZero() {
        // Arrange
        AdjustableChair adjChair = createAdjustableChair();

        //Act
        Assertions.assertThrows(InvalidUserInputException.class, () -> adjChair.setHeight(-1));

    }

}
