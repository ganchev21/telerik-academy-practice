package com.telerikacademy.oop.furniture.models;

import com.telerikacademy.oop.furniture.models.contracts.ConvertibleChair;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static com.telerikacademy.oop.furniture.TestUtilities.createConvertibleChair;
import static com.telerikacademy.oop.furniture.models.ConvertibleChairImpl.CONVERTED_HEIGHT;

public class ConvertibleChairImpl_Tests {

    @Test
    public void convert_should_changeIsConvertedFromTrueToFalse() {
        // Arrange
        ConvertibleChair convertibleChair = createConvertibleChair();

        // Act
        convertibleChair.convert();

        // Assert
        // A ConvertibleChair is initialized with isConverted equal to false.
        Assertions.assertTrue(convertibleChair.getIsConverted());
    }

    @Test
    public void convert_should_changeIsConvertedFromFalseToTrue() {
        // Arrange
        ConvertibleChair convertibleChair = createConvertibleChair();
        convertibleChair.convert();

        // Act
        convertibleChair.convert();

        // Assert
        // A ConvertibleChair is initialized with isConverted equal to false.
        Assertions.assertFalse(convertibleChair.getIsConverted());
    }

    @Test
    public void convert_should_convertHeightCorrectly() {
        // Arrange
        ConvertibleChair convertibleChair = createConvertibleChair();

        // Act
        convertibleChair.convert();

        // Assert
        Assertions.assertEquals(CONVERTED_HEIGHT, convertibleChair.getHeight());
    }

    @Test
    public void convert_should_restoreOriginalHeight() {
        // Arrange
        ConvertibleChair convertibleChair = createConvertibleChair();
        double initialHeight = convertibleChair.getHeight();
        convertibleChair.convert();

        // Act
        convertibleChair.convert();

        // Assert
        Assertions.assertEquals(initialHeight, convertibleChair.getHeight());
    }

}
