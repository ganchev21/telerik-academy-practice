package com.telerikacademy.oop.furniture.models;

import com.telerikacademy.oop.furniture.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.furniture.models.enums.MaterialType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static com.telerikacademy.oop.furniture.TestData.Chair.*;

public class ChairImpl_Tests {

    @Test
    public void constructor_should_throwError_when_numberOfLegsIsLessThanZero() {
        //Arrange, Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class,
                () -> new ChairImpl(VALID_MODEL_CODE,
                        MaterialType.LEATHER,
                        Double.parseDouble(VALID_PRICE),
                        Double.parseDouble(VALID_HEIGHT),
                        -1));
    }

}
