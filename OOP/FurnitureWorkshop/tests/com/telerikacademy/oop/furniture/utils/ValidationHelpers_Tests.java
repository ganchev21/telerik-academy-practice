package com.telerikacademy.oop.furniture.utils;

import com.telerikacademy.oop.furniture.exceptions.InvalidUserInputException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static com.telerikacademy.oop.furniture.TestUtilities.initializeListWithSize;
import static com.telerikacademy.oop.furniture.TestUtilities.initializeStringWithSize;
import static com.telerikacademy.oop.furniture.utils.ValidationHelper.*;

public class ValidationHelpers_Tests {

    @Test
    void validateOnlyDigits_shouldNotThrowException_when_onlyDigits() {
        Assertions.assertDoesNotThrow(() -> validateOnlyDigits("123", ""));
    }

    @Test
    void validateOnlyDigits_shouldThrowException_when_notOnlyDigits() {
        Assertions.assertThrows(InvalidUserInputException.class, () -> validateOnlyDigits("123A", ""));
    }

    @Test
    void validateArgumentsCount_shouldNotThrowException_when_argumentsHaveCorrectCount() {
        Assertions.assertDoesNotThrow(() -> validateArgumentsCount(initializeListWithSize(2), 2));
    }

    @Test
    void validateArgumentsCount_shouldThrowException_when_argumentsDoesntHaveCorrectCount() {
        Assertions.assertThrows(InvalidUserInputException.class,
                () -> validateArgumentsCount(initializeListWithSize(2), 6));
    }

    @Test
    void validateGreaterThanZero_shouldNotThrowException_when_graterThanZero() {
        Assertions.assertDoesNotThrow(() -> validateGreaterThanZero(1));
    }

    @Test
    void validateGreaterThanZero_shouldThrowException_when_lessThanZero() {
        Assertions.assertThrows(InvalidUserInputException.class, () -> validateGreaterThanZero(-1));
    }

    @Test
    void validateStringMinLength_shouldNotThrowException_when_stringMatchesExpectedLength() {
        Assertions.assertDoesNotThrow(() -> validateStringMinLength(initializeStringWithSize(5), 2));
    }

    @Test
    void validateStringMinLength_shouldThrowException_when_stringDoesntMatchExpectedLength() {
        Assertions.assertThrows(InvalidUserInputException.class,
                () -> validateStringMinLength(initializeStringWithSize(2), 5));
    }

    @Test
    void validateStringExactLength_shouldNotThrowException_when_stringMatchesExpectedLength() {
        Assertions.assertDoesNotThrow(() -> validateStringMinLength(initializeStringWithSize(5), 5));
    }

    @Test
    void validateStringExactLength_shouldThrowException_when_stringDoesntMatchExpectedLength() {
        Assertions.assertThrows(InvalidUserInputException.class,
                () -> validateStringMinLength(initializeStringWithSize(2), 5));
    }
}
