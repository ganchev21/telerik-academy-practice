package com.telerikacademy.oop.furniture.commands;

import com.telerikacademy.oop.furniture.commands.contracts.Command;
import com.telerikacademy.oop.furniture.core.FurnitureRepositoryImpl;
import com.telerikacademy.oop.furniture.core.contracts.FurnitureRepository;
import com.telerikacademy.oop.furniture.exceptions.ElementNotFoundException;
import com.telerikacademy.oop.furniture.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.furniture.models.contracts.AdjustableChair;
import com.telerikacademy.oop.furniture.models.contracts.Chair;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.List;

import static com.telerikacademy.oop.furniture.TestData.Table.VALID_MODEL_CODE;
import static com.telerikacademy.oop.furniture.TestUtilities.*;
import static com.telerikacademy.oop.furniture.commands.SetChairHeightCommand.EXPECTED_NUMBER_OF_ARGUMENTS;

public class SetChairHeightCommand_Tests {

    private Command command;
    private FurnitureRepository furnitureRepository;

    @BeforeEach
    public void before() {
        furnitureRepository = new FurnitureRepositoryImpl();
        command = new SetChairHeightCommand(furnitureRepository);
    }

    @ParameterizedTest(name = "with arguments count: {0}")
    @ValueSource(ints = {EXPECTED_NUMBER_OF_ARGUMENTS - 1, EXPECTED_NUMBER_OF_ARGUMENTS + 1})
    public void execute_should_throwException_when_argumentsCountDifferentThanExpected(int argumentsCount) {
        // Arrange
        List<String> arguments = initializeListWithSize(argumentsCount);

        // Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_heightUnparsable() {
        // Arrange, Act
        List<String> arguments = List.of(VALID_MODEL_CODE, "invalid-height");

        // Arrange
        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(arguments));

    }

    @Test
    public void execute_should_throwException_when_providedModelCodeNotOfAdjustableChair() {
        // Arrange
        Chair chair = initializeChair(furnitureRepository);
        List<String> arguments = List.of(chair.getModelCode(), "1.11");

        // Act, Arrange
        Assertions.assertThrows(ElementNotFoundException.class, () -> command.execute(arguments));
    }


    @Test
    public void execute_should_setHeight_when_inputValid() {
        // Arrange
        AdjustableChair chair = initializeAdjustableChair(furnitureRepository);
        List<String> arguments = List.of(chair.getModelCode(), "1.11");

        // Act, Arrange
        Assertions.assertAll(
                () -> Assertions.assertDoesNotThrow(() -> command.execute(arguments)),
                () -> Assertions.assertEquals(1.11, chair.getHeight())
        );
    }

}
