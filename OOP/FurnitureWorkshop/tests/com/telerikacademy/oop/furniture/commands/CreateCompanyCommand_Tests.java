package com.telerikacademy.oop.furniture.commands;

import com.telerikacademy.oop.furniture.commands.contracts.Command;
import com.telerikacademy.oop.furniture.core.FurnitureRepositoryImpl;
import com.telerikacademy.oop.furniture.core.contracts.FurnitureRepository;
import com.telerikacademy.oop.furniture.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.furniture.models.contracts.Company;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.List;

import static com.telerikacademy.oop.furniture.TestData.Company.VALID_NAME;
import static com.telerikacademy.oop.furniture.TestData.Company.VALID_REGISTRATION;
import static com.telerikacademy.oop.furniture.TestUtilities.initializeCompany;
import static com.telerikacademy.oop.furniture.TestUtilities.initializeListWithSize;
import static com.telerikacademy.oop.furniture.commands.CreateCompanyCommand.EXPECTED_NUMBER_OF_ARGUMENTS;

public class CreateCompanyCommand_Tests {

    private Command command;
    private FurnitureRepository furnitureRepository;

    @BeforeEach
    public void before() {
        furnitureRepository = new FurnitureRepositoryImpl();
        command = new CreateCompanyCommand(furnitureRepository);
    }

    @ParameterizedTest(name = "with arguments count: {0}")
    @ValueSource(ints = {EXPECTED_NUMBER_OF_ARGUMENTS - 1, EXPECTED_NUMBER_OF_ARGUMENTS + 1})
    public void execute_should_throwException_when_argumentsCountDifferentThanExpected(int argumentsCount) {
        // Arrange
        List<String> arguments = initializeListWithSize(argumentsCount);

        // Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_companyExists() {
        // Arrange, Act
        Company company = initializeCompany(furnitureRepository);
        List<String> arguments = List.of(company.getName(), VALID_REGISTRATION);

        // Assert
        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_createCompany_when_inputIsValid() {
        // Arrange, Act
        command.execute(List.of(VALID_NAME, VALID_REGISTRATION));

        // Assert
        Assertions.assertEquals(1, furnitureRepository.getCompanies().size());
    }

}
