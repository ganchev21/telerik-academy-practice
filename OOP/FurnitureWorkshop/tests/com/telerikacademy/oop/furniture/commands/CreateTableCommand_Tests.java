package com.telerikacademy.oop.furniture.commands;

import com.telerikacademy.oop.furniture.commands.contracts.Command;
import com.telerikacademy.oop.furniture.core.FurnitureRepositoryImpl;
import com.telerikacademy.oop.furniture.core.contracts.FurnitureRepository;
import com.telerikacademy.oop.furniture.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.furniture.models.contracts.Furniture;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.List;

import static com.telerikacademy.oop.furniture.TestData.Table.*;
import static com.telerikacademy.oop.furniture.TestUtilities.initializeFurniture;
import static com.telerikacademy.oop.furniture.TestUtilities.initializeListWithSize;
import static com.telerikacademy.oop.furniture.commands.CreateTableCommand.EXPECTED_NUMBER_OF_ARGUMENTS;

public class CreateTableCommand_Tests {

    private Command command;
    private FurnitureRepository furnitureRepository;

    @BeforeEach
    public void before() {
        furnitureRepository = new FurnitureRepositoryImpl();
        command = new CreateTableCommand(furnitureRepository);
    }

    @ParameterizedTest(name = "with arguments count: {0}")
    @ValueSource(ints = {EXPECTED_NUMBER_OF_ARGUMENTS - 1, EXPECTED_NUMBER_OF_ARGUMENTS + 1})
    public void execute_should_throwException_when_argumentsCountDifferentThanExpected(int argumentsCount) {
        // Arrange
        List<String> arguments = initializeListWithSize(argumentsCount);

        // Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_materialUnparsable() {
        // Arrange, Act
        List<String> arguments = List.of(
                VALID_MODEL_CODE,
                "invalid-material",
                VALID_PRICE,
                VALID_HEIGHT,
                VALID_LENGTH,
                VALID_WIDTH);

        // Arrange
        Assertions.assertThrows(InvalidUserInputException.class,
                () -> command.execute(arguments));

    }

    @Test
    public void execute_should_throwException_when_priceUnparsable() {
        // Arrange, Act
        List<String> arguments = List.of(
                VALID_MODEL_CODE,
                VALID_MATERIAL,
                "invalid-price",
                VALID_HEIGHT,
                VALID_LENGTH,
                VALID_WIDTH);

        // Arrange
        Assertions.assertThrows(InvalidUserInputException.class,
                () -> command.execute(arguments));

    }

    @Test
    public void execute_should_throwException_when_heightUnparsable() {
        // Arrange, Act
        List<String> arguments = List.of(
                VALID_MODEL_CODE,
                VALID_MATERIAL,
                VALID_PRICE,
                "invalid-height",
                VALID_LENGTH,
                VALID_WIDTH);

        // Arrange
        Assertions.assertThrows(InvalidUserInputException.class,
                () -> command.execute(arguments));

    }

    @Test
    public void execute_should_throwException_when_lengthUnparsable() {
        // Arrange, Act
        List<String> arguments = List.of(
                VALID_MODEL_CODE,
                VALID_MATERIAL,
                VALID_PRICE,
                VALID_HEIGHT,
                "invalid-length",
                VALID_WIDTH);

        // Arrange
        Assertions.assertThrows(InvalidUserInputException.class,
                () -> command.execute(arguments));

    }

    @Test
    public void execute_should_throwException_when_widthUnparsable() {
        // Arrange, Act
        List<String> arguments = List.of(
                VALID_MODEL_CODE,
                VALID_MATERIAL,
                VALID_PRICE,
                VALID_HEIGHT,
                VALID_LENGTH,
                "invalid-width");

        // Arrange
        Assertions.assertThrows(InvalidUserInputException.class,
                () -> command.execute(arguments));

    }

    @Test
    public void execute_should_throwException_when_modelCodeExists() {
        // Arrange, Act
        Furniture furniture = initializeFurniture(furnitureRepository);

        List<String> arguments = List.of(
                furniture.getModelCode(),
                VALID_MATERIAL,
                VALID_PRICE,
                VALID_HEIGHT,
                VALID_LENGTH,
                VALID_WIDTH);

        // Arrange
        Assertions.assertThrows(InvalidUserInputException.class,
                () -> command.execute(arguments));

    }

    @Test
    public void execute_should_throwException_when_inputIsValid() {
        // Arrange, Act
        List<String> arguments = List.of(
                VALID_MODEL_CODE,
                VALID_MATERIAL,
                VALID_PRICE,
                VALID_HEIGHT,
                VALID_LENGTH,
                VALID_WIDTH);

        // Arrange
        Assertions.assertAll(
                () -> Assertions.assertDoesNotThrow(() -> command.execute(arguments)),
                () -> Assertions.assertEquals(1, furnitureRepository.getFurniture().size())
        );

    }

}
