package com.telerikacademy.oop.cosmetics.commands;

import com.telerikacademy.oop.cosmetics.Exceptions.InvalidInputException;
import com.telerikacademy.oop.cosmetics.Exceptions.NameAlreadyExistException;
import com.telerikacademy.oop.cosmetics.commands.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import com.telerikacademy.oop.cosmetics.models.GenderType;
import com.telerikacademy.oop.cosmetics.Exceptions.InvalidPriceException;

import java.util.List;

public class CreateProductCommand implements Command {

    private static final String PRODUCT_CREATED = "Product with name %s was created!";

    private final ProductRepository productRepository;

    public CreateProductCommand(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        //TODO Validate parameters count
        if (parameters.size() != 4) {
            throw new IllegalArgumentException("CreateProduct command expects 4 parameters.");
        }

        String name = parameters.get(0);
        String brand = parameters.get(1);
        //TODO Validate price format
        double price = 0;
        try {
            price = Double.parseDouble(parameters.get(2));
        } catch (NumberFormatException e) {
            throw new InvalidPriceException("Third parameter should be price (real number).");
        }
        //TODO Validate gender format
        GenderType gender = null;
        try {
            gender = GenderType.valueOf(parameters.get(3).toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new InvalidInputException("Forth parameter should be one of Men, Women or Unisex.");
        }

        return createProduct(name, brand, price, gender);
    }

    private String createProduct(String name, String brand, double price, GenderType gender) {
        //TODO Ensure product name is unique
        if (productRepository.productExist(name)) {
            throw new NameAlreadyExistException(String.format("Product %s already exist.", name));
        }

        productRepository.createProduct(name, brand, price, gender);

        return String.format(PRODUCT_CREATED, name);
    }

}
