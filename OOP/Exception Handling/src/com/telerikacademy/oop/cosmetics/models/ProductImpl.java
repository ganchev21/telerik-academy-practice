package com.telerikacademy.oop.cosmetics.models;

import com.telerikacademy.oop.cosmetics.Exceptions.InvalidInputException;
import com.telerikacademy.oop.cosmetics.Exceptions.InvalidPriceException;
import com.telerikacademy.oop.cosmetics.models.contracts.Product;

public class ProductImpl implements Product {

    private static int PRODUCT_NAME_MIN_LENGTH = 3;
    private static int PRODUCT_NAME_MAX_LENGTH = 10;
    private static int PRODUCT_BRAND_MIN_LENGTH = 2;
    private static int PRODUCT_BRAND_MAX_LENGTH = 10;

    private String name;
    private String brand;
    private double price;
    private final GenderType gender;

    public ProductImpl(String name, String brand, double price, GenderType gender) {
        setName(name);
        setBrand(brand);
        setPrice(price);
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    private void setName(String name) {
        if (name.length() >= PRODUCT_NAME_MIN_LENGTH && name.length() <= PRODUCT_NAME_MAX_LENGTH) {
            this.name = name;
        } else {
            throw new InvalidInputException(String.format("Name must be between %d and %d characters!",
                    PRODUCT_NAME_MIN_LENGTH, PRODUCT_NAME_MAX_LENGTH));
        }
    }

    public String getBrand() {
        return brand;
    }

    private void setBrand(String brand) {
        if (brand.length() >= PRODUCT_BRAND_MIN_LENGTH && brand.length() <= PRODUCT_BRAND_MAX_LENGTH) {
            this.brand = brand;
        } else {
            throw new InvalidInputException(String.format("Product brand should be between %d and %d symbols.",
                    PRODUCT_BRAND_MIN_LENGTH, PRODUCT_BRAND_MAX_LENGTH));
        }
    }

    public double getPrice() {
        return price;
    }

    private void setPrice(double price) {
        if (price > 0) {
            this.price = price;
        } else {
            throw new InvalidPriceException("Price can't be negative.");
        }
    }

    public GenderType getGender() {
        return gender;
    }

    @Override
    public String print() {
        return String.format(
                "#%s %s%n" +
                " #Price: $%.2f%n" +
                " #Gender: %s%n",
                name,
                brand,
                price,
                gender);
    }

}
