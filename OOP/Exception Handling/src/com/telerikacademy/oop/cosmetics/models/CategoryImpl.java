package com.telerikacademy.oop.cosmetics.models;

import com.telerikacademy.oop.cosmetics.Exceptions.InvalidInputException;
import com.telerikacademy.oop.cosmetics.models.contracts.Category;
import com.telerikacademy.oop.cosmetics.models.contracts.Product;

import java.util.ArrayList;
import java.util.List;

public class CategoryImpl implements Category {

    private static int CATEGORY_MIN_LENGTH = 3;
    private static int CATEGORY_MAX_LENGTH = 10;
    private String name;
    private final List<Product> products;

    public CategoryImpl(String name) {
        setName(name);
        products = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    private void setName(String name) {
        if (name.length() >= CATEGORY_MIN_LENGTH && name.length() <= CATEGORY_MAX_LENGTH) {
            this.name = name;
        } else {
            throw new InvalidInputException(String.format("Category name should be between %d and %d symbols.",
                    CATEGORY_MIN_LENGTH, CATEGORY_MAX_LENGTH));
        }
    }

    public List<Product> getProducts() {
        return new ArrayList<>(products);
    }

    public void addProduct(Product product) {
        products.add(product);
    }

    public void removeProduct(Product product) {
        products.remove(product);
    }

    public String print() {
        if (products.size() == 0) {
            return String.format(
                    "#Category: %s%n" +
                    " #No product in this category",
                    name);
        }

        StringBuilder strBuilder = new StringBuilder();
        strBuilder.append(String.format("#Category: %s%n", name));

        for (Product product : products) {
            strBuilder.append(product.print());
            strBuilder.append(String.format(" ===%n"));
        }

        return strBuilder.toString();
    }

}
