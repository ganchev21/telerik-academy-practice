package com.telerikacademy.oop.cosmetics.Exceptions;

public class NameAlreadyExistException extends RuntimeException{
    public NameAlreadyExistException(String message) {
        super(message);
    }
}
