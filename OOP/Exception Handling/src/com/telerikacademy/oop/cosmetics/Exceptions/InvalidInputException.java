package com.telerikacademy.oop.cosmetics.Exceptions;

public class InvalidInputException extends RuntimeException{
    public InvalidInputException(String text) {
        super(text);
    }
}
