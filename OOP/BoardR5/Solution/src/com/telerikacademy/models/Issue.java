package com.telerikacademy.models;

import com.telerikacademy.enums.Status;

import java.time.LocalDate;

public class Issue extends BoardItem {

    private final String description;

    public Issue(String title, String description, LocalDate dueDate) {
        super(title, dueDate, Status.OPEN);

        if (description.isEmpty()) {
            this.description = "No description";
        } else {
            this.description = description;
        }
    }

    public String getDescription() {
        return description;
    }

    @Override
    public void revertStatus() {
        if (getStatus() != Status.OPEN) {
            this.status = Status.OPEN;
            this.logEvent(String.format("Issue status set to %s", getStatus()));
        } else {
            this.logEvent(String.format("Issue status already %s", getStatus()));
        }
    }

    @Override
    public void advanceStatus() {
        if (getStatus() != Status.VERIFIED) {
            this.status = Status.VERIFIED;
            this.logEvent(String.format("Issue status set to %s", getStatus()));
        } else {
            this.logEvent(String.format("Issue status already %s", getStatus()));
        }
    }
}
