package com.telerikacademy.oop.dealership.models;

import com.telerikacademy.oop.dealership.utils.models.MotorcycleImpl;
import com.telerikacademy.oop.dealership.utils.models.contracts.Motorcycle;
import com.telerikacademy.oop.dealership.utils.models.contracts.Vehicle;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static com.telerikacademy.oop.dealership.utils.VehicleBaseConstants.*;
import static com.telerikacademy.oop.dealership.utils.TestUtilities.getString;
import static org.junit.jupiter.api.Assertions.*;

public class MotorcycleImplTests {

    public static final int CATEGORY_LEN_MIN = 3;
    public static final String VALID_CATEGORY = getString(CATEGORY_LEN_MIN + 1);
    public static final String INVALID_CATEGORY = getString(CATEGORY_LEN_MIN - 1);

    @Test
    public void motorcycleImpl_Should_ImplementMotorcycleInterface() {
        // Arrange, Act
        MotorcycleImpl motorcycle = initializeTestMotorcycle();
        // Assert
        Assertions.assertTrue(motorcycle instanceof Motorcycle);
    }

    @Test
    public void motorcycleImpl_Should_ImplementVehicleInterface() {
        // Arrange, Act
        MotorcycleImpl motorcycle = initializeTestMotorcycle();
        // Assert
        Assertions.assertTrue(motorcycle instanceof Vehicle);
    }

    @Test
    public void constructor_Should_ThrowException_When_MakeNameLengthOutOfBounds() {
        // Arrange, Act, Assert
        assertThrows(IllegalArgumentException.class, () ->
                new MotorcycleImpl(
                        INVALID_MAKE,
                        VALID_MODEL,
                        VALID_PRICE,
                        VALID_CATEGORY));
    }

    @Test
    public void constructor_Should_ThrowException_When_ModelNameLengthOutOfBounds() {
        // Arrange, Act, Assert
        assertThrows(IllegalArgumentException.class, () ->
                new MotorcycleImpl(
                        VALID_MAKE,
                        INVALID_MODEL,
                        VALID_PRICE,
                        VALID_CATEGORY));
    }

    @Test
    public void constructor_Should_ThrowException_When_PriceIsInvalid() {
        // Arrange, Act, Assert
        assertThrows(IllegalArgumentException.class, () ->
                new MotorcycleImpl(
                        VALID_MAKE,
                        VALID_MODEL,
                        -1,
                        VALID_CATEGORY));
    }

    @Test
    public void constructor_Should_ThrowException_When_CategoryLengthOutOfBounds() {
        // Arrange, Act, Assert
        assertThrows(IllegalArgumentException.class, () ->
                new MotorcycleImpl(
                        VALID_MAKE,
                        VALID_MODEL,
                        VALID_PRICE,
                        INVALID_CATEGORY));
    }

    @Test
    public void constructor_Should_CreateNewMotorcycle_When_ParametersAreCorrect() {
        // Arrange, Act
        MotorcycleImpl motorcycle = initializeTestMotorcycle();

        // Assert
        assertAll(
                () -> assertEquals(VALID_MAKE, motorcycle.getMake()),
                () -> assertEquals(VALID_MODEL, motorcycle.getModel()),
                () -> assertEquals(VALID_PRICE, motorcycle.getPrice()),
                () -> assertEquals(VALID_CATEGORY, motorcycle.getCategory())
        );
    }

    public static MotorcycleImpl initializeTestMotorcycle() {
        return new MotorcycleImpl(
                VALID_MAKE,
                VALID_MODEL,
                VALID_PRICE,
                VALID_CATEGORY);
    }
}
