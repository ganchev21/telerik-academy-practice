package com.telerikacademy.oop.dealership.models;

import com.telerikacademy.oop.dealership.utils.models.CarImpl;
import com.telerikacademy.oop.dealership.utils.models.CommentImpl;
import com.telerikacademy.oop.dealership.utils.models.contracts.Car;
import com.telerikacademy.oop.dealership.utils.models.contracts.Vehicle;
import org.junit.jupiter.api.Test;

import static com.telerikacademy.oop.dealership.models.CommentImplTests.*;
import static com.telerikacademy.oop.dealership.utils.VehicleBaseConstants.*;
import static org.junit.jupiter.api.Assertions.*;

public class CarImplTests {
    public static final int VALID_SEATS = 4;

    @Test
    public void carImpl_Should_ImplementCarInterface() {
        // Arrange, Act
        CarImpl car = initializeTestCar();
        // Assert
        assertTrue(car instanceof Car);
    }

    @Test
    public void carImpl_Should_ImplementVehicleInterface() {
        // Arrange, Act
        CarImpl car = initializeTestCar();
        // Assert
        assertTrue(car instanceof Vehicle);
    }

    @Test
    public void constructor_Should_ThrowException_When_MakeNameLengthOutOfBounds() {
        // Arrange, Act, Assert
        assertThrows(IllegalArgumentException.class, () ->
                new CarImpl(
                        INVALID_MAKE,
                        VALID_MODEL,
                        VALID_PRICE,
                        VALID_SEATS));
    }

    @Test
    public void constructor_Should_ThrowException_When_ModelNameLengthOutOfBounds() {
        // Arrange, Act, Assert
        assertThrows(IllegalArgumentException.class, () ->
                new CarImpl(
                        VALID_MAKE,
                        INVALID_MODEL,
                        VALID_PRICE,
                        VALID_SEATS));
    }

    @Test
    public void constructor_Should_ThrowException_When_PriceIsInvalid() {
        // Arrange, Act, Assert
        assertThrows(IllegalArgumentException.class, () ->
                new CarImpl(
                        VALID_MAKE,
                        VALID_MODEL,
                        -1,
                        VALID_SEATS));
    }

    @Test
    public void constructor_Should_ThrowException_When_SeatsValueIsInvalidOrNegative() {
        // Arrange, Act, Assert
        assertThrows(IllegalArgumentException.class, () ->
                new CarImpl(
                        VALID_MAKE,
                        VALID_MODEL,
                        VALID_PRICE,
                        -1));
    }

    @Test
    public void constructor_Should_CreateNewCar_When_ParametersAreCorrect() {
        // Arrange, Act
        CarImpl car = initializeTestCar();

        // Assert
        assertAll(
                () -> assertEquals(VALID_MAKE, car.getMake()),
                () -> assertEquals(VALID_MODEL, car.getModel()),
                () -> assertEquals(VALID_PRICE, car.getPrice()),
                () -> assertEquals(VALID_SEATS, car.getSeats())
        );
    }

    @Test
    public void getComments_Should_ReturnCopyOfTheCollection() {
        // Arrange
        CarImpl car = initializeTestCar();

        // Act
        car.getComments().add(initializeTestComment());

        // Assert
        assertEquals(0, car.getComments().size());
    }

    @Test
    public void addComment_Should_AddCommentToTheCollection() {
        // Arrange
        CarImpl car = initializeTestCar();

        // Act
        car.addComment(initializeTestComment());

        // Assert
        assertEquals(1, car.getComments().size());
        assertEquals(VALID_CONTENT, car.getComments().get(0).getContent());
    }

    @Test
    public void removeComment_Should_RemoveCommentFromTheCollection() {
        // Arrange
        CarImpl car = initializeTestCar();
        CommentImpl comment = initializeTestComment();
        car.addComment(comment);

        // Act
        car.removeComment(comment);

        // Assert
        assertEquals(0, car.getComments().size());
    }

    public static CarImpl initializeTestCar() {
        return new CarImpl(
                VALID_MAKE,
                VALID_MODEL,
                VALID_PRICE,
                VALID_SEATS);
    }
}
