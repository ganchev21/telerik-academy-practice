package com.telerikacademy.oop.dealership.commands;

import com.telerikacademy.oop.dealership.core.VehicleDealershipRepositoryImpl;
import com.telerikacademy.oop.dealership.core.contracts.VehicleDealershipRepository;
import com.telerikacademy.oop.dealership.utils.models.contracts.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static com.telerikacademy.oop.dealership.models.UserImplTests.initializeTestUser;

public class LogoutTests {
    private VehicleDealershipRepository repository;
    private LogoutCommand logoutCommand;

    @BeforeEach
    public void before() {
        repository = new VehicleDealershipRepositoryImpl();
        logoutCommand = new LogoutCommand(repository);
    }

    @Test
    public void should_Throw_When_UserNotLoggedIn() {

        // Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> logoutCommand.execute(new ArrayList<>()));
    }

    @Test
    public void should_LogoutUser() {
        // Arrange
        User userToLogIn = initializeTestUser();
        repository.login(userToLogIn);

        // Act
        logoutCommand.execute(new ArrayList<>());

        // Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> repository.getLoggedInUser());
    }
}
