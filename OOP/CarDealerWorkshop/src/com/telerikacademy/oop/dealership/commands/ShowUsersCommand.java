package com.telerikacademy.oop.dealership.commands;

import com.telerikacademy.oop.dealership.core.contracts.VehicleDealershipRepository;
import com.telerikacademy.oop.dealership.utils.models.contracts.User;

import java.util.List;

public class ShowUsersCommand extends BaseCommand{


    public ShowUsersCommand(VehicleDealershipRepository vehicleDealershipRepository) {
        super(vehicleDealershipRepository);
    }

    @Override
    protected String executeCommand(List<String> parameters) {
        if (!getVehicleDealershipRepository().getLoggedInUser().isAdmin()) {
            throw new IllegalArgumentException("You are not an admin!");
        }
       return showUsersCommand();
    }

    private String showUsersCommand () {
        StringBuilder sb = new StringBuilder();
        int counter = 0;
        sb.append("--USERS--").append(System.lineSeparator());
        for (User users : getVehicleDealershipRepository().getUsers()) {
            counter++;
            sb.append(String.format("%d. ", counter)).append(users).append(System.lineSeparator());
        }
        return sb.toString().trim();
    }
    @Override
    protected boolean requiresLogin() {
        return true;
    }
}
