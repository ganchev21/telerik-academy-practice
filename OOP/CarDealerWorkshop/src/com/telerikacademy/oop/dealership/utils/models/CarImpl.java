package com.telerikacademy.oop.dealership.utils.models;

import com.telerikacademy.oop.dealership.utils.ValidationHelpers;
import com.telerikacademy.oop.dealership.utils.models.contracts.Car;
import com.telerikacademy.oop.dealership.utils.models.enums.VehicleType;

import static java.lang.String.format;

public class CarImpl extends VehicleBase implements Car {

    public static final int CAR_SEATS_MIN = 1;
    public static final int CAR_SEATS_MAX = 10;
    private static final String CAR_SEATS_ERR = format(
            "Seats must be between %d and %d!",
            CAR_SEATS_MIN,
            CAR_SEATS_MAX);
    private int seats;
    private final int wheels;

    public CarImpl(String make, String model, double price, int seats) {
        super(make, model, price, VehicleType.CAR);
        setSeats(seats);
        this.wheels = 4;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        ValidationHelpers.validateIntRange(seats, CAR_SEATS_MIN, CAR_SEATS_MAX, CAR_SEATS_ERR);
        this.seats = seats;
    }

    @Override
    public int getWheels() {
        return this.wheels;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        sb.append(String.format("Seats: %d",getSeats())).append(System.lineSeparator());
        if (comments.isEmpty()) {
            sb.append("--NO COMMENTS--").append(System.lineSeparator());
        } else {
            sb.append("--COMMENTS--").append(System.lineSeparator());
            sb.append("----------").append(System.lineSeparator());
            for (int i = 0; i < comments.size(); i++) {
                sb.append(String.format("%s", comments.get(i).getContent())).append(System.lineSeparator());
                sb.append(String.format("User: %s", comments.get(i).getAuthor())).append(System.lineSeparator());
                sb.append("----------").append(System.lineSeparator());
                if (i < comments.size() - 1) {
                    sb.append("----------").append(System.lineSeparator());
                }
            }
            sb.append("--COMMENTS--").append(System.lineSeparator());
        }
        return sb.toString().trim();
    }
}