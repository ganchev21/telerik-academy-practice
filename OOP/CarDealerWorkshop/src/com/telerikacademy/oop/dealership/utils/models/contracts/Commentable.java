package com.telerikacademy.oop.dealership.utils.models.contracts;

import java.util.List;

public interface Commentable {

    List<Comment> getComments();

}
