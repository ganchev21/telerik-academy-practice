package com.telerikacademy.oop.dealership.utils.models;

import com.telerikacademy.oop.dealership.utils.ValidationHelpers;
import com.telerikacademy.oop.dealership.utils.models.contracts.Comment;
import com.telerikacademy.oop.dealership.utils.models.contracts.User;
import com.telerikacademy.oop.dealership.utils.models.contracts.Vehicle;
import com.telerikacademy.oop.dealership.utils.models.enums.UserRole;

import java.util.ArrayList;
import java.util.List;


import static java.lang.String.format;

public class UserImpl implements User {

    public static final int USERNAME_LEN_MIN = 2;
    public static final int USERNAME_LEN_MAX = 20;
    private static final String USERNAME_REGEX_PATTERN = "^[A-Za-z0-9]+$";
    private static final String USERNAME_PATTERN_ERR = "Username contains invalid symbols!";
    private static final String USERNAME_LEN_ERR = format(
            "Username must be between %d and %d characters long!",
            USERNAME_LEN_MIN,
            USERNAME_LEN_MAX);

    public static final int PASSWORD_LEN_MIN = 5;
    public static final int PASSWORD_LEN_MAX = 30;
    private static final String PASSWORD_REGEX_PATTERN = "^[A-Za-z0-9@*_-]+$";
    private static final String PASSWORD_PATTERN_ERR = "Password contains invalid symbols!";
    private static final String PASSWORD_LEN_ERR = format(
            "Password must be between %s and %s characters long!",
            PASSWORD_LEN_MIN,
            PASSWORD_LEN_MAX);

    public static final int LASTNAME_LEN_MIN = 2;
    public static final int LASTNAME_LEN_MAX = 20;
    private static final String LASTNAME_LEN_ERR = format(
            "Lastname must be between %s and %s characters long!",
            LASTNAME_LEN_MIN,
            LASTNAME_LEN_MAX);

    public static final int FIRSTNAME_LEN_MIN = 2;
    public static final int FIRSTNAME_LEN_MAX = 20;
    private static final String FIRSTNAME_LEN_ERR = format(
            "Firstname must be between %s and %s characters long!",
            FIRSTNAME_LEN_MIN,
            FIRSTNAME_LEN_MAX);

    private final static String NOT_AN_VIP_USER_VEHICLES_ADD = "You are not VIP and cannot add more than %d vehicles!";
    private final static String ADMIN_CANNOT_ADD_VEHICLES = "You are an admin and therefore cannot add vehicles!";
    private static final String YOU_ARE_NOT_THE_AUTHOR = "You are not the author of the comment you are trying to remove!";
    private final static String USER_TO_STRING = "Username: %s, FullName: %s %s, Role: %s";
    private final static String NO_VEHICLES_HEADER = "--NO VEHICLES--";
    private final static String USER_HEADER = "--USER %s--";
    public static final int NORMAL_ROLE_VEHICLE_LIMIT = 5;

    private String username;
    private String firstname;
    private String lastname;
    private String password;
    private final UserRole user;
    private final List<Vehicle> vehicles;

    public UserImpl(String username, String firstname, String lastname, String password, UserRole user) {
        setUsername(username);
        setFirstname(firstname);
        setLastname(lastname);
        setPassword(password);
        this.user = user;
        vehicles = new ArrayList<>();
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        ValidationHelpers.validatePattern(username, USERNAME_REGEX_PATTERN, USERNAME_PATTERN_ERR);
        ValidationHelpers.validateIntRange(username.length(), USERNAME_LEN_MIN, USERNAME_LEN_MAX, USERNAME_LEN_ERR);
        this.username = username;
    }

    @Override
    public String getFirstName() {
        return this.firstname;
    }

    public void setFirstname(String firstname) {
        ValidationHelpers.validateIntRange(firstname.length(), FIRSTNAME_LEN_MIN, FIRSTNAME_LEN_MAX, FIRSTNAME_LEN_ERR);
        this.firstname = firstname;
    }

    @Override
    public String getLastName() {
        return this.lastname;
    }

    public void setLastname(String lastname) {
        ValidationHelpers.validateIntRange(lastname.length(), LASTNAME_LEN_MIN, LASTNAME_LEN_MAX, LASTNAME_LEN_ERR);
        this.lastname = lastname;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        ValidationHelpers.validatePattern(password, PASSWORD_REGEX_PATTERN, PASSWORD_PATTERN_ERR);
        ValidationHelpers.validateIntRange(password.length(), PASSWORD_LEN_MIN, PASSWORD_LEN_MAX, PASSWORD_LEN_ERR);
        this.password = password;
    }

    @Override
    public UserRole getRole() {
        return this.user;
    }

    @Override
    public List<Vehicle> getVehicles() {
        return new ArrayList<>(vehicles);
    }

    @Override
    public void addVehicle(Vehicle vehicle) {
        if (user.equals(UserRole.ADMIN)) {
            throw new IllegalArgumentException(ADMIN_CANNOT_ADD_VEHICLES);
        } else if (user.equals(UserRole.NORMAL)) {
            if (vehicles.size() == NORMAL_ROLE_VEHICLE_LIMIT) {
                throw new IllegalArgumentException(
                        String.format(NOT_AN_VIP_USER_VEHICLES_ADD, NORMAL_ROLE_VEHICLE_LIMIT));
            } else {
                vehicles.add(vehicle);
            }
        } else if (user.equals(UserRole.VIP)) {
            vehicles.add(vehicle);
        }
    }

    @Override
    public void removeVehicle(Vehicle vehicle) {
        vehicles.remove(vehicle);
    }

    @Override
    public void addComment(Comment commentToAdd, Vehicle vehicleToAddComment) {
        vehicleToAddComment.addComment(commentToAdd);

    }

    @Override
    public void removeComment(Comment commentToRemove, Vehicle vehicleToRemoveComment) {
        if (commentToRemove.getAuthor().equals(username)) {
            vehicleToRemoveComment.removeComment(commentToRemove);
        } else {
            throw new IllegalArgumentException(YOU_ARE_NOT_THE_AUTHOR);
        }
    }
    @Override
    public String printVehicles() {
        int counter = 0;
        StringBuilder sb = new StringBuilder();
        sb.append(String.format(USER_HEADER, getUsername())).append(System.lineSeparator());
        if (vehicles.isEmpty()) {
            sb.append(NO_VEHICLES_HEADER).append(System.lineSeparator());
        } else {
            for (Vehicle vehicle : vehicles) {
                counter++;
                sb.append(String.format("%d. %s:", counter, vehicle.getType())).append(System.lineSeparator());
                sb.append(vehicle).append(System.lineSeparator());
            }
        }
        return sb.toString().trim();
    }

    @Override
    public boolean isAdmin() {
        return user.equals(UserRole.ADMIN);
    }
    @Override
    public String toString() {
        return String.format(USER_TO_STRING, getUsername(), getFirstName(), getLastName(), getRole());
    }
}

