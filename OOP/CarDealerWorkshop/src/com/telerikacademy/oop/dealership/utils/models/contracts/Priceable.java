package com.telerikacademy.oop.dealership.utils.models.contracts;

public interface Priceable {

    double getPrice();

}
