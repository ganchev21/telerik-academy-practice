package com.telerikacademy.oop.dealership.utils.models.contracts;

public interface Comment {

    String getContent();

    String getAuthor();

}
