package com.telerikacademy.oop.dealership.utils.models.contracts;

import com.telerikacademy.oop.dealership.utils.models.enums.VehicleType;

public interface Vehicle extends Commentable, Priceable {

    int getWheels();
    double getPrice();

    VehicleType getType();

    String getMake();

    String getModel();

    void addComment(Comment comment);

    void removeComment(Comment comment);

}
