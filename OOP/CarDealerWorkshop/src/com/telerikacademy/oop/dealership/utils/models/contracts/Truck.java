package com.telerikacademy.oop.dealership.utils.models.contracts;

public interface Truck extends Vehicle {

    int getWeightCapacity();

}
