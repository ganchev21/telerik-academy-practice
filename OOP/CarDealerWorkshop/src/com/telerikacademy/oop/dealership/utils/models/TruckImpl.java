package com.telerikacademy.oop.dealership.utils.models;

import com.telerikacademy.oop.dealership.utils.models.contracts.Truck;
import com.telerikacademy.oop.dealership.utils.models.enums.VehicleType;
import com.telerikacademy.oop.dealership.utils.ValidationHelpers;

import static java.lang.String.format;

public class TruckImpl extends VehicleBase implements Truck {

    public static final int WEIGHT_CAP_MIN = 1;
    public static final int WEIGHT_CAP_MAX = 100;
    private static final String WEIGHT_CAP_ERR = format(
            "Weight capacity must be between %d and %d!",
            WEIGHT_CAP_MIN,
            WEIGHT_CAP_MAX);

    private int weightCapacity;
    private final int wheels;

    public TruckImpl(String make, String model, double price, int weightCapacity) {
        super(make, model, price, VehicleType.TRUCK);
        setWeightCapacity(weightCapacity);
        this.wheels = 8;
    }

    @Override
    public int getWeightCapacity() {
        return this.weightCapacity;
    }

    public void setWeightCapacity(int weightCapacity) {
        ValidationHelpers.validateIntRange(weightCapacity, WEIGHT_CAP_MIN, WEIGHT_CAP_MAX, WEIGHT_CAP_ERR);
        this.weightCapacity = weightCapacity;
    }

    @Override
    public int getWheels() {
        return this.wheels;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        sb.append(String.format("Weight Capacity: %st",getWeightCapacity())).append(System.lineSeparator());
        if (comments.isEmpty()) {
            sb.append("--NO COMMENTS--").append(System.lineSeparator());
        } else {
            sb.append("--COMMENTS--").append(System.lineSeparator());
            sb.append("----------").append(System.lineSeparator());
            for (int i = 0; i < comments.size(); i++) {
                sb.append(String.format("%s", comments.get(i).getContent())).append(System.lineSeparator());
                sb.append(String.format("User: %s", comments.get(i).getAuthor())).append(System.lineSeparator());
                sb.append("----------").append(System.lineSeparator());
                if (i < comments.size() - 1) {
                    sb.append("----------").append(System.lineSeparator());
                }
            }
            sb.append("--COMMENTS--").append(System.lineSeparator());
        }
        return sb.toString().trim();
    }
}
