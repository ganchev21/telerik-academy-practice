package com.telerikacademy.oop.dealership.utils.models;

import com.telerikacademy.oop.dealership.utils.models.contracts.Comment;
import com.telerikacademy.oop.dealership.utils.models.contracts.Priceable;
import com.telerikacademy.oop.dealership.utils.models.contracts.Vehicle;
import com.telerikacademy.oop.dealership.utils.models.enums.VehicleType;
import com.telerikacademy.oop.dealership.utils.ValidationHelpers;

import java.util.ArrayList;
import java.util.List;

import static java.lang.String.format;
public abstract class VehicleBase implements Vehicle{
    public static final int MAKE_NAME_LEN_MIN = 2;
    public static final int MAKE_NAME_LEN_MAX = 15;
    private static final String MAKE_NAME_LEN_ERR = format(
            "Make must be between %s and %s characters long!",
            MAKE_NAME_LEN_MIN,
            MAKE_NAME_LEN_MAX);
    public static final int MODEL_NAME_LEN_MIN = 1;
    public static final int MODEL_NAME_LEN_MAX = 15;
    private static final String MODEL_NAME_LEN_ERR = format(
            "Model must be between %s and %s characters long!",
            MODEL_NAME_LEN_MIN,
            MODEL_NAME_LEN_MAX);
    public static final double PRICE_VAL_MIN = 0;
    public static final double PRICE_VAL_MAX = 1000000;
    private static final String PRICE_VAL_ERR = format(
            "Price must be between %.1f and %.1f!",
            PRICE_VAL_MIN,
            PRICE_VAL_MAX);

    private String make;
    private String model;
    private double price;
    List<Comment> comments;
    VehicleType type;

    public VehicleBase(String make, String model, double price, VehicleType type) {
        setMake(make);
        setModel(model);
        setPrice(price);
        this.type = type;
        comments = new ArrayList<>();
    }

    @Override
    public String getMake() {
        return this.make;
    }

    public void setMake(String make) {
        ValidationHelpers.validateIntRange(make.length(), MAKE_NAME_LEN_MIN, MAKE_NAME_LEN_MAX, MAKE_NAME_LEN_ERR);
        this.make = make;
    }

    @Override
    public String getModel() {
        return this.model;
    }

    public void setModel(String model) {
        ValidationHelpers.validateIntRange(model.length(), MODEL_NAME_LEN_MIN, MODEL_NAME_LEN_MAX, MODEL_NAME_LEN_ERR);
        this.model = model;
    }

    @Override
    public abstract int getWheels();


    public double getPrice() {
        return this.price;
    }

    public void setPrice(double price) {
        ValidationHelpers.validateDecimalRange(price, PRICE_VAL_MIN, PRICE_VAL_MAX, PRICE_VAL_ERR);
        this.price = price;
    }

    @Override
    public VehicleType getType() {
        return this.type;
    }

    @Override
    public void addComment(Comment comment) {
        comments.add(comment);
    }

    @Override
    public void removeComment(Comment comment) {
        comments.remove(comment);
    }

    @Override
    public List<Comment> getComments() {
        return new ArrayList<>(comments);
    }

    @Override
    public String toString() {
        return String.format(
                "Make: %s%n" +
                        "Model: %s%n" +
                        "Wheels: %d%n" +
                        "Price: $%.0f%n", getMake(), getModel(), getWheels(), getPrice());

    }
}


