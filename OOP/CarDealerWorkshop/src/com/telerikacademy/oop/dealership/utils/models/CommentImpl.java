package com.telerikacademy.oop.dealership.utils.models;

import com.telerikacademy.oop.dealership.utils.ValidationHelpers;
import com.telerikacademy.oop.dealership.utils.models.contracts.Comment;

import static java.lang.String.format;

public class CommentImpl implements Comment {

    public static final int CONTENT_LEN_MIN = 3;
    public static final int CONTENT_LEN_MAX = 200;
    private static final String CONTENT_LEN_ERR = format(
            "Content must be between %d and %d characters long!",
            CONTENT_LEN_MIN,
            CONTENT_LEN_MAX);
    private final String author;
    private String content;

    public CommentImpl(String content, String author) {
        this.author = author;
        setContent(content);
    }

    public String getAuthor() {
        return author;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        ValidationHelpers.validateIntRange(content.length(), CONTENT_LEN_MIN, CONTENT_LEN_MAX, CONTENT_LEN_ERR);
        this.content = content;
    }
}
