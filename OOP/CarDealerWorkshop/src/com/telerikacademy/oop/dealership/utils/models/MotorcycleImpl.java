package com.telerikacademy.oop.dealership.utils.models;

import com.telerikacademy.oop.dealership.utils.ValidationHelpers;
import com.telerikacademy.oop.dealership.utils.models.contracts.Motorcycle;
import com.telerikacademy.oop.dealership.utils.models.enums.VehicleType;

import static java.lang.String.format;

public class MotorcycleImpl extends VehicleBase implements Motorcycle {


    public static final int CATEGORY_LEN_MIN = 3;
    public static final int CATEGORY_LEN_MAX = 10;
    private static final String CATEGORY_LEN_ERR = format(
            "Category must be between %d and %d characters long!",
            CATEGORY_LEN_MIN,
            CATEGORY_LEN_MAX);
    private String category;
    private final int wheels;

    public MotorcycleImpl(String make, String model, double price, String category) {
        super(make, model, price,VehicleType.MOTORCYCLE);
        setCategory(category);
        this.wheels = 2;
    }

    @Override
    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        ValidationHelpers.validateIntRange(category.length(), CATEGORY_LEN_MIN, CATEGORY_LEN_MAX, CATEGORY_LEN_ERR);
        this.category = category;
    }

    @Override
    public int getWheels() {
        return this.wheels;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        sb.append(String.format("Category: %s",getCategory())).append(System.lineSeparator());
        if (comments.isEmpty()) {
            sb.append("--NO COMMENTS--").append(System.lineSeparator());
        } else {
            sb.append("--COMMENTS--").append(System.lineSeparator());
            sb.append("----------").append(System.lineSeparator());
            for (int i = 0; i < comments.size(); i++) {
                sb.append(String.format("%s", comments.get(i).getContent())).append(System.lineSeparator());
                sb.append(String.format("User: %s", comments.get(i).getAuthor())).append(System.lineSeparator());
                sb.append("----------").append(System.lineSeparator());
                if (i < comments.size() - 1) {
                    sb.append("----------").append(System.lineSeparator());
                }
            }
            sb.append("--COMMENTS--").append(System.lineSeparator());
        }
        return sb.toString().trim();
    }
}
