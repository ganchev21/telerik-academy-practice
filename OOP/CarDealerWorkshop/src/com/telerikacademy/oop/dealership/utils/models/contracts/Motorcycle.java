package com.telerikacademy.oop.dealership.utils.models.contracts;

public interface Motorcycle extends Vehicle{

    String getCategory();

}
