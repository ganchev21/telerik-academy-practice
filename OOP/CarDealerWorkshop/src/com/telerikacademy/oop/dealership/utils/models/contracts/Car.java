package com.telerikacademy.oop.dealership.utils.models.contracts;

public interface Car extends Vehicle {

    int getSeats();

}
