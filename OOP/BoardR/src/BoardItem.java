import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class BoardItem {
    private String title;
    private LocalDate dueDate;
    public Status status = Status.OPEN;
    private int index = 0;
    private final List<EventLog> history = new ArrayList<>();

    public BoardItem(String title, LocalDate plusDays) {
        setTitle(title);
        this.dueDate = plusDays;
        history.add(new EventLog("Item created: " + String.format("'%s', [%s | %s]", title, status, dueDate)));

    }
    //[15-September-2020 12:57:22] Item created: 'Refactor this mess', [Open | 2020-09-17]

    public LocalDate getDueDate() {
        return dueDate;
    }


    public void setDueDate(LocalDate dueDate) {
        if (dueDate.isBefore(LocalDate.now())) {
            throw new IllegalArgumentException("Invalid date!");
        }
        String currentDueDate = String.valueOf(this.dueDate);
        this.dueDate = dueDate;
        //{Property} changed from {previous} to {new}
        history.add(new EventLog("DueDate changed from " + currentDueDate + " to " + dueDate));
    }

    public void setTitle(String title) {
        if (title.length() < 5 || title.length() > 30) {
            throw new IllegalArgumentException("Please provide a title with length between 5 and 30 chars");
        }
        String previousTitle = this.title;
        this.title = title;
        if (previousTitle != null) {
            history.add(new EventLog("Title changed from " + previousTitle + " to " + title));
        }


    }

    public String getTitle() {
        return title;
    }

    public Status getStatus() {
        return status;
    }

    public void revertStatus() {
        int position = index;
        Status[] array = Status.values().clone();
        Status currentStatus = array[index];
        boolean isInvalid = false;
        index--;
        if (index < 0) {
            index = 0;
            isInvalid = true;
            status = array[index];
        } else {
            status = array[index];
        }
        if (isInvalid) {
            history.add(new EventLog("Can't revert, already at Open"));
        } else {
            history.add(new EventLog("Status changed from " + currentStatus + " to " + status));
        }
    }

    public void advanceStatus() {
        int position = index;
        Status[] array = Status.values().clone();
        Status currentStatus = array[index];
        boolean isInvalid = false;
        index++;
        if (index >= Status.values().length) {
            index = Status.values().length - 1;
            isInvalid = true;
//            status = Status.valueOf("Can't advance, already at Verified");
            status = array[index];
        } else {
            status = array[index];
        }
        if (isInvalid) {
            history.add(new EventLog("Can't advance, already at Verified"));
        } else {
            history.add(new EventLog("Status changed from " + currentStatus + " to " + status));
        }
    }

    public String viewInfo() {
        return String.format("%s, [%s | %s%n", title, status, dueDate);
    }
    public String displayHistory () {
        for (EventLog item : history) {
            System.out.println(item.viewInfo());
        }
        return null;
    }
}
