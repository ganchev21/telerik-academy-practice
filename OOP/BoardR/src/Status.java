public enum Status {
    //Open -> Todo -> InProgress -> Done -> Verified
    OPEN, TO_DO, IN_PROGRESS, DONE, VERIFIED;

    @Override
    public String toString() {
        switch (this) {
            case OPEN:
                return "Open";
            case TO_DO:
                return "ToDo";
            case IN_PROGRESS:
                return "InProgress";
            case DONE:
                return "Done";
            case VERIFIED:
                return "Verified";
        }
        return "UNKNOWN";
    }
}