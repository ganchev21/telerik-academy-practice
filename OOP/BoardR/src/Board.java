import java.util.ArrayList;
import java.util.List;

public class Board {
    private List<BoardItem> storage;


    public Board() {
        this.storage = new ArrayList<>();
    }
    void addItem (BoardItem item) {
        if (storage.contains(item)) {
            throw new IllegalArgumentException("Item already in the list");
        }
        storage.add(item);
    }

    public int totalItems() {
        return storage.size();
    }
}

