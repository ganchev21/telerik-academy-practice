import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

public class EventLog {
    private final String description;
    private final LocalDate timestamp;


    public EventLog(String description) {
        this.description = description;
        this.timestamp = LocalDate.now();
    }
    public EventLog () {
        throw new IllegalArgumentException("Description cannot be empty");
    }


    public String getDescription() {
        return description;
    }
    public String viewInfo () {
        return String.format("[%s-%s-%s %s]", timestamp.getDayOfMonth(), timestamp.getMonth().toString().toUpperCase().charAt(0) +
                timestamp.getMonth().toString().substring(1).toLowerCase(),
                timestamp.getYear(), LocalTime.now().toString().substring(0, 8)) + " " + description;
    }

}
