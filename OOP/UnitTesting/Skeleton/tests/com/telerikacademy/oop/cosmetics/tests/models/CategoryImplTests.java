package com.telerikacademy.oop.cosmetics.tests.models;

import com.telerikacademy.oop.cosmetics.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.cosmetics.models.CategoryImpl;
import com.telerikacademy.oop.cosmetics.models.GenderType;
import com.telerikacademy.oop.cosmetics.models.ProductImpl;
import com.telerikacademy.oop.cosmetics.models.contracts.Category;
import com.telerikacademy.oop.cosmetics.models.contracts.Product;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.telerikacademy.oop.cosmetics.models.CategoryImpl.NAME_MAX_LENGTH;
import static com.telerikacademy.oop.cosmetics.models.CategoryImpl.NAME_MIN_LENGTH;
import static com.telerikacademy.oop.cosmetics.tests.models.ProductImplTests.initializeProduct;
import static com.telerikacademy.oop.cosmetics.tests.utils.TestUtilities.getString;

public class CategoryImplTests {
    public static final String VALID_NAME = getString(NAME_MIN_LENGTH);

    public static final String INVALID_NAME_SHORTER = getString(NAME_MIN_LENGTH - 1);
    public static final String INVALID_NAME_LONGER = getString(NAME_MAX_LENGTH + 1);


    @Test
    public void CategoryImpl_Should_ImplementCategoryInterface() {
        Category category = initializeCategory();
        Assertions.assertTrue(category instanceof Category);
    }

    @Test
    public void constructor_Should_InitializeName_When_ArgumentsAreValid() {
        // Arrange
        Category category = initializeCategory();
        // Act, Assert
        Assertions.assertEquals(VALID_NAME, category.getName());
    }

    @Test
    public void constructor_Should_InitializeProducts_When_ArgumentsAreValid() {
        // Arrange
        Category category = initializeCategory();
        // Act, Assert
        Assertions.assertNotNull(category.getProducts());

    }

    @Test
    public void constructor_Should_ThrowException_When_NameIsShorterThanExpected() {
        //Arrange
        Category category = initializeCategory();
        //Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class, () ->
                new CategoryImpl(INVALID_NAME_SHORTER));

    }
    @Test
    public void constructor_Should_ThrowException_When_NameIsOutOfBounds() {
        //Arrange
        Category category = initializeCategory();
        //Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class, () ->
                new CategoryImpl(INVALID_NAME_LONGER));
    }

    @Test
    public void addProduct_Should_AddProductToList() {
        //Arrange
        Category category = initializeCategory();
        Product product = initializeProduct();
        //Act
        category.addProduct(product);
        //Assert
        Assertions.assertEquals(1, category.getProducts().size());


    }

    @Test
    public void removeProduct_Should_RemoveProductFromList_When_ProductExist() {
        //Arrange
        Category category = initializeCategory();
        Product product = initializeProduct();
        //Act
        category.addProduct(product);
        category.removeProduct(product);
        //Assert
        Assertions.assertEquals(0, category.getProducts().size());
    }

    @Test
    public void removeProduct_should_notRemoveProductFromList_when_productNotExist() {
        //Arrange
        Category category = initializeCategory();
        Product product = initializeProduct();
        Product product1 = new ProductImpl("####", "#####", 2, GenderType.WOMEN);
        //Act
        category.addProduct(product);
        category.addProduct(product);
        if (!category.getProducts().contains(product1)) {
            category.removeProduct(product1);
        }
        //Assert
        Assertions.assertEquals(2, category.getProducts().size());
    }

    public static Category initializeCategory () {
        return new CategoryImpl(VALID_NAME);
    }

}
