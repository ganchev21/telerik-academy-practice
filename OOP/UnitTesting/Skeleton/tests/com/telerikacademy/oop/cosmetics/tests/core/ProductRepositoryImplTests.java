package com.telerikacademy.oop.cosmetics.tests.core;

import com.telerikacademy.oop.cosmetics.commands.AddProductToCategoryCommand;
import com.telerikacademy.oop.cosmetics.commands.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.ProductRepositoryImpl;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import com.telerikacademy.oop.cosmetics.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.cosmetics.models.CategoryImpl;
import com.telerikacademy.oop.cosmetics.models.GenderType;
import com.telerikacademy.oop.cosmetics.models.ProductImpl;
import com.telerikacademy.oop.cosmetics.models.contracts.Category;
import com.telerikacademy.oop.cosmetics.models.contracts.Product;
import com.telerikacademy.oop.cosmetics.tests.models.CategoryImplTests;
import com.telerikacademy.oop.cosmetics.tests.models.ProductImplTests;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.telerikacademy.oop.cosmetics.tests.models.CategoryImplTests.VALID_NAME;
import static com.telerikacademy.oop.cosmetics.tests.models.ProductImplTests.VALID_BRAND;
import static com.telerikacademy.oop.cosmetics.tests.models.ProductImplTests.VALID_PRICE;

public class ProductRepositoryImplTests {

    private ProductRepository repository;

    @BeforeEach
    public void before() {
        repository = new ProductRepositoryImpl();
    }


    @Test
    public void constructor_Should_InitializeProducts() {
        //Arrange, Act, Assert
        Assertions.assertNotNull(repository.getProducts());
    }

    @Test
    public void constructor_Should_InitializeCategories() {
        //Arrange, Act, Assert
        Assertions.assertNotNull(repository.getCategories());
    }

    @Test
    public void getCategories_Should_ReturnCopyOfCollection() {
        //Arrange
        Category category = new CategoryImpl(VALID_NAME);
        repository.createCategory(category.getName());
        repository.getCategories().remove(category);
        //Act, Assert
        Assertions.assertEquals(1, repository.getCategories().size());
    }

    @Test
    public void getProducts_Should_ReturnCopyOfCollection() {
        //Arrange
        Product product = new ProductImpl(ProductImplTests.VALID_NAME, VALID_BRAND, VALID_PRICE, GenderType.UNISEX);
        repository.createProduct(product.getName(), product.getBrand(), product.getPrice(), product.getGender());
        repository.getProducts().remove(product);
        //Act, Assert
        Assertions.assertEquals(1, repository.getProducts().size());
    }

    @Test
    public void categoryExists_Should_ReturnTrue_When_CategoryExists() {
        //Arrange
        Category category = new CategoryImpl(VALID_NAME);
        repository.createCategory(category.getName());
        //Act, Assert
        Assertions.assertTrue(repository.categoryExist(category.getName()));

    }

    @Test
    public void categoryExists_Should_ReturnFalse_When_CategoryDoesNotExist() {
        //Arrange
        Category category = new CategoryImpl(VALID_NAME);
        //Act, Assert
        Assertions.assertFalse(repository.categoryExist(category.getName()));
    }

    @Test
    public void productExists_Should_ReturnTrue_When_ProductExists() {
        //Arrange
        Product product = new ProductImpl(ProductImplTests.VALID_NAME, VALID_BRAND,
                VALID_PRICE, GenderType.UNISEX);

        repository.createProduct(product.getName(), product.getBrand(), product.getPrice(),
                product.getGender());

        //Act, Assert
        Assertions.assertTrue(repository.productExist(product.getName()));
    }

    @Test
    public void productExists_Should_ReturnFalse_When_ProductDoesNotExist() {
        //Arrange
        Product product = new ProductImpl(ProductImplTests.VALID_NAME, VALID_BRAND,
                VALID_PRICE, GenderType.UNISEX);

        //Act, Assert
        Assertions.assertFalse(repository.productExist(product.getName()));
    }

    @Test
    public void createProduct_Should_AddToProducts_When_ArgumentsAreValid() {
        //Arrange
        Category category = new CategoryImpl(VALID_NAME);
        Product product = new ProductImpl(ProductImplTests.VALID_NAME, VALID_BRAND,
                VALID_PRICE, GenderType.UNISEX);
        category.addProduct(product);
        Assertions.assertEquals(1, category.getProducts().size());

    }

    @Test
    public void createCategory_Should_AddToCategories_When_ArgumentsAreValid() {
        // Arrange
        Command addProductsToCategory = new AddProductToCategoryCommand(repository);
        Category category = new CategoryImpl(VALID_NAME);
        repository.createCategory(category.getName());
        Product product = new ProductImpl(ProductImplTests.VALID_NAME, VALID_BRAND, VALID_PRICE, GenderType.UNISEX);
        repository.createProduct(product.getName(), product.getBrand(), product.getPrice(), product.getGender());
        List<String> params = List.of(category.getName(), product.getName());
        addProductsToCategory.execute(params);

        // Act, Assert
        Assertions.assertEquals(1, repository.getCategories().size());

    }

    @Test
    public void findCategoryByName_Should_ReturnCategory_When_Exists() {
        //Arrange
        Category category = new CategoryImpl(VALID_NAME);
        repository.createCategory(category.getName());
        Category exist = repository.findCategoryByName(category.getName());
        //Act, Assert
        Assertions.assertEquals(exist.getName(), category.getName());
    }

    @Test
    public void findCategoryByName_Should_ThrowException_When_DoesNotExist() {
        Assertions.assertThrows(InvalidUserInputException.class, () -> repository.
                findCategoryByName(VALID_NAME));
    }

    @Test
    public void findProductByName_Should_ReturnProduct_When_Exists() {
        //Arrange
        Product product = new ProductImpl(
                ProductImplTests.VALID_NAME, VALID_BRAND, VALID_PRICE, GenderType.UNISEX);
        repository.createProduct(product.getName(), product.getBrand(), product.getPrice(), product.getGender());
        Product exist = repository.findProductByName(product.getName());
        //Act, Assert
        Assertions.assertEquals(exist.getName(), product.getName());
    }

    @Test
    public void findProductByName_Should_ThrowException_When_DoesNotExist() {
        Assertions.assertThrows(InvalidUserInputException.class, () -> repository.
                findCategoryByName(ProductImplTests.VALID_NAME));
    }

}
