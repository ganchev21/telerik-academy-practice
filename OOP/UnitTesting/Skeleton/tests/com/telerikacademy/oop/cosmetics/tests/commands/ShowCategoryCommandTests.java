package com.telerikacademy.oop.cosmetics.tests.commands;

import com.telerikacademy.oop.cosmetics.commands.ShowCategoryCommand;
import com.telerikacademy.oop.cosmetics.commands.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.ProductRepositoryImpl;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import com.telerikacademy.oop.cosmetics.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.cosmetics.models.CategoryImpl;
import com.telerikacademy.oop.cosmetics.models.contracts.Category;
import com.telerikacademy.oop.cosmetics.models.contracts.Product;
import com.telerikacademy.oop.cosmetics.tests.models.CategoryImplTests;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.oop.cosmetics.tests.models.CategoryImplTests.VALID_NAME;
import static com.telerikacademy.oop.cosmetics.tests.models.ProductImplTests.initializeProduct;

public class ShowCategoryCommandTests {

    private ProductRepository repository;
    private Command showCategory;

    @BeforeEach
    public void before() {
        repository = new ProductRepositoryImpl();
        showCategory = new ShowCategoryCommand(repository);
    }

    @Test
    public void should_ThrowException_When_ArgumentCountDifferentThanExpected() {
        //Arrange
        List<String> params = new ArrayList<>();
        //Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class, () -> showCategory.execute(params));
    }

    @Test
    public void should_ThrowException_When_CategoryNameIsInvalid() {
        //Arrange
        List<String> params = List.of(CategoryImplTests.INVALID_NAME_LONGER);
        //Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class, () -> showCategory.execute(params));
    }

    @Test
    public void should_ShowCategory_When_ArgumentsAreValid() {
        // Arrange
        Category category = new CategoryImpl(VALID_NAME);
        repository.createCategory(category.getName());
        showCategory.execute(List.of(category.getName()));

        // Act, Assert
        Assertions.assertEquals(1, repository.getCategories().size());
    }


    @Test
    public void should_PrintCategory_When_CategoryIsNotEmpty() {
        Category category = new CategoryImpl(VALID_NAME);
        Product product = initializeProduct();
        category.addProduct(product);
        repository.createCategory(category.getName());

        StringBuilder sb = new StringBuilder();
        sb.append(String.format("#Category: %s%n", category.getName()));
        for (Product item : category.getProducts()) {
            sb.append(product.print());
            sb.append(String.format(" ===%n"));
        }

        Assertions.assertEquals(sb.toString(), category.print());
    }

}
