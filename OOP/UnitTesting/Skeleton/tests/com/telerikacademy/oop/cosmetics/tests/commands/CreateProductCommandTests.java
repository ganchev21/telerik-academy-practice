package com.telerikacademy.oop.cosmetics.tests.commands;

import com.telerikacademy.oop.cosmetics.commands.CreateCategoryCommand;
import com.telerikacademy.oop.cosmetics.commands.CreateProductCommand;
import com.telerikacademy.oop.cosmetics.commands.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.ProductRepositoryImpl;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import com.telerikacademy.oop.cosmetics.exceptions.DuplicateEntityException;
import com.telerikacademy.oop.cosmetics.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.cosmetics.models.GenderType;
import com.telerikacademy.oop.cosmetics.models.ProductImpl;
import com.telerikacademy.oop.cosmetics.models.contracts.Product;
import com.telerikacademy.oop.cosmetics.tests.models.CategoryImplTests;
import com.telerikacademy.oop.cosmetics.tests.models.ProductImplTests;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.oop.cosmetics.tests.models.ProductImplTests.*;

public class CreateProductCommandTests {

    public static final int EXPECTED_PARAMETERS_COUNT = 4;
    private ProductRepository repository;
    private CreateProductCommand createProduct;

    @BeforeEach
    public void before() {
        repository = new ProductRepositoryImpl();
        createProduct = new CreateProductCommand(repository);
    }

    @Test
    public void should_CreateProduct_When_ArgumentsAreValid() {
        //Act, Arrange
        Product product = new ProductImpl(VALID_NAME, VALID_BRAND, VALID_PRICE, GenderType.UNISEX);
        repository.createProduct(product.getName(), product.getBrand(), product.getPrice(), product.getGender());
        //Assert
        Assertions.assertEquals(1, repository.getProducts().size());
    }

    @Test
    public void execute_Should_AddNewProductToRepository_When_ValidParameters() {
        // Arrange
        List<String> params = List.of(VALID_NAME,
                VALID_BRAND,
                String.valueOf(VALID_PRICE),
                GenderType.UNISEX.toString());
        //Act
        repository.createProduct(params.get(0), params.get(1), Double.parseDouble(params.get(2)),
                GenderType.UNISEX);
        //Assert
        Assertions.assertEquals(1, repository.getProducts().size());
    }

    @Test
    public void execute_Should_ThrowException_When_MissingParameters() {
        //Arrange
        List<String> params = new ArrayList<>();
        //Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class, () -> createProduct.execute(params));
    }

    @Test
    public void execute_Should_ThrowException_When_DuplicateProductName() {

        //Act, Arrange
        Product product = initializeProduct();
        repository.createProduct(product.getName(), product.getBrand(), product.getPrice(), product.getGender());
        List<String> params = List.of(VALID_NAME,
                VALID_BRAND,
                String.valueOf(product.getPrice()),
                String.valueOf(product.getGender()));

        //Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> createProduct.execute(params));
    }

    @Test
    public void should_ThrowException_When_ProductNameIsInvalid() {
        // Arrange
        List<String> params = List.of(INVALID_NAME_SHORTER, VALID_BRAND, String.valueOf(VALID_PRICE),
                GenderType.UNISEX.toString());

        // Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class, () -> createProduct.execute(params));
    }

    @Test
    public void tryParseDouble_Should_ThrowException_WhenPriceIsNotNumber() {
        //Arrange
        String value = "gosho";
        List<String> params = List.of(VALID_NAME, VALID_BRAND, value,
                GenderType.UNISEX.toString());
        //Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class, () -> createProduct.execute(params));
    }

    @Test
    public void tryParseDouble_Should_ThrowException_WhenGenderIsNotValid() {
        //Arrange
        String value = "gosho";
        List<String> params = List.of(VALID_NAME, VALID_BRAND, String.valueOf(VALID_PRICE),
                value);
        //Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class, () -> createProduct.execute(params));
    }

}