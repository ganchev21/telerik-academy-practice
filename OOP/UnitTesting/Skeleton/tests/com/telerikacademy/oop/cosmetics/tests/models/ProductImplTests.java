package com.telerikacademy.oop.cosmetics.tests.models;

import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import com.telerikacademy.oop.cosmetics.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.cosmetics.models.CategoryImpl;
import com.telerikacademy.oop.cosmetics.models.GenderType;
import com.telerikacademy.oop.cosmetics.models.ProductImpl;
import com.telerikacademy.oop.cosmetics.models.contracts.Category;
import com.telerikacademy.oop.cosmetics.models.contracts.Product;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static com.telerikacademy.oop.cosmetics.models.ProductImpl.*;
import static com.telerikacademy.oop.cosmetics.tests.utils.TestUtilities.getString;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ProductImplTests {
    //Which are the test cases?
    public static final String VALID_NAME = getString(NAME_MIN_LENGTH);
    public static final String INVALID_NAME_SHORTER = getString(NAME_MIN_LENGTH - 1);
    public static final String NAME_OUT_OF_BOUNDS = getString(NAME_MAX_LENGTH + 1);
    public static final String VALID_BRAND = getString(BRAND_MIN_LENGTH);
    public static final String INVALID_BRAND_SHORTER = getString(BRAND_MIN_LENGTH - 1);
    public static final String BRAND_OUT_OF_BOUNDS = getString(BRAND_MAX_LENGTH + 1);
    public static final double VALID_PRICE = 0;
    public static final double INVALID_PRICE = VALID_PRICE - 1;

    @Test
    public void CategoryImpl_Should_ImplementCategoryInterface() {
        Product product = initializeProduct();
        Assertions.assertTrue(product instanceof Product);
    }

    @Test
    public void constructor_Should_CreateNewProduct_When_ParametersAreCorrect() {
        // Arrange, Act
        Product product = initializeProduct();

        // Assert
        assertAll(
                () -> assertEquals(VALID_NAME, product.getName()),
                () -> assertEquals(VALID_BRAND, product.getBrand()),
                () -> assertEquals(VALID_PRICE, product.getPrice()),
                () -> assertEquals(GenderType.UNISEX, product.getGender())
        );
    }

    @Test
    public void constructor_Should_InitializeName_When_ArgumentsAreValid() {
        // Arrange
        Product product = initializeProduct();
        // Act, Assert
        Assertions.assertEquals(VALID_NAME, product.getName());

    }
    @Test
    public void constructor_Should_InitializeBrand_When_ArgumentsAreValid() {
        // Arrange
        Product product = initializeProduct();
        // Act, Assert
        Assertions.assertEquals(VALID_BRAND, product.getBrand());

    }
    @Test
    public void constructor_Should_InitializePrice_When_ArgumentsAreValid() {
        // Arrange
        Product product = initializeProduct();
        // Act, Assert
        Assertions.assertEquals(VALID_PRICE, product.getPrice());
    }
    @Test
    public void constructor_Should_InitializeGender_When_ArgumentsAreValid() {
        // Arrange
        Product product = initializeProduct();
        // Act, Assert
        Assertions.assertEquals(GenderType.UNISEX, product.getGender());
    }
    @Test
    public void constructor_Should_ThrowException_When_NameIsOutOfBounds() {
        //Arrange
        Product product = initializeProduct();
        //Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class, () ->
                new ProductImpl(NAME_OUT_OF_BOUNDS, VALID_BRAND,VALID_PRICE, GenderType.UNISEX));
    }

    @Test
    public void constructor_Should_ThrowException_When_NameIsShorterThanExpected() {
        //Arrange
        Product product = initializeProduct();
        //Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class, () ->
          new ProductImpl(INVALID_NAME_SHORTER, VALID_BRAND,VALID_PRICE, GenderType.UNISEX));
    }

    @Test
    public void constructor_Should_ThrowException_When_BrandIsShorterThanExpected() {
        //Arrange
        Product product = initializeProduct();
        //Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class, () ->
          new ProductImpl(VALID_NAME,INVALID_BRAND_SHORTER, VALID_PRICE, GenderType.UNISEX));
    }

    @Test
    public void constructor_Should_ThrowException_When_BrandIsOutOfBounds() {
        //Arrange
        Product product = initializeProduct();
        //Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class, () ->
          new ProductImpl(VALID_NAME,BRAND_OUT_OF_BOUNDS, VALID_PRICE, GenderType.UNISEX));
    }

    @Test
    public void constructor_Should_ThrowException_When_PriceIsNegative() {
        //Arrange
        Product product = initializeProduct();
        //Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class, () ->
          new ProductImpl(VALID_NAME, VALID_BRAND, -1, GenderType.UNISEX));
    }

    public static Product initializeProduct () {
        return new ProductImpl(VALID_NAME, VALID_BRAND, VALID_PRICE, GenderType.UNISEX);
    }
    public static Product initializeProductToRepository (ProductRepository repository) {
        Product product = initializeProduct();
        repository.createProduct(product.getName(),
                product.getBrand(),
                product.getPrice(),
                product.getGender());

        return product;
    }
}
