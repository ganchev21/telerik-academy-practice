package com.telerikacademy.oop.cosmetics.tests.commands;

import com.telerikacademy.oop.cosmetics.commands.AddProductToCategoryCommand;
import com.telerikacademy.oop.cosmetics.commands.CreateCategoryCommand;
import com.telerikacademy.oop.cosmetics.commands.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.ProductRepositoryImpl;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import com.telerikacademy.oop.cosmetics.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.cosmetics.models.CategoryImpl;
import com.telerikacademy.oop.cosmetics.models.GenderType;
import com.telerikacademy.oop.cosmetics.models.ProductImpl;
import com.telerikacademy.oop.cosmetics.models.contracts.Category;
import com.telerikacademy.oop.cosmetics.models.contracts.Product;
import com.telerikacademy.oop.cosmetics.tests.models.CategoryImplTests;
import com.telerikacademy.oop.cosmetics.tests.models.ProductImplTests;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.telerikacademy.oop.cosmetics.tests.models.CategoryImplTests.VALID_NAME;
import static com.telerikacademy.oop.cosmetics.tests.models.ProductImplTests.VALID_BRAND;
import static com.telerikacademy.oop.cosmetics.tests.models.ProductImplTests.VALID_PRICE;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class AddProductToCategoryCommandTests {

    private Command addProductToCategory;
    private ProductRepository repository;

    @BeforeEach
    public void before() {
        repository = new ProductRepositoryImpl();
        addProductToCategory = new AddProductToCategoryCommand(repository);
    }

    @Test
    public void execute_Should_AddNewProductToCategory_When_ValidParameters() {
        // Arrange
        Category category = new CategoryImpl(VALID_NAME);
        repository.createCategory(category.getName());
        Product product = new ProductImpl(ProductImplTests.VALID_NAME, VALID_BRAND, VALID_PRICE, GenderType.UNISEX);
        repository.createProduct(product.getName(), product.getBrand(), product.getPrice(), product.getGender());
        List<String> params = List.of(category.getName(), product.getName());
        addProductToCategory.execute(params);

        // Act, Assert
        Assertions.assertEquals(1, repository.getCategories().size());
    }

    @Test
    public void execute_Should_ThrowException_When_InValidParameters() {
        // Arrange
        List<String> params = List.of();

        // Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class, () -> addProductToCategory.execute(params));
    }

    @Test
    public void should_ThrowException_When_ProductDoesNotExist() {
        // Arrange
        Product product = new ProductImpl(ProductImplTests.VALID_NAME, VALID_BRAND, VALID_PRICE, GenderType.UNISEX);
        repository.createProduct(product.getName(), product.getBrand(), product.getPrice(), product.getGender());
        List<String> params = List.of(VALID_NAME, product.getName());

        // Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class, () -> addProductToCategory.execute(params));
    }

    @Test
    public void should_ThrowException_When_CategoryDoesNotExist() {
        // Arrange
        Category category = new CategoryImpl(VALID_NAME);
        repository.createCategory(category.getName());
        List<String> params = List.of(category.getName(), ProductImplTests.VALID_NAME);

        // Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class, () -> addProductToCategory.execute(params));
    }

    @Test
    public void should_ThrowException_When_CategoryNameIsInvalid() {
        // Arrange
        List<String> params = List.of(CategoryImplTests.INVALID_NAME_LONGER, ProductImplTests.VALID_NAME);

        // Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class, () -> addProductToCategory.execute(params));
    }

    @Test
    public void should_ThrowException_When_ProductNameIsInvalid() {
        // Arrange
        List<String> params = List.of(VALID_NAME, ProductImplTests.INVALID_NAME_SHORTER);

        // Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class, () -> addProductToCategory.execute(params));
    }

}
