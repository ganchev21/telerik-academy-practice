package com.telerikacademy.oop.cosmetics.tests.commands;

import com.telerikacademy.oop.cosmetics.commands.CreateCategoryCommand;
import com.telerikacademy.oop.cosmetics.commands.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.ProductRepositoryImpl;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import com.telerikacademy.oop.cosmetics.exceptions.DuplicateEntityException;
import com.telerikacademy.oop.cosmetics.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.cosmetics.models.CategoryImpl;
import com.telerikacademy.oop.cosmetics.models.contracts.Category;
import com.telerikacademy.oop.cosmetics.tests.models.CategoryImplTests;
import com.telerikacademy.oop.cosmetics.tests.utils.TestUtilities;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.oop.cosmetics.tests.models.CategoryImplTests.*;
import static com.telerikacademy.oop.cosmetics.tests.utils.TestUtilities.getList;

public class CreateCategoryCommandTests {
    public static final int EXPECTED_PARAMETERS_COUNT = 1;;
    private ProductRepository repository;
    private Command createCategory;
    // @BeforeEach may help here
    @BeforeEach
    public void before() {
        repository = new ProductRepositoryImpl();
        createCategory = new CreateCategoryCommand(repository);
    }

    @Test
    public void execute_Should_AddNewCategoryToRepository_When_ValidParameters() {
        // Arrange
        List<String> params = List.of(CategoryImplTests.VALID_NAME);
        //Act
        repository.createCategory(params.get(0));
        //Assert
        Assertions.assertEquals(1, repository.getCategories().size());
    }

    @Test
    public void execute_Should_ThrowException_When_MissingParameters() {
        //Arrange
        List<String> params = new ArrayList<>();
        //Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class, () -> createCategory.execute(params));
    }

    @Test
    public void execute_Should_ThrowException_When_ArgumentsAreInvalid() {
        //Arrange
        List<String> params = List.of(INVALID_NAME_SHORTER);
        //Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class, () -> createCategory.execute(params));
    }

    @Test
    public void execute_Should_ThrowException_When_DuplicateCategoryName() {
        //Arrange
        Category category = new CategoryImpl(VALID_NAME);
        repository.createCategory(category.getName());
        repository.findCategoryByName(category.getName());
        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> createCategory.execute(List.of(category.getName())));

    }

}
