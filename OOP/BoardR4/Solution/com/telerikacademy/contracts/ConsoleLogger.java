package com.telerikacademy.contracts;

import com.telerikacademy.models.Board;

public class ConsoleLogger implements Logger{

    @Override
    public void log(String value) {
        System.out.println(value);
    }
}
