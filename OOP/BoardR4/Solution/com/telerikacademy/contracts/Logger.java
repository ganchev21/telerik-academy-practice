package com.telerikacademy.contracts;

public interface Logger {
    public abstract void log (String value);
}
