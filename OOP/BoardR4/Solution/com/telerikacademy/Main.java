package com.telerikacademy;

import com.telerikacademy.contracts.ConsoleLogger;
import com.telerikacademy.contracts.Logger;
import com.telerikacademy.models.Board;
import com.telerikacademy.models.BoardItem;
import com.telerikacademy.models.Issue;
import com.telerikacademy.models.Task;

import java.time.LocalDate;

public class Main {

    public static void main(String[] args) {

        LocalDate tomorrow = LocalDate.now().plusDays(1);
        Task task = new Task("Write unit tests", "Pesho", tomorrow);
        Issue issue = new Issue("Review tests", "Someone must review Pesho's tests.", tomorrow);

        Board board = new Board();

        board.addItem(task);
        board.addItem(issue);

        ConsoleLogger logger = new ConsoleLogger();
        board.displayHistory(logger);
    }

}
