package com.telerikacademy.oop;

import java.util.ArrayList;
import java.util.Iterator;

public class Main {
    public static void main(String[] args) {

        MyListImpl<String> test = new MyListImpl<>(1);

        test.add("water");
        test.add("gold");
        test.add("water");
        test.add("bear");
        test.add("gun");

        test.swap(4,0);
        test.print();

//        System.out.println("Before");
//        for (String s : test) {
//            System.out.print(s + " ");
//        }
//        System.out.println();
//        System.out.println();
//
//        test.remove("water");
//        System.out.println("After command: remove");
//        for (String s : test) {
//            System.out.print(s + " ");
//        }
//        System.out.println();
//        System.out.println();
//
//        test.removeAt(3);
//        System.out.println("After command: removeAt");
//        for (String s : test) {
//            System.out.print(s + " ");
//        }
//        System.out.println();
//        System.out.println();
//
//        System.out.println(test.size());
//        System.out.println(test.capacity());


    }
}