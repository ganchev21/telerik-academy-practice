package com.telerikacademy.oop;

import java.util.Arrays;
import java.util.Iterator;

public class MyListImpl<T> implements MyList<T> {

    public static final int INITIAL_CAPACITY = 4;
    private int size;
   private int capacity;
   private T[] data = (T[]) new Object[INITIAL_CAPACITY];

    public MyListImpl(int capacity) {
        this.capacity = capacity;
    }

    public MyListImpl() {
        this.size = 0;
        this.capacity = INITIAL_CAPACITY;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public int capacity() {
        return capacity;
    }

    @Override
    public void add(T element) {
        if (size < capacity) {
            data[size] = element;
            size++;
        } else {
            capacity = capacity * 2;
            data = Arrays.copyOf(data, capacity);
            data[size] = element;
            size++;
        }
    }

    @Override
    public T get(int index) {
        if (index >= size) {
            throw new IndexOutOfBoundsException();
        } else {
            return data[index];
        }
    }

    @Override
    public int indexOf(T element) {
        int count = -1;
        for (int i = 0; i < size; i++) {
            count++;
            if (data[i].equals(element)) {
                return count;
            }
        }
        return -1;
    }

    @Override
    public int lastIndexOf(T element) {
        int index = -1;
        for (int i = 0; i < size; i++) {
            if (data[i].equals(element)) {
                index = i;
            }
        }
        if (index != -1) {
            return index;
        }
        return -1;
    }

    @Override
    public boolean contains(T element) {
        for (int i = 0; i < size; i++) {
            if (data[i].equals(element)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void removeAt(int index) {
        if (index >= size) {
            throw new ArrayIndexOutOfBoundsException();
        } else {
            T[] newData = (T[]) new Object[capacity];
            int currentIndex = 0;
            for (int i = 0; i < size; i++) {
                if (i != index) {
                    newData[currentIndex] = data[i];
                    currentIndex++;
                }
            }
            data = Arrays.copyOf(newData, capacity);
            size--;
        }
    }
    @Override
    public boolean remove(T element) {
        if (contains(element)) {
            T[] newData = (T[]) new Object[capacity];
            int index = 0;
            int firstElement = 0;
            for (int i = 0; i < size; i++) {
                if (firstElement < 1) {
                    if (data[i].equals(element)) {
                        firstElement++;
                        continue;
                    } else {
                        newData[index] = data[i];
                        index++;
                    }
                } else {
                    newData[index] = data[i];
                    index++;
                }
            }
            data = Arrays.copyOf(newData, capacity);
            size--;
            return true;
        }
        return false;
    }

    @Override
    public void clear() {
        T[] newData = (T[]) new Object[capacity];
        capacity = INITIAL_CAPACITY;
        size = 0;
        data = Arrays.copyOf(newData, capacity);
    }

    @Override
    public void swap(int from, int to) {
        if (from >= 0 && from < size && to >= 0 && to < size && from != to) {
            T word = data[from];
            data[from] = data[to];
            data[to] = word;
        } else {
            throw new IllegalArgumentException("Invalid index!");
        }
    }

    @Override
    public void print() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (int i = 0; i < size; i++) {
            if (i == size - 1) {
                sb.append(data[i]);
            } else {
                sb.append(data[i] + ", ");
            }
        }
        sb.append("]");
        System.out.println(sb);
    }


    int counter = 0;
    @Override
    public Iterator<T> iterator() {
        Iterator<T> it = new Iterator<T>() {
            private int currentIndex = 0;

            @Override
            public boolean hasNext() {
                return currentIndex < size && data[currentIndex] != null;
            }

            @Override
            public T next() {
                return data[currentIndex++];
            }
        };
        return it;

    }

}
