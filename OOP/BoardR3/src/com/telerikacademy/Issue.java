package com.telerikacademy;

import java.time.LocalDate;

public class Issue extends BoardItem {

    private final String description;
    public Issue(String title, String description, LocalDate dueDate) {
        super(title, dueDate);
        this.description = setDescription(description);
    }

    public String getDescription() {
        return description;
    }

    protected String setDescription(String description) {
        if (description.isBlank()) {
            description = "No description";
        }
        return description;
    }
}
