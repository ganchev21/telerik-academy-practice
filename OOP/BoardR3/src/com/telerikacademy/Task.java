package com.telerikacademy;

import com.telerikacademy.enums.Status;

import java.time.LocalDate;

public class Task extends BoardItem {

    protected static final int ASSIGNEE_MIN_LENGTH = 5;
    protected static final int ASSIGNEE_MAX_LENGTH = 30;
    private static final Status TASK_STATUS = Status.TODO;
    private String assignee;


    public Task(String title, String assignee, LocalDate dueDate) {
        super(title, dueDate);
        setAssignee(assignee);
        super.status = TASK_STATUS;
    }

    public String getAssignee() {
        return assignee;
    }

    public void setAssignee(String assignee) {
        validateAssignee(assignee);

        if (this.assignee != null) {
            logEvent(String.format("Assignee changed from %s to %s", getAssignee(), assignee));
        }

        this.assignee = assignee;
    }

    @Override
    public String viewInfo() {
        return String.format("'%s', [%s | %s]", super.getTitle(), TASK_STATUS, super.getDueDate());
    }


    protected void validateAssignee(String assignee) {
        if (assignee.length() < ASSIGNEE_MIN_LENGTH || assignee.length() > ASSIGNEE_MAX_LENGTH) {
            throw new IllegalArgumentException(String.format(
                    "Please provide a assignee with length between %d and %d chars",
                    TITLE_MIN_LENGTH, TITLE_MAX_LENGTH));
        }

    }
}