package StringManipulation.sherlockAndTheValidString;

import java.util.*;

public class SherlockAndTheValidString {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String text = scanner.nextLine();

        System.out.println(isValid(text));

    }

    public static String isValid(String s) {
        Map<Character, Integer> map = new HashMap<>();
        char[] chars = s.toCharArray();

        for (int i = 0; i < chars.length; i++) {
            if (!map.containsKey(chars[i])) {
                map.put(chars[i], 1);
            } else {
                map.put(chars[i], map.get(chars[i]) + 1);
            }
        }
        int[] array = new int[map.size()];
        Set<Integer> set = new HashSet<>();
        int index = 0;
        for (Map.Entry<Character, Integer> pair : map.entrySet()) {
            array[index] = pair.getValue();
            set.add(pair.getValue());
            index++;
        }

        if (set.size() == 1) {
            return "YES";
        } else if (set.size() > 2) {
            return "NO";
        }


        Arrays.sort(array);

        int firstNumCount = 0;
        int secondNumCount = 0;
        if (array[0] != array[array.length - 1]) {
            int num1 = array[0];
            int num2 = array[array.length - 1];
            if (num1 != 1 && num2 != 1) {
                if (num2 - num1 != 1) {
                    return "NO";
                }
            }

            for (int i = 0; i < array.length; i++) {
                if (array[i] == array[0]) {
                    firstNumCount++;
                } else {
                    secondNumCount++;
                }
            }
            if (firstNumCount == 1 || secondNumCount == 1) {
                return "YES";
            } else {
                return "NO";
            }

        } else {
            return "YES";
        }

    }


}
