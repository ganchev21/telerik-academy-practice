package StringManipulation.alternatingCharacters;

import java.util.Scanner;

public class AlternatingCharacters {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = Integer.parseInt(scanner.nextLine());

        for (int i = 0; i < n; i++) {
            String s = scanner.nextLine();
            System.out.println(alternatingCharacters(s));
        }

    }

    public static int alternatingCharacters(String s) {
        int deletions = 0;
        for (int i = 0; i < s.length() - 1; i++) {
            char currentChar = s.charAt(i);
            if (currentChar == s.charAt(i + 1)) {
                deletions++;
            }
        }

        return deletions;

    }

}
