package StringManipulation.makingAnagrams;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class MakingAnagrams {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String text1 = scanner.nextLine();
        String text2 = scanner.nextLine();

        char[] sorted1 = text1.toCharArray();
        char[] sorted2 = text2.toCharArray();
        Arrays.sort(sorted1);
        Arrays.sort(sorted2);

        // Calculate the count of deletions
        int deletions = 0;
        int i = 0;
        int j = 0;

        while (i < sorted1.length && j < sorted2.length) {
            if (sorted1[i] == sorted2[j]) {
                i++;
                j++;
            } else if (sorted1[i] < sorted2[j]) {
                deletions++;
                i++;
            } else {
                deletions++;
                j++;
            }
        }

        deletions += sorted1.length - i;
        deletions += sorted2.length - j;

        System.out.println(deletions);
    }
}
