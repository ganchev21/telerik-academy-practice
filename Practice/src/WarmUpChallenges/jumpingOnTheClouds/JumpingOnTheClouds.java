package WarmUpChallenges.jumpingOnTheClouds;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class JumpingOnTheClouds {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = Integer.parseInt(scanner.nextLine());


        List<Integer> numbers = Arrays.stream(scanner.nextLine().split(" "))
                .mapToInt(Integer::parseInt)
                .boxed()
                .toList();

        System.out.println(jumpingOnClouds(numbers));

    }

    public static int jumpingOnClouds(List<Integer> c) {

        int moves = 0;
        for (int i = 0; i < c.size(); i++) {
            if (i == c.size() - 1) {
                return moves;
            }
            if (i + 2 <= c.size() - 1) {
                if (c.get(i + 2) != 1) {
                    moves++;
                    i += 1;
                } else {
                    if (i + 1 <= c.size() - 1) {
                        if (c.get(i + 1) != 1) {
                            moves++;
                        }
                    }
                }
            } else {
                if (i + 1 <= c.size() - 1) {
                    if (c.get(i + 1) != 1) {
                        moves++;
                    }
                }
            }


        }

        return moves;

    }


}
