package WarmUpChallenges.salesByMatch;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class SalesByMatch {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = Integer.parseInt(scanner.nextLine());

        Set<Integer> pairs = new HashSet<>();
        int totalPairs = 0;

        for (int i = 0; i < n; i++) {
            int number = Integer.parseInt(scanner.next());
            if (pairs.contains(number)) {
                pairs.remove(number);
                totalPairs++;
            } else {
                pairs.add(number);
            }
        }
        System.out.println(totalPairs);
    }
}
