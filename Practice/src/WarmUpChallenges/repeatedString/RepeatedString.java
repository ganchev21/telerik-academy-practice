package WarmUpChallenges.repeatedString;

import java.util.Scanner;

public class RepeatedString {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String s = scanner.nextLine();
        long n = Long.parseLong(scanner.nextLine());
        System.out.println(repeatedString(s, n));
    }
    public static long repeatedString(String s, long n) {

        int stringLength = s.length();
        long counter = 0;
        long result = 0;
        for (int i = 0; i < stringLength; i++) {
            if (s.charAt(i) == 97) {
                counter++;
            }
        }

        if (n % stringLength == 0) {
            return (n / stringLength) * counter;
        } else {
            long numberToMultiply = n / stringLength;
            result = counter * numberToMultiply;
            long diff = n % stringLength;

            for (int i = 0; i < diff; i++) {
                if (s.charAt(i) == 97) {
                    result++;
                }
            }
            return result;

        }
    }
}
