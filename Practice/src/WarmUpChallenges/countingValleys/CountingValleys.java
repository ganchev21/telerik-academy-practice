package WarmUpChallenges.countingValleys;

import java.util.Scanner;

public class CountingValleys {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int steps = Integer.parseInt(scanner.nextLine());
        String path = scanner.nextLine();
        int counter = 0;
        int altitude = 0;
        for (int i = 0; i < path.length() ; i++) {

            if (path.charAt(i) == 85) {
                altitude++;
                if (altitude == 0) {
                    counter++;
                }
            } else {
                altitude--;
            }
        }
        System.out.println(counter);
    }
}
