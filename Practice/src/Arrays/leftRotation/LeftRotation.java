package Arrays.leftRotation;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class LeftRotation {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String[] firstLine = scanner.nextLine().split(" ");
        List<Integer> numbers = Arrays.stream(scanner.nextLine().split(" "))
                .mapToInt(Integer::parseInt).boxed().collect(Collectors.toList());

        int rotations = Integer.parseInt(firstLine[1]);

        for (int i = 0; i < rotations; i++) {
            int numberToAdd = numbers.get(0);
            numbers.add(numberToAdd);
            numbers.remove(0);
        }

        numbers.forEach(num -> System.out.print(num + " "));


    }
}
