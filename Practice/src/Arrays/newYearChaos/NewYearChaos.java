package Arrays.newYearChaos;

import java.util.*;

public class NewYearChaos {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = Integer.parseInt(scanner.nextLine());

        Set<Integer> set = new HashSet<>();

        for (int i = 0; i < n; i++) {
            boolean isChaotic = false;
            int numberOfPeople = Integer.parseInt(scanner.nextLine());
            List<Integer> q = Arrays.stream(scanner.nextLine().split(" "))
                    .mapToInt(Integer::parseInt).boxed().toList();

            int result = 0;
            for (int j = 0; j < q.size(); j++) {
                if (q.get(j) - 1 > j + 2) {
                    System.out.println("Too chaotic");
                    isChaotic = true;
                    break;
                }
                int counter = 0;
                int currentNumber = q.get(j);
                set.add(currentNumber);



                for (int k = j + 1; k < q.size(); k++) {
                    if (currentNumber > q.get(k)) {
                        counter++;
                    }
                }
                result += counter;
            }


            if (!isChaotic) {
                System.out.println(result);
            }
        }

    }
}
