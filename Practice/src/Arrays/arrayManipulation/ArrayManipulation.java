package Arrays.arrayManipulation;

import java.util.Scanner;

public class ArrayManipulation {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String[] firstLine = scanner.nextLine().split(" ");
        int n = Integer.parseInt(firstLine[0]);
        int m = Integer.parseInt(firstLine[1]);

        long[] arr = new long[n];

        for (int i = 0; i < m; i++) {
            String[] inputArr = scanner.nextLine().split(" ");
            int startIndex = Integer.parseInt(inputArr[0]) - 1;
            int endIndex = Integer.parseInt(inputArr[1]);
            int numToAdd = Integer.parseInt(inputArr[2]);

            arr[startIndex] += numToAdd;
            if (endIndex < n) {
                arr[endIndex] -= numToAdd;
            }
        }

        long maxNum = 0;
        long prefixSum = 0;

        for (int i = 0; i < n; i++) {
            prefixSum += arr[i];
            maxNum = Math.max(maxNum, prefixSum);
        }

        System.out.println(maxNum);
    }
}