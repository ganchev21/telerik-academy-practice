package Arrays.array2D;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Array2D {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int[][] matrix = new int[6][6];

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j <matrix[i].length ; j++) {
                matrix[i][j] = Integer.parseInt(scanner.next());
            }
        }

        /*
        List<List<Integer>> arr = new ArrayList<>();

        for (int i = 0; i < 6; i++) {
            List<Integer> row = new ArrayList<>();
            for (int j = 0; j < 6; j++) {
                int num = Integer.parseInt(scanner.next());
                row.add(num);
            }
            arr.add(row);
        }

        System.out.println(hourglassSum(arr));
         */

        int start = 0;
        int end = start + 3;
        int lines = 0;
        int cycles = 0;
        int sum = 0;
        int result = Integer.MIN_VALUE;

        for (int i = 0; i < matrix.length; i++) {
            if (end == 7) {
                break;
            }
            lines++;
            for (int j = start; j < end; j++) {
                if (lines == 2) {
                    sum += matrix[i][j + 1];
                    break;
                }
                sum += matrix[i][j];
            }
            if (lines == 3) {
                lines = 0;
                i -= 2;
                cycles++;
                if (sum > result) {
                    result = sum;
                }
                sum = 0;
            }

            if (cycles == 4) {
                i = -1;
                start += 1;
                end++;
                cycles = 0;
            }
        }

        System.out.println(result);

    }

    public static int hourglassSum(List<List<Integer>> arr) {

        int start = 0;
        int end = start + 3;
        int lines = 0;
        int cycles = 0;
        int sum = 0;
        int result = Integer.MIN_VALUE;

        for (int i = 0; i < arr.size(); i++) {
            if (end == 6) {
                break;
            }
            lines++;
            for (int j = start; j < end; j++) {
                if (lines == 2) {
                    sum += arr.get(i).get(j + 1);
                    break;
                }
                sum += arr.get(i).get(j);
            }
            if (lines == 3) {
                lines = 0;
                i -= 2;
                cycles++;
                if (sum > result) {
                    result = sum;
                }
                sum = 0;
            }

            if (cycles == 4) {
                i = -1;
                start += 1;
                end++;
                cycles = 0;
            }
        }

        return result;
    }
}
