package Arrays.minimumSwaps;

import java.util.Arrays;
import java.util.Scanner;

public class MinimumSwaps {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = Integer.parseInt(scanner.nextLine());
        int[] arr = Arrays.stream(scanner.nextLine().split(" "))
                .mapToInt(Integer::parseInt).toArray();
        int size = arr.length;

        int index = 0;
        int swaps = 0;
        int temp = 0;

        while (index < size - 1) {

            if (arr[index] != index + 1) {
                temp = arr[index];
                int index2 = arr[index] - 1;
                arr[index] = arr[index2];
                arr[temp - 1] = temp;
                swaps++;
            } else {
                index++;
            }
        }
        System.out.println(swaps);

    }
}
