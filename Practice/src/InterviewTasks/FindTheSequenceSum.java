package InterviewTasks;

import java.util.Scanner;

public class FindTheSequenceSum {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int i = Integer.parseInt(scanner.nextLine());
        int j = Integer.parseInt(scanner.nextLine());
        int k = Integer.parseInt(scanner.nextLine());

        int num = i;
        int num1 = j;

        int sum = 0;
        int sumToAdd = 0;
        while (i < j) {
            sum += (num + sumToAdd);
            sumToAdd++;
            i++;
        }
        int newSumToAdd = 0;
        while(j > k) {
            sum += (num1 - newSumToAdd);
            newSumToAdd++;
            j--;
        }

        System.out.println(sum + k);

    }
}
