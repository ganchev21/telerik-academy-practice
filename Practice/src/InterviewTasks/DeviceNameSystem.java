package InterviewTasks;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class DeviceNameSystem {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        List<String> deviceNames = new ArrayList<>();

        int n = Integer.parseInt(scanner.nextLine());

        for (int i = 0; i < n; i++) {
            int counter = 0;
            String word = scanner.nextLine();
            checkWord(deviceNames, word, counter);
        }

        for (int i = 0; i < deviceNames.size(); i++) {
            System.out.println(deviceNames.get(i));
        }
    }

    public static void checkWord(List<String> list, String word, int counter) {
        if (list.contains(word)) {
            counter++;
            char c = word.charAt(word.length() - 1);
            if (Character.isDigit(c)) {
                String newWord = word.replaceAll("\\d", "");
                checkWord(list, newWord + counter, counter);
            } else {
                checkWord(list, word + counter, counter);
            }
        } else {
            list.add(word);
        }
    }
}
