package MockExam.titleSearch;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class TitleSearch {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String word = scanner.nextLine();
        int n = Integer.parseInt(scanner.nextLine());
        StringBuilder sb = new StringBuilder(word);


        for (int i = 0; i < n; i++) {
            String wordToCheck = scanner.nextLine();

            char[] arr = wordToCheck.toCharArray();
            int lastIndex = 0;
            boolean hasCommonChar = false;
            int arrIndex = 0;
            List<Integer> indexes = new ArrayList<>();

            for (int k = 0; k < word.length(); k++) {
                if (word.charAt(k) == arr[arrIndex]) {
                    hasCommonChar = true;
                    if (lastIndex > k) {
                        System.out.println("No such title found!");
                        break;
                    } else {
                        lastIndex = k;
                        arrIndex++;
                        indexes.add(k);
                        if (arrIndex == arr.length) {
                            break;
                        }
                    }
                }
            }

            if (arrIndex < arr.length || !hasCommonChar) {
                System.out.println("No such title found!");
                continue;
            }

            for (int j = 0; j < indexes.size(); j++) {
                sb.deleteCharAt(indexes.get(j) - j);
            }
            System.out.println(sb);
            word = sb.toString();

        }

    }
}
