package MockExam.militaryTanks;

import java.util.Scanner;

public class MilitaryTanks {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String input = scanner.nextLine();

        int x = 0;
        int y = 0;

        char[] array = input.toCharArray();

        for (int i = 0; i < array.length; i++) {
            if (array[i] == 'L') {
                x--;
            } else if (array[i] == 'R') {
                x++;
            } else if (array[i] == 'U') {
                y++;
            } else {
                y--;
            }
        }

        //(0, 0)

        System.out.printf("(%d, %d)", x, y);

    }
}
