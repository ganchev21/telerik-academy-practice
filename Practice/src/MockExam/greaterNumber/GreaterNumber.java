package MockExam.greaterNumber;

import java.util.Arrays;
import java.util.Scanner;

public class GreaterNumber {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int[] arrayToCompare = Arrays.stream(scanner.nextLine().split(","))
                .mapToInt(Integer::parseInt).toArray();

        int[] secondArr = Arrays.stream(scanner.nextLine().split(","))
                .mapToInt(Integer::parseInt).toArray();

        int[] result = new int[arrayToCompare.length];


        int index = 0;

        for (int i = 0; i < secondArr.length; i++) {
            boolean numberFound = false;
            if (secondArr[i] == arrayToCompare[index]) {
                for (int j = i; j < secondArr.length; j++) {
                    if (secondArr[j] > arrayToCompare[index]) {
                        numberFound = true;
                        result[index] = secondArr[j];
                        index++;
                        i = -1;
                        break;
                    }
                }
                if (!numberFound) {
                    result[index] = -1;
                    index++;
                    i = -1;
                }
            }
            if (index == arrayToCompare.length) {
                break;
            }
        }

        System.out.println(String.join(",", Arrays.toString(result))
                .replace('[', ' ')
                .replace(']', ' ')
                .replaceAll("\\s+", "")
                .trim());

    }
}
