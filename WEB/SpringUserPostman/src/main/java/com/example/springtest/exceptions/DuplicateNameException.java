package com.example.springtest.exceptions;

public class DuplicateNameException extends RuntimeException{
    public DuplicateNameException(String type, String attribute, String value) {
        super(String.format("%s with %s %s already exist!", type, attribute, value));
    }
}
