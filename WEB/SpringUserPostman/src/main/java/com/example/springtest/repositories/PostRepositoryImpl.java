package com.example.springtest.repositories;

import com.example.springtest.exceptions.EntityNotFoundException;
import com.example.springtest.models.Post;
import com.example.springtest.models.User;
import com.example.springtest.repositories.contracts.PostRepository;
import com.example.springtest.repositories.contracts.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class PostRepositoryImpl implements PostRepository {

    private final List<Post> posts;
    private List<User> users;
    private UserRepository repository;


    @Autowired
    public PostRepositoryImpl(UserRepository repository) {
        this.posts = new ArrayList<>();
        this.repository = repository;
       List<User> users = repository.getAllUsers();
//       Comment comment = new Comment("nice nice nice", )
//       comments.add("nice nice nice");
//        posts.add(new Post(1, "Is it nice?", "It's summer", 0, users.get(0)));
//        posts.add(new Post(2, "Car forum", "This is volvo", 0, users.get(1)));
//        posts.add(new Post(1,"What a beautiful car", "random content, it should be 32 characters long",
//                new ArrayList<>(Collections.singleton("yea it is really amazing")), 5, users.get(0)));
//        posts.add(new Post(2,"It is a wonderful day", "i am surprised that it is summer and it is snowy",
//                new ArrayList<>(Collections.singleton("what a weather wowww")), 21, users.get(1)));
//        posts.add(new Post(3,"Eat healthy, do not smoke", "cigarettes can damage your health, DANGER",
//                new ArrayList<>(Collections.singleton("i will smoke anyway, idc")), 99, users.get(2)));
////        posts.add(new Post(4, "Neznam kakvo da pisha", "opisanie na neznam kakvo da pisha, neka da e po dulgichko",
////                new ArrayList<>(Collections.singleton("naistina ne znam")), 90, admins.get(0)));
    }

    @Override
    public List<Post> getAll() {
        return new ArrayList<>(posts);
    }

    @Override
    public Post getPost(int id) {
        return posts.stream().filter(p -> p.getId() == id)
                .findFirst()
                .orElseThrow(() -> new EntityNotFoundException("Post", id));
    }

    @Override
    public int getPostLikes(Post post) {
        return post.getLikes();
    }

    public List<Post> getUserPosts(List<Post> posts,User user){
        return posts.stream()
                .filter(p -> p.getCreatedBy().getUsername().equals(user.getUsername()))
                .collect(Collectors.toList());

    }

    @Override
    public void create(Post post) {
        int currentId = 0;
        if (!posts.isEmpty()) {
            currentId = posts.size();
        }
        post.setId(++currentId);
        posts.add(post);
    }

    @Override
    public void updatePost(Post post) {
        Post postToUpgrade = getPostById(post.getId());

        postToUpgrade.setTitle(post.getTitle());
        postToUpgrade.setContent(post.getContent());
    }

    @Override
    public void delete(int id) {
        posts.remove(getPostById(id));
    }

    @Override
    public Post getPostById(int id) {
        return posts.stream().filter(p -> p.getId() == id)
                .findFirst()
                .orElseThrow(() -> new EntityNotFoundException("Post", id));
    }

    @Override
    public Post getPostByTitle(String title) {
        return posts.stream().filter(p -> p.getTitle().equals(title))
                .findFirst()
                .orElseThrow(() -> new EntityNotFoundException("Post", "title", title));
    }

}
