package com.example.springtest.helpers;

import com.example.springtest.models.Comment;
import com.example.springtest.models.CommentDto;
import com.example.springtest.services.contracts.PostService;
import com.example.springtest.services.contracts.UserService;
import org.springframework.stereotype.Component;

@Component
public class CommentMapper {

    private UserService service;
    private PostService postService;

    public CommentMapper(UserService service, PostService postService) {
        this.service = service;
        this.postService = postService;
    }

    public Comment dtoToObject (Comment comment, CommentDto commentDto) {
        comment.setContent(commentDto.getContent());
        comment.setUser(service.getUser(commentDto.getUserId()));
        comment.setPost(postService.getPost(commentDto.getPostId()));
        return comment;
    }
}
