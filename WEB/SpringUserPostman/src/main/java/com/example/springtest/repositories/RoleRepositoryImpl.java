package com.example.springtest.repositories;

import com.example.springtest.exceptions.EntityNotFoundException;
import com.example.springtest.models.Role;
import com.example.springtest.repositories.contracts.RoleRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class RoleRepositoryImpl implements RoleRepository {

    private List<Role> roles;

    public RoleRepositoryImpl() {
        this.roles = new ArrayList<>();

        roles.add(new Role(1, "admin"));
        roles.add(new Role(2, "user"));
    }


    @Override
    public List<Role> getAll() {
        return new ArrayList<>(roles);
    }

    @Override
    public Role getRoleById(int id) {
        return roles.stream().filter(r -> r.getId() == id)
                .findFirst()
                .orElseThrow(() -> new EntityNotFoundException("Role", id));
    }
}
