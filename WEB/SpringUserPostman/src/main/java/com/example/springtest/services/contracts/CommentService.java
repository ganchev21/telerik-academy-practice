package com.example.springtest.services.contracts;

import com.example.springtest.models.Comment;
import com.example.springtest.models.Post;
import com.example.springtest.models.User;

import java.util.List;

public interface CommentService {

    List<Comment> getAll();
    void create (Comment comment, User user);
    void update (Comment comment, User user);

//    void delete (Comment comment, User user);

    List<Comment> getUserComments (User user, int id);

    List<Comment> getPostComments (List<Comment> comments, int id);

}
