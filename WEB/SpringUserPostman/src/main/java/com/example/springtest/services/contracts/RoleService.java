package com.example.springtest.services.contracts;

import com.example.springtest.models.Role;

import java.util.List;

public interface RoleService {
    List<Role> getAll();

    Role getRoleById(int id);
}
