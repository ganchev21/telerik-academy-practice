package com.example.springtest.controllers;

import com.example.springtest.exceptions.DuplicateNameException;
import com.example.springtest.exceptions.EntityNotFoundException;
import com.example.springtest.exceptions.UniqueEmailException;
import com.example.springtest.helpers.AuthenticationHelper;
import com.example.springtest.helpers.UserMapper;
import com.example.springtest.models.User;
import com.example.springtest.models.UserDto;
import com.example.springtest.services.contracts.UserService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private final UserService service;
    private final UserMapper userMapper;
    private final AuthenticationHelper helper;

    @Autowired
    public UserController(UserService service, UserMapper userMapper, AuthenticationHelper helper) {
        this.service = service;
        this.userMapper = userMapper;
        this.helper = helper;
    }

    @GetMapping()
    public List<User> getAll(
            @RequestParam(required = false) String firstName,
            @RequestParam(required = false) String lastName,
            @RequestParam(required = false) String email,
            @RequestParam(required = false) String username,
            @RequestParam(required = false) Integer addressId,
            @RequestParam(required = false) String sortBy,
            @RequestParam(required = false) String sortOrder) {
        return service.getAll(firstName, lastName, email, username, addressId, sortBy, sortOrder);
    }

    @GetMapping("/{id}")
    public User getUser(@PathVariable int id) {
        try {
            return service.getUser(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    e.getMessage());
        }
    }


    @PostMapping
    public User create(@Valid @RequestBody UserDto userDto) {
        try {
            User user = userMapper.dtoToObject(new User(), userDto);
            service.create(user);
            return user;
        } catch (UniqueEmailException e) {
            throw new ResponseStatusException(HttpStatus.FOUND, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateNameException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public void upgrade( @PathVariable int id, @Valid @RequestBody UserDto userDto) {
        try {
            User user = userMapper.dtoToObject(id, userDto);
            service.updateUser(user);
        } catch (UniqueEmailException e) {
            throw new ResponseStatusException(HttpStatus.FOUND, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateNameException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = helper.tryGetUser(headers);
            service.delete(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }


}
