package com.example.springtest.exceptions;

public class NoPhoneProvidedException extends RuntimeException{
    public NoPhoneProvidedException(String name) {
        super(String.format("%s has not provided a phone number!", name));
    }
}
