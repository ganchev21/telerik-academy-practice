package com.example.springtest.exceptions;

public class UniqueEmailException extends RuntimeException{
    public UniqueEmailException() {
        super("Email must be unique!");
    }
}
