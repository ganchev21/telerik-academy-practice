package com.example.springtest.services;

import com.example.springtest.exceptions.AuthorizationException;
import com.example.springtest.exceptions.EntityNotFoundException;
import com.example.springtest.models.Comment;
import com.example.springtest.models.Post;
import com.example.springtest.models.User;
import com.example.springtest.repositories.contracts.CommentRepository;
import com.example.springtest.repositories.contracts.PostRepository;
import com.example.springtest.repositories.contracts.UserRepository;
import com.example.springtest.services.contracts.CommentService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentServiceImpl implements CommentService {

    private static final String MODIFY_COMMENT_ERROR_MESSAGE =
            "Only admin or user who created the comment can modify it!";
    private final CommentRepository repository;
    private final UserRepository userRepository;
    private final PostRepository postRepository;

    public CommentServiceImpl(CommentRepository repository, UserRepository userRepository, PostRepository postRepository) {
        this.repository = repository;
        this.userRepository = userRepository;
        this.postRepository = postRepository;
    }


    @Override
    public List<Comment> getAll() {
        return repository.getAll();
    }

    @Override
    public void create(Comment comment, User user) {
        Post post = postRepository.getPost(comment.getPost().getId());
        User user1 = userRepository.getUser(comment.getUser().getId());
        checkPermission(user.getId(), user);
        repository.create(comment);


    }

    @Override
    public void update(Comment comment, User user) {
        boolean isExist = true;
        checkPermission(comment.getUser().getId(), user);
        try {
            Comment comment1 = repository.getCommentById(comment.getId());
        } catch (EntityNotFoundException e) {
            isExist = false;
        }

        if (isExist) {
            repository.update(comment);
        }
    }

//    @Override
//    public void delete(Comment comment) {
//
//    }

    @Override
    public List<Comment> getUserComments(User user, int id) {
        User userToCheck = userRepository.getUser(id);
        return repository.getUserComments(user);
    }

    @Override
    public List<Comment> getPostComments(List<Comment> comments, int id) {
        Post postToCheck = postRepository.getPostById(id);
        return repository.getPostComments(comments, id);
    }

    public void checkPermission (int id, User user) {
        User userToCheck = userRepository.getUser(id);
        if (!(userToCheck.getRole().isAdmin() || userToCheck.getUsername().equals(user.getUsername()))) {
            throw new AuthorizationException(MODIFY_COMMENT_ERROR_MESSAGE);
        }
    }
}
