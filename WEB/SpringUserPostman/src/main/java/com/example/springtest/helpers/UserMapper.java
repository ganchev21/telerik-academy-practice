package com.example.springtest.helpers;

import com.example.springtest.models.User;
import com.example.springtest.models.UserDto;
import com.example.springtest.services.contracts.PostService;
import com.example.springtest.services.contracts.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {
    private PostService postService;
    private RoleService roleService;

    @Autowired
    public UserMapper(PostService postService, RoleService roleService) {
        this.postService = postService;
        this.roleService = roleService;
    }

    public User dtoToObject(int id, UserDto userDto) {
        User user = new User();
        user.setId(id);
        return dtoToObject(user, userDto);
    }

    public User dtoToObject(User user, UserDto userDto) {
        user.setFirstname(userDto.getFirstname());
        user.setLastName(userDto.getLastName());
        user.setUsername(userDto.getUsername());
        user.setPassword(userDto.getPassword());
        user.setEmail(userDto.getEmail());
        user.setRole(roleService.getRoleById(userDto.getRoleId()));

        if (user.getRole().isAdmin()) {
            user.setTelephone(userDto.getTelephone());
        }


        return user;
    }
}
