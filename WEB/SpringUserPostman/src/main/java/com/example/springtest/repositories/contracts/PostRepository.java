package com.example.springtest.repositories.contracts;

import com.example.springtest.models.Comment;
import com.example.springtest.models.Post;
import com.example.springtest.models.User;

import java.util.List;

public interface PostRepository {

    List<Post> getAll();
    Post getPost(int id);
    int getPostLikes (Post post);
    void create(Post post);
    void updatePost (Post post);
    void delete (int id);
    Post getPostById (int id);
    Post getPostByTitle (String title);
    List<Post> getUserPosts(List<Post> post, User user);

}
