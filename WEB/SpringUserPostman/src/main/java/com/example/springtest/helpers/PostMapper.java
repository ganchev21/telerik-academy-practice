package com.example.springtest.helpers;

import com.example.springtest.models.Post;
import com.example.springtest.models.PostDtoIn;
import com.example.springtest.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PostMapper {

    private UserService service;

    @Autowired
    public PostMapper(UserService service) {
        this.service = service;
    }

    public Post dtoToObject (int id, PostDtoIn postDtoIn) {
        Post post = new Post();
        post.setId(id);
        return dtoToObject(post, postDtoIn);
    }

    public Post dtoToObject (Post post, PostDtoIn postDtoIn) {
        post.setTitle(postDtoIn.getTitle());
        post.setContent(postDtoIn.getContent());
        post.setLikes(postDtoIn.getLikes());
        post.setCreatedBy(service.getUser(postDtoIn.getUserId()));
        return post;
    }
}
