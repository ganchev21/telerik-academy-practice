package com.example.springtest.services.contracts;

import com.example.springtest.models.User;

import java.util.List;

public interface UserService {
    List<User> getAll(String firstName, String lastName, String email,
                      String username, Integer addressId, String sortBy, String sortOrder);

    User getUser(int id);
    User getUserByUsername(String username);

    void create(User user);

    void updateUser(User user);

    void delete(int id, User user);
}
