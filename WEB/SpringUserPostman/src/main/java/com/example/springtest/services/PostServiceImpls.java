package com.example.springtest.services;

import com.example.springtest.exceptions.AuthorizationException;
import com.example.springtest.exceptions.DuplicateNameException;
import com.example.springtest.exceptions.EntityNotFoundException;
import com.example.springtest.models.Post;
import com.example.springtest.models.User;
import com.example.springtest.repositories.contracts.PostRepository;
import com.example.springtest.services.contracts.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PostServiceImpls implements PostService {
    private static final String MODIFY_POST_ERROR_MESSAGE = "Only admin or user who created the post can modify it!";
    private final PostRepository repository;

    @Autowired
    public PostServiceImpls(PostRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Post> getAll() {
        return repository.getAll();
    }

    @Override
    public Post getPost(int id) {
        return repository.getPost(id);
    }

    @Override
    public String getPostTitle(int id) {
        Post postToCheck = repository.getPostById(id);
        return postToCheck.getTitle();
    }

    @Override
    public String getPostContent(int id) {
        Post postToCheck = repository.getPostById(id);
        return postToCheck.getContent();
    }

    @Override
    public int getPostLikes(Post post) {
        return repository.getPostLikes(post);
    }

    public List<Post> getUserPosts(List<Post> posts,User user){
        return repository.getUserPosts(posts, user);

    }

    @Override
    public void create(Post post, User user) {
        boolean isExist = true;
        try {
            repository.getPostByTitle(post.getTitle());
        } catch (EntityNotFoundException e) {
            isExist = false;
        }
        if (isExist) {
            throw new DuplicateNameException("Post", "title", post.getTitle());
        }
        post.setCreatedBy(user);
        repository.create(post);


    }

    @Override
    public void updatePost(Post post, User user) {
        boolean isExist = true;
        checkPermission(post.getId(), user);
        try {
            Post post1 = repository.getPostById(post.getId());
            if (!post.getTitle().equals(post1.getTitle())) {
                isExist = false;
            }
        } catch (EntityNotFoundException e) {
            isExist = false;
        }
        if (isExist) {
            throw new DuplicateNameException("Post", "title", post.getTitle());
        }
        repository.updatePost(post);
    }
    @Override
    public void delete(int id, User user) {
        checkPermission(id, user);
        repository.delete(id);
    }

    public void checkPermission (int id, User user) {
        Post post = repository.getPost(id);
        if (!(user.getRole().isAdmin() || post.getCreatedBy().getUsername().equals(user.getUsername()))) {
            throw new AuthorizationException(MODIFY_POST_ERROR_MESSAGE);
        }
    }
}
