package com.example.springtest.services;

import com.example.springtest.models.Role;
import com.example.springtest.repositories.contracts.RoleRepository;
import com.example.springtest.services.contracts.RoleService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {

    private RoleRepository repository;

    public RoleServiceImpl(RoleRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Role> getAll() {
        return repository.getAll();
    }

    @Override
    public Role getRoleById(int id) {
        return repository.getRoleById(id);
    }
}
