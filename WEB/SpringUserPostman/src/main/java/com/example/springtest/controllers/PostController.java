package com.example.springtest.controllers;

import com.example.springtest.exceptions.AuthorizationException;
import com.example.springtest.exceptions.DuplicateNameException;
import com.example.springtest.exceptions.EntityNotFoundException;
import com.example.springtest.helpers.AuthenticationHelper;
import com.example.springtest.helpers.PostMapper;
import com.example.springtest.models.*;
import com.example.springtest.services.contracts.PostService;
import com.example.springtest.services.contracts.UserService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/posts")
public class PostController {
    private final PostService service;
    private final UserService userService;

    private final PostMapper postMapper;

    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public PostController(PostService service, UserService userService, PostMapper postMapper,
                          AuthenticationHelper authenticationHelper) {
        this.service = service;
        this.userService = userService;
        this.postMapper = postMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<Post> getAll() {
        return service.getAll();
    }

    @GetMapping("/{id}")
    public Post getPost(@PathVariable int id) {
        try {
            return service.getPost(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}/title")
    public String getPostTitle(@PathVariable int id) {
        try {
            return service.getPostTitle(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}/content")
    public String getPostContent (@PathVariable int id) {
        try {
            return service.getPostTitle(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}/likes")
    public int getPostLikes(@PathVariable int id, Post post) {
        return service.getPostLikes(post);
    }

    @GetMapping("/user/{id}")
    public List<Post> getUserPosts (@PathVariable int id) {
        User user = userService.getUser(id);
        return service.getUserPosts(service.getAll(), user);
    }

    @PostMapping
    public void create(@RequestHeader HttpHeaders headers, @Valid @RequestBody PostDtoIn postDtoIn) {
        try {
            Post post = postMapper.dtoToObject(new Post(), postDtoIn);
            User user = authenticationHelper.tryGetUser(headers);
            service.create(post, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateNameException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public void updatePost(@RequestHeader HttpHeaders headers, @PathVariable int id,
                           @Valid @RequestBody PostDtoIn postDtoIn) {
        try {
            Post post = postMapper.dtoToObject(id, postDtoIn);
            User user = authenticationHelper.tryGetUser(headers);
            service.updatePost(post, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateNameException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            service.delete(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
