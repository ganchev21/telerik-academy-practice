package com.example.springtest.repositories;

import com.example.springtest.exceptions.EntityNotFoundException;
import com.example.springtest.models.Role;
import com.example.springtest.models.User;
import com.example.springtest.repositories.contracts.RoleRepository;
import com.example.springtest.repositories.contracts.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class UserRepositoryImpl implements UserRepository {

    private final List<User> users;

    @Autowired
    public UserRepositoryImpl(RoleRepository roleRepository) {
        users = new ArrayList<>();
        List<Role> roles = roleRepository.getAll();

        users.add(new User(1, "Gancho", "Ivanov", "gacata@abv.bg",
                "gancho", "pass9090", roles.get(1)));
//        users.add(new User(2, "Mitko", "Petkov", "miteto@abv.bg",
//                "mitko", "pass123", roles.get(1)));
        //0 admin......1 user
//        users.add(new User(1, "Georgi", "Petkov", "bbbbb@abv.bg",
//                "gosheto", "pass123","0888455992",roles.get(0)));
//        users.add(new User(2, "Mitko", "Georgiev", "ccccc@abv.bg",
//                "mitaka", "pass234", roles.get(1)));
//        users.add(new User(3, "Ivan", "Aleksiev", "aaaaa@abv.bg",
//                "tochi95", "pass678", "0876554401",roles.get(0)));
//        users.add(new User(4, "Evlogi", "Kamenov", "ddddd@abv.bg",
//                "evcho930", "pass901", roles.get(1)));
    }

    public List<User> getAllUsers() {
        return new ArrayList<>(users);
    }

    @Override
    public List<User> getAll(String firstName, String lastName, String email,
                             String username, String sortBy, String sortOrder) {
        List<User> result = new ArrayList<>(users);
        result = filterByFirstName(result, firstName);
        result = filterByLastName(result, lastName);
        result = filterByEmail(result, email);
        result = filterByUsername(result, username);
        result = sortBy(result, sortBy);
        result = sortOrder(result, sortOrder);
        return result;
    }

    private List<User> filterByFirstName(List<User> result, String firstName) {
        if (!result.isEmpty() && firstName != null && !firstName.isEmpty()) {
            result = result.stream().filter(u -> u.getFirstname().toLowerCase().contains(firstName.toLowerCase()))
                    .collect(Collectors.toList());
        }
        return result;
    }

    private List<User> filterByLastName(List<User> result, String lastName) {
        if (!result.isEmpty() && lastName != null && !lastName.isEmpty()) {
            result = result.stream().filter(u -> u.getLastName().toLowerCase().contains(lastName.toLowerCase()))
                    .collect(Collectors.toList());
        }
        return result;
    }

    private List<User> filterByEmail(List<User> result, String email) {
        if (!result.isEmpty() && email != null && !email.isEmpty()) {
            result = result.stream().filter(u -> u.getEmail().toLowerCase().contains(email.toLowerCase()))
                    .collect(Collectors.toList());
        }
        return result;
    }

    private List<User> filterByUsername(List<User> result, String username) {
        if (!result.isEmpty() && username != null && !username.isEmpty()) {
            result = result.stream().filter(u -> u.getUsername().toLowerCase().contains(username.toLowerCase()))
                    .collect(Collectors.toList());
        }
        return result;
    }


    private List<User> sortBy(List<User> result, String sortBy) {
        if (!result.isEmpty() && sortBy != null && !sortBy.isEmpty()) {
            switch (sortBy.toLowerCase()) {
                case "firstname":
                    result.sort(Comparator.comparing(User::getFirstname));
                    break;
                case "lastname":
                    result.sort(Comparator.comparing(User::getLastName));
                    break;
                case "username":
                    result.sort(Comparator.comparing(User::getUsername));
                    break;
                case "email":
                    result.sort(Comparator.comparing(User::getEmail));
                    break;
            }
        }
        return result;
    }

    private List<User> sortOrder(List<User> result, String sortOrderDesc) {
        if (!result.isEmpty() && sortOrderDesc != null && !sortOrderDesc.isEmpty()) {
            if (sortOrderDesc.equals("desc")) {
                Collections.reverse(result);
            }
        }
        return result;
    }

    @Override
    public User getUser(int id) {
        for (User user : users) {
            if (user.getId() == id) {
                return user;
            }
        }
        throw new ResponseStatusException(
                HttpStatus.NOT_FOUND,
                String.format("User with ID %d was not found", id));
    }

    @Override
    public void create(User user) {
        int currentId = 0;
        if (!users.isEmpty()) {
            currentId = users.size();
        }
        user.setId(++currentId);
        users.add(user);
    }

    @Override
    public void updateUser(User user) {
        User userToUpgrade = getUserById(user.getId());

        userToUpgrade.setFirstname(user.getFirstname());
        userToUpgrade.setLastName(user.getLastName());
        userToUpgrade.setEmail(user.getEmail());
        userToUpgrade.setUsername(user.getUsername());
        userToUpgrade.setPassword(user.getPassword());
    }

    @Override
    public void delete(int id) {
        User userToRemove = getUserById(id);
        users.remove(userToRemove);
    }

    @Override
    public User getUserById(int id) {
        return users.stream().filter(u -> u.getId() == id)
                .findFirst()
                .orElseThrow(() -> new EntityNotFoundException("User", id));
    }

    @Override
    public User getUserByName(String name) {
        return users.stream().filter(u -> u.getUsername().equals(name))
                .findFirst()
                .orElseThrow(() -> new EntityNotFoundException("User", "name", name));
    }

    @Override
    public boolean isUniqueEmail(String email) {
        return users.stream().noneMatch(u -> u.getEmail().equals(email));
    }

}
