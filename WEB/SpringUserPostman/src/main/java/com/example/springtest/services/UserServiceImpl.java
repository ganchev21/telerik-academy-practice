package com.example.springtest.services;

import com.example.springtest.exceptions.AuthorizationException;
import com.example.springtest.exceptions.DuplicateNameException;
import com.example.springtest.exceptions.EntityNotFoundException;
import com.example.springtest.exceptions.UniqueEmailException;
import com.example.springtest.models.User;
import com.example.springtest.repositories.contracts.UserRepository;
import com.example.springtest.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class UserServiceImpl implements UserService {
    private static final String DELETE_USER_ERROR_MESSAGE = "Only admin can delete users!";


    private final UserRepository repository;

    @Autowired
    public UserServiceImpl(UserRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<User> getAll(String firstName, String lastName, String email,
                             String username, Integer addressId, String sortBy, String sortOrder) {
        return repository.getAll(firstName, lastName, email, username, sortBy, sortOrder);
    }

    @Override
    public User getUser(int id) {
        return repository.getUser(id);
    }

    @Override
    public User getUserByUsername(String username) {
        return repository.getUserByName(username);
    }

    @Override
    public void create(User user) {
        boolean isExist = true;
        try {
            repository.getUserByName(user.getUsername());
        } catch (EntityNotFoundException e) {
            isExist = false;
        }
        if (isExist) {
            throw new DuplicateNameException("User", "name", user.getUsername());
        }
        if (!repository.isUniqueEmail(user.getEmail())) {
            throw new UniqueEmailException();
        }
        repository.create(user);
    }

    @Override
    public void updateUser(User user) {
        boolean isExist = true;
        try {
            repository.getUserByName(user.getUsername());

        } catch (EntityNotFoundException e) {
            isExist = false;
        }

        if (isExist) {
            throw new DuplicateNameException("User", "name", user.getUsername());
        }
        if (!repository.isUniqueEmail(user.getEmail())) {
            throw new UniqueEmailException();
        }
        repository.updateUser(user);
    }

    @Override
    public void delete(int id, User user) {
        checkPermission(id);
        repository.delete(id);

    }

    public void checkPermission(int id) {
        User user1 = repository.getUserById(id);
        if (user1.getRole().isAdmin()) {
            throw new AuthorizationException(DELETE_USER_ERROR_MESSAGE);
        }

    }
}
