package com.example.springtest.repositories;

import com.example.springtest.exceptions.EntityNotFoundException;
import com.example.springtest.models.Comment;
import com.example.springtest.models.User;
import com.example.springtest.repositories.contracts.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class CommentRepositoryImpl implements CommentRepository {


    private final List<Comment> comments;



    @Autowired
    public CommentRepositoryImpl() {
        this.comments = new ArrayList<>();

    }


    @Override
    public List<Comment> getAll() {
        return new ArrayList<>(comments);
    }

    @Override
    public void create(Comment comment) {
        int currentId = 0;
        if (!comments.isEmpty()) {
            currentId = comments.size();
        }
        comment.setId(++currentId);
        comments.add(comment);

    }

    @Override
    public void update(Comment comment) {
        Comment commentToUpgrade = getCommentById(comment.getId());

        commentToUpgrade.setContent(comment.getContent());


    }

    @Override
    public void delete(Comment comment) {

    }

    @Override
    public List<Comment> getUserComments(User user) {
        return comments.stream()
                .filter(c -> c.getUser().getUsername().equals(user.getUsername()))
                .collect(Collectors.toList());
    }

    @Override
    public List<Comment> getPostComments(List<Comment> comments, int id) {
        List<Comment> comments1 = new ArrayList<>();
        for (Comment comment : comments) {
            if (comment.getPost().getId() == id) {
                comments1.add(comment);
            }
        }
        return  comments1;
    }

    @Override
    public Comment getCommentById(int id) {
        return comments.stream().filter(c -> c.getId() == id)
                .findFirst()
                .orElseThrow(() -> new EntityNotFoundException("Comment", id));
    }
}
