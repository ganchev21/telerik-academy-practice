package com.example.springtest.controllers;

import com.example.springtest.exceptions.AuthorizationException;
import com.example.springtest.exceptions.EntityNotFoundException;
import com.example.springtest.helpers.AuthenticationHelper;
import com.example.springtest.helpers.CommentMapper;
import com.example.springtest.models.Comment;
import com.example.springtest.models.CommentDto;
import com.example.springtest.models.User;
import com.example.springtest.services.contracts.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/comments")
public class CommentController {

    private final CommentService commentService;
    private final AuthenticationHelper helper;
    private final CommentMapper commentMapper;


    @Autowired
    public CommentController(CommentService commentService, CommentMapper commentMapper, AuthenticationHelper helper) {
        this.commentService = commentService;
        this.commentMapper = commentMapper;
        this.helper = helper;
    }

    @GetMapping
    public List<Comment> getAll() {
        return commentService.getAll();
    }

    @GetMapping("/user/{id}")
    public List<Comment> getUserComments (@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = helper.tryGetUser(headers);
            return commentService.getUserComments(user, id);
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/post/{id}")
    public List<Comment> getPostComments (@PathVariable int id) {
            return commentService.getPostComments(commentService.getAll(),id);

    }

    @PostMapping()
    public void create (@RequestHeader HttpHeaders headers, @RequestBody CommentDto commentDto) {
        try {
            User user = helper.tryGetUser(headers);
            Comment comment = commentMapper.dtoToObject(new Comment(), commentDto);
            commentService.create(comment, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public void update (@RequestHeader HttpHeaders headers, @PathVariable int id,
                        @RequestBody CommentDto commentDto) {
        try {
            User user = helper.tryGetUser(headers);
            Comment comment = commentMapper.dtoToObject(new Comment(), commentDto);
            comment.setId(id);
            commentService.update(comment, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
