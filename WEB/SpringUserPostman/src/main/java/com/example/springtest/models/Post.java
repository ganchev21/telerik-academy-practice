package com.example.springtest.models;

import jakarta.validation.constraints.Positive;

import java.util.ArrayList;
import java.util.List;

public class Post {
    @Positive
    int id;

    private String title;

    private String content;
    private int likes;

    private User createdBy;

    public Post() {
    }

    public Post(int id, String title, String content,int likes, User user) {
        this.id = id;
        this.createdBy = user;
        this.title = title;
        this.content = content;
        this.likes = likes;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User user) {
        this.createdBy = user;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }
}
