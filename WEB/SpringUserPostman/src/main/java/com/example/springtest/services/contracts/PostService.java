package com.example.springtest.services.contracts;

import com.example.springtest.models.Post;
import com.example.springtest.models.User;

import java.util.List;

public interface PostService {
    List<Post> getAll();
    Post getPost(int id);
    String getPostTitle (int id);
    String getPostContent (int id);
    List<Post> getUserPosts (List<Post> posts, User user);
    int getPostLikes (Post post);
    void create(Post post, User user);
    void updatePost (Post post, User user);
    void delete (int id, User user);
}
