package com.example.springtest.repositories.contracts;

import com.example.springtest.models.Comment;
import com.example.springtest.models.User;

import java.util.List;

public interface CommentRepository {

    List<Comment> getAll();
    void create (Comment comment);
    void update (Comment comment);

    void delete (Comment comment);

    List<Comment> getUserComments (User user);

    List<Comment> getPostComments (List<Comment> comments, int id);

    public Comment getCommentById(int id);

}
