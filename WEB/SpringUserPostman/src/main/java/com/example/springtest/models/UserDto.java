package com.example.springtest.models;

import jakarta.validation.constraints.*;

import java.util.Optional;

public class UserDto {

    @NotNull
    @Size(min = 4, max = 32)
    private String firstname;
    @NotNull
    @Size(min = 4, max = 32)
    private String lastName;
    @NotNull
    private String email;
    @NotNull
    @Size(min = 4, max = 32)
    private String username;
    @NotNull
    @Size(min = 4, max = 32)
    private String password;
    private Optional<String> telephone;
//    @Positive
    private int postId;
    @PositiveOrZero
    private int roleId;

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public Optional<String> getTelephone() {
        if (telephone == null) {
            setTelephone("No phone provided!");
        }
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone.describeConstable();
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }
}
