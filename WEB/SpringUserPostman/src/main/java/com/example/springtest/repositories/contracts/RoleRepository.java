package com.example.springtest.repositories.contracts;

import com.example.springtest.models.Role;

import java.util.List;

public interface RoleRepository {

    List<Role> getAll();

    Role getRoleById(int id);
}
