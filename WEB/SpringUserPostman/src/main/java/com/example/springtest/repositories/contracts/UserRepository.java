package com.example.springtest.repositories.contracts;

import com.example.springtest.models.User;

import java.util.List;

public interface UserRepository {
    List<User> getAllUsers ();
    List<User> getAll(String firstName, String lastName, String email,
                      String username, String sortBy, String sortOrder);

    User getUser(int id);

    void create(User user);

    void updateUser(User user);

    void delete(int id);

    User getUserById(int id);

    User getUserByName(String name);
    boolean isUniqueEmail (String email);
}
