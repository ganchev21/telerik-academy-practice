package com.telerikacademy.dsa;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FilesUtils {
    public static void traverseDirectories(String path) {
        File newFile = new File(path);
        int count = 0;
        File[] fileArray = newFile.listFiles();
        System.out.println(newFile.getName() + ":");
        assert fileArray != null;
        for (File file : fileArray) {
            StringBuilder space = new StringBuilder();
            count = 1;
            space.append(" ".repeat(count));
            System.out.printf("%s%s:%n", space, file.getName());
            File[] arr = file.listFiles();
            count++;
            space.append(" ".repeat(count));
            assert arr != null;
            for (File newFiles : arr) {
                System.out.printf("%s%s%n", space, newFiles.getName());
                String[] fileArr = newFiles.list();
                if (fileArr != null) {
                    count++;
                    space.append(" ".repeat(Math.max(0, count)));
                    for (String s : fileArr) {
                        System.out.printf("%s%s%n", space, s);
                    }
                }
            }

        }
    }

    public static List<String> findFiles(String path, String extension) {
        List<String> fileList = new ArrayList<>();
        File newFile = new File(path);
        File[] fileArray = newFile.listFiles();
        assert fileArray != null;
        for (File file : fileArray) {
            File[] arr = file.listFiles();

            assert arr != null;
            for (File newFiles : arr) {
                String fileString = String.valueOf(newFiles);
                int index = fileString.lastIndexOf(".") + 1;
                String strToCheck = fileString.substring(index);
                if (strToCheck.equals(extension)) {
                    fileList.add(newFiles.getName());
                }
                String[] fileArr = newFiles.list();
                if (fileArr != null) {
                    for (String s : fileArr) {
                        int newIndex = s.lastIndexOf(".") + 1;
                        String stringToCheck = s.substring(newIndex);
                        if (stringToCheck.equals(extension)) {
                            fileList.add(s);
                        }
                    }
                }
            }
        }

        return fileList;
    }

    public static boolean fileExists(String path, String fileName) {
        File newFile = new File(path);
        File[] fileArray = newFile.listFiles();
        assert fileArray != null;
        for (File file : fileArray) {
            File[] arr = file.listFiles();

            assert arr != null;
            for (File newFiles : arr) {
                String fileString = newFiles.getName();
                if (fileString.equals(fileName)) {
                    return true;
                }
                String[] fileArr = newFiles.list();
                if (fileArr != null) {
                    for (String s : fileArr) {
                        if (s.equals(fileName)) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    public static Map<String, Integer> getDirectoryStats(String path) {
        Map<String, Integer> map = new HashMap<>();
        File newFile = new File(path);
        File[] fileArray = newFile.listFiles();
        assert fileArray != null;
        for (File file : fileArray) {
            File[] arr = file.listFiles();

            assert arr != null;
            for (File newFiles : arr) {

                String fileString = newFiles.getName();
                int index = fileString.lastIndexOf(".") + 1;
                String strToCheck = fileString.substring(index);
                if (map.containsKey(strToCheck) && newFiles.isFile()) {
                    map.put(strToCheck, map.get(strToCheck) + 1);
                } else if (!map.containsKey(strToCheck) && newFiles.isFile()) {
                    map.put(strToCheck, 1);
                }

                String[] fileArr = newFiles.list();
                if (fileArr != null) {
                    for (String newFileString : fileArr) {
                        int newIndex = newFileString.lastIndexOf(".") + 1;
                        String stringToCheck = newFileString.substring(newIndex);
                        if (map.containsKey(stringToCheck)) {
                            map.put(stringToCheck, map.get(stringToCheck) + 1);
                        } else {
                            map.put(stringToCheck, 1);
                        }
                    }
                }
            }
        }
        return map;
    }
}
