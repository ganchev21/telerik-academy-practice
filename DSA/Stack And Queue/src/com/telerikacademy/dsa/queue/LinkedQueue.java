package com.telerikacademy.dsa.queue;

import com.telerikacademy.dsa.Node;

import java.util.NoSuchElementException;

public class LinkedQueue<E> implements Queue<E> {
    private Node<E> head, tail;
    private int size;

    public LinkedQueue() {
        this.head = null;
        this.tail = null;
        this.size = 0;
    }

    @Override
    public void enqueue(E element) {
        //add
        Node<E> newNode = new Node<>();
        if (isEmpty()) {
            newNode.data = element;
            newNode.next = null;
            head = newNode;
            tail = newNode;
            size++;
        } else {
            newNode.data = element;
            tail.next = newNode;
            tail = newNode;
            size++;
        }
    }

    @Override
    public E dequeue() {
        //remove
        if (isEmpty()) {
            throw new NoSuchElementException("Cannot remove, no items are found!");
        }
        E obj = head.data;
        head = head.next;
        size--;
        return obj;
    }

    @Override
    public E peek() {
        //get value of the first element
        if (isEmpty()) {
            throw new NoSuchElementException("No items!");
        }
        return head.data;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }
}
