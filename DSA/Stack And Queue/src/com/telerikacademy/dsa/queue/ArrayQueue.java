package com.telerikacademy.dsa.queue;

import java.util.Arrays;
import java.util.NoSuchElementException;

public class ArrayQueue<E> implements Queue<E> {
    public static int INITIAL_CAPACITY = 4;
    private E[] items;
    private int head, tail, size;


    public ArrayQueue() {
        this.items = (E[]) new Object[INITIAL_CAPACITY];
        this.head = 0;
        this.tail = 0;
        this.size = 0;
    }

    @Override
    public void enqueue(E element) {
        //add
        if (items.length == size) {
            this.items = Arrays.copyOf(this.items, this.items.length * 2);
            items[size] = element;
            size++;
            tail = size - 1;
        } else {
            items[size] = element;
            size++;
            tail = size - 1;
        }
    }

    @Override
    public E dequeue() {
        //remove
        if (isEmpty()) {
            throw new NoSuchElementException("Cannot remove, no items are found!");
        }
        E obj = items[head];
        items[head] = null;
        for (int i = 0; i < size - 1; i++) {
            items[i] = items[i + 1];
            if (i == size - 2) {
                items[size - 1] = null;
            }
            size--;
        }
        return obj;
    }

    @Override
    public E peek() {
        //get value of the first element
        if (isEmpty()) {
            throw new NoSuchElementException("No items!");
        }
        return this.items[head];
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

}
