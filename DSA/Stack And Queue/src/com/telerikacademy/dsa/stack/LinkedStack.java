package com.telerikacademy.dsa.stack;

import com.telerikacademy.dsa.Node;

import java.util.LinkedList;
import java.util.NoSuchElementException;

public class LinkedStack<E> implements Stack<E> {
    private Node<E> top;
    private int size;

    public LinkedStack() {
        this.top = null;
        this.size = 0;
    }

    @Override
    public void push(E element) {
        //add
        Node<E> newNode = new Node<>();
        if (isEmpty()) {
            newNode.data = element;
            top = newNode;
            newNode.next = null;
            size++;
        } else {
            newNode.data = element;
            newNode.next = top;
            top = newNode;
            size++;
        }
    }

    @Override
    public E pop() {
        //remove
        if (isEmpty()) {
            throw new NoSuchElementException("Cannot remove, no items are found!");
        }
        E obj = top.data;
        top = top.next;
        size--;
        return obj;

        //throw new UnsupportedOperationException();
    }

    @Override
    public E peek() {
        //get value of the first element
        if (isEmpty()) {
            throw new NoSuchElementException("No items!");
        }
        return top.data;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }
}
