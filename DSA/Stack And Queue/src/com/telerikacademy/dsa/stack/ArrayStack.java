package com.telerikacademy.dsa.stack;

import java.util.Arrays;
import java.util.NoSuchElementException;

public class ArrayStack<E> implements Stack<E> {

    public static int INITIAL_CAPACITY = 4;
    private E[] items;
    private int top;

    public ArrayStack() {
        this.items = (E[])new Object[INITIAL_CAPACITY];
        this.top = 0;
    }

    @Override
    public void push(E element) {
        //add
        if (items.length == top) {
            this.items = Arrays.copyOf(this.items, this.items.length * 2);
            items[top] = element;
            top++;
        } else {
            items[top] = element;
            top++;
        }


    }

    @Override
    public E pop() {
        //remove
        if (isEmpty()) {
            throw new NoSuchElementException("Cannot remove, no items are found!");
        }
        E obj = items[top - 1];
        items[items.length - 1] = null;
        top--;
        return obj;
    }

    @Override
    public E peek() {
        //get value of the first element
        if (isEmpty()) {
            throw new NoSuchElementException("No items!");
        }
        return this.items[top - 1];
    }

    @Override
    public int size() {
        return top;
    }

    @Override
    public boolean isEmpty() {
        return top == 0;
    }

}
