package com.telerikacademy.dsa;


public class Main {

    public static void main(String[] args) {
        BinarySearchTree<Integer> testTree;
        testTree = new BinarySearchTreeImpl<>(50);
        testTree.insert(30);
        testTree.insert(20);
        testTree.insert(40);
        testTree.insert(70);
        testTree.insert(60);
        testTree.insert(80);
        testTree.insert(72);
        testTree.insert(71);

        System.out.println(testTree.inOrder());

//        System.out.println(testTree.getLeftTree().getRootValue());
//        System.out.println(testTree.getRightTree().getRootValue());


    }

}
