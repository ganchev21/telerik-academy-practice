package com.telerikacademy.dsa;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;


public class BinarySearchTreeImpl<E extends Comparable<E>> implements BinarySearchTree<E> {
    private final E value;
    private BinarySearchTreeImpl<E> left;
    private BinarySearchTreeImpl<E> right;

    public BinarySearchTreeImpl(E value) {
        this.value = value;
        left = null;
        right = null;

    }


    @Override
    public E getRootValue() {
        return value;
    }

    @Override
    public BinarySearchTree<E> getLeftTree() {
        return left;
    }

    @Override
    public BinarySearchTree<E> getRightTree() {
        return right;
    }

    @Override
    public void insert(E element) {
        if (element.compareTo(value) < 0) {
            if (left != null) {
                left.insert(element);
            } else {
                left = new BinarySearchTreeImpl<>(element);
            }
        } else if (element.compareTo(value) >= 0) {
            if (right != null) {
                right.insert(element);
            } else {
                right = new BinarySearchTreeImpl<>(element);
            }
        }
    }

    @Override
    public boolean search(E element) {
        if (element.equals(this.value)) {
            return true;
        }
        boolean isExist = false;
        if (element.compareTo(this.value) > 0) {
            if (right != null) {
                isExist = searchMethod(right, element, false);
            }
        } else {
            if (left != null) {
                isExist = searchMethod(left, element, false);
            }
        }
        return isExist;
    }

    @Override
    public List<E> inOrder() {
        List<E> list = new ArrayList<>();
        inOrderAddMethod(list);
        return list;
    }

    @Override
    public List<E> postOrder() {
        List<E> list = new ArrayList<>();
        postOrderAddMethod(list);
        return list;
    }

    @Override
    public List<E> preOrder() {
        List<E> list = new ArrayList<>();
        preOrderAddMethod(list);
        return list;
    }

    @Override
    public List<E> bfs() {
        List<E> list = new ArrayList<>();
        Queue<BinarySearchTreeImpl<E>> queue = new ArrayDeque<>();
        queue.offer(this);
        while (!queue.isEmpty()) {
            BinarySearchTreeImpl<E> currentElement = queue.poll();
            list.add(currentElement.value);
            if (currentElement.left != null) {
                queue.offer(currentElement.left);
            }
            if (currentElement.right != null) {
                queue.offer(currentElement.right);
            }

        }
        return list;
    }

    @Override
    public int height() {
        if (left == null && right == null) {
            return 0;
        }
        if (right == null) {
            return left.height() + 1;
        }

        if (left == null) {
            return right.height() + 1;
        }
        return Math.max(left.height(), right.height()) + 1;
    }
    // Advanced task: implement remove method. To test, uncomment the commented tests in BinaryTreeImplTests
//    @Override
//    public boolean remove(E element) {
//    }

    public boolean searchMethod(BinarySearchTreeImpl<E> root, E element, boolean isExist) {
        if (root.value.equals(element)) {
            return true;
        }
        if (element.compareTo(root.value) < 0) {
            if (root.left != null) {
                isExist = searchMethod(root.left, element, isExist);
            }
        } else {
            if (root.right != null) {
                isExist = searchMethod(root.right, element, isExist);
            }
        }
        return isExist;
    }

    public void inOrderAddMethod(List<E> list) {
        if (left != null) {
            left.inOrderAddMethod(list);
        }
        list.add(value);
        if (right != null) {
            right.inOrderAddMethod(list);
        }
    }

    public void postOrderAddMethod(List<E> list) {
        if (left != null) {
            left.postOrderAddMethod(list);
        }
        if (right != null) {
            right.postOrderAddMethod(list);
        }
        list.add(value);
    }

    public void preOrderAddMethod(List<E> list) {
        list.add(value);
        if (left != null) {
            left.preOrderAddMethod(list);
        }
        if (right != null) {
            right.preOrderAddMethod(list);
        }
    }


}
