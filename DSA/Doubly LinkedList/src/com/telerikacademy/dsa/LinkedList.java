package com.telerikacademy.dsa;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class LinkedList<T> implements List<T> {
    private Node head;
    private Node tail;
    private int size = 0;

    public LinkedList() {
        head = tail = null;
    }

    public LinkedList(Iterable<T> iterable) {
        iterable.forEach(this::addLast);
    }

    @Override
    public void addFirst(T value) {
        Node newNode = new Node(value);
        if (isEmpty()) {
            tail = head = newNode;
        }
        newNode.next = head;
        head.prev = newNode;
        head = newNode;
        tail.next = null;
        size++;


    }

    @Override
    public void addLast(T value) {
        Node newNode = new Node(value);
        if (isEmpty()) {
            head = tail = newNode;
        }
        tail.next = newNode;
        newNode.prev = tail;
        tail = newNode;
        size++;
    }

    @Override
    public void add(int index, T value) {
        Node newNode = new Node(value);
        Node current = new Node();
        Node temp = new Node();
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        temp = head;
        for (int i = 0; i < index; i++) {
            current = temp;
            temp = temp.next;
        }
        if (index == size) {
            addLast(value);

        } else if (index == 0) {
            addFirst(value);

        } else {
            current.next = newNode;
            newNode.next = temp;
            newNode.prev = current;
            temp.prev = newNode;
            current.prev = null;
            size++;
        }

    }

    @Override
    public T getFirst() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        return head.value;
    }

    @Override
    public T getLast() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        return tail.value;
    }

    @Override
    public T get(int index) {
        if (index >= size) {
            throw new NoSuchElementException();
        }
        Node temp = new Node();
        temp = head;
        for (int i = 0; i < index; i++) {
            temp = temp.next;
        }
        return temp.value;
    }

    @Override
    public int indexOf(T value) {
        Node temp = new Node();
        temp = head;

        for (int i = 0; i < size; i++) {
            if (temp.value == value) {
                return i;
            }
            temp = temp.next;
        }
        return -1;
    }

    @Override
    public T removeFirst() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        T obj = head.value;
        head = head.next;
        if (head != null) {
            head.prev = null;
        }
        size--;
        return obj;
    }

    @Override
    public T removeLast() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        T obj = tail.value;
        tail = tail.prev;
        tail.next = null;
        size--;
        return obj;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public Iterator<T> iterator() {
        Iterator<T> it = new Iterator<T>() {

            private int currentIndex = 0;
            private Node node = head;


            @Override
            public boolean hasNext() {
                return node != null;
            }

            @Override
            public T next() {
                T data = node.value;
                node = node.next;
                return data;
            }
        };
        return it;
    }
    public boolean isEmpty () {
        return size() == 0;
    }

    private class Node {
        T value;
        Node prev;
        Node next;

        Node(T value) {
            this.value = value;
        }
        Node () {

        }
    }
}
