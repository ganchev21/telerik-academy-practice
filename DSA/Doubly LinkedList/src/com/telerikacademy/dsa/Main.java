package com.telerikacademy.dsa;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        LinkedList<Integer> list = new LinkedList<>();

        list.addFirst(2);
        list.addLast(3);
        list.addLast(4);
        list.add(0,1);

        for (Integer element : list) {
            System.out.print(element + " ");
        }

    }
}
